﻿using System;
using System.Collections.Generic;
using UIKit;

namespace boastloadapp.iOS
{
    public class CountdownPickerDataModel : UIPickerViewModel
    {
        public event EventHandler<EventArgs> ValueChanged;

        public List<int> Items { get; private set; }

        public int SelectedItem
        {
            get { return Items[selectedIndex]; }
        }

        int selectedIndex = 0;

        public CountdownPickerDataModel()
        {
            Items = new List<int>();
            for (int i = 0; i <= 15; i++)
            {
                Items.Add(i);
            }
        }

        public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
        {
            return Items.Count;
        }

        public override nint GetComponentCount(UIPickerView pickerView)
        {
            return 1;
        }

        public override void Selected(UIPickerView pickerView, nint row, nint component)
        {
            selectedIndex = (int)row;
            if (ValueChanged != null)
            {
                ValueChanged(this, new EventArgs());
            }
        }

        public override string GetTitle(UIPickerView pickerView, nint row, nint component)
        {
            return string.Format("{0} s", Items[(int)row]);
        }
    }
}

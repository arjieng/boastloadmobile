﻿using System;
using System.Threading.Tasks;
using AVFoundation;
using boastloadapp;
using boastloadapp.iOS;
using CoreMedia;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(VideoPreview), typeof(VideoPreviewRenderer))]
namespace boastloadapp.iOS
{

    public class VideoPreviewRenderer : ViewRenderer<VideoPreview, UIImageView>, IDisposable
    {
        UIImageView _imageView;
        VideoPreview _element;

        public VideoPreviewRenderer()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<VideoPreview> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                _imageView = new UIImageView();
                //_imageView.Frame = new CoreGraphics.CGRect(0, 0, 100, 100);
                //_imageView.BackgroundColor = UIColor.Black;

                SetNativeControl(_imageView);

            }

            if (e.OldElement != null)
            {
                // unsubscribe

            }

            if (e.NewElement != null)
            {
                // subscribe
                _element = e.NewElement;
                if (!string.IsNullOrEmpty(_element.VideoSource))
                {
                    Task.Run(() => RetrieveVideoThumbnail(_element.VideoSource));
                }

            }
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "VideoSource" && !string.IsNullOrEmpty(_element.VideoSource))
            {
                Task.Run(() => RetrieveVideoThumbnail(_element.VideoSource));
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _imageView.Dispose();
            _imageView = null;
        }

        protected void RetrieveVideoThumbnail(string url)
        {
            System.Diagnostics.Debug.WriteLine("Retrieve video thumbnail");
            AVUrlAsset asset = new AVUrlAsset(NSUrl.FromFilename(url));
            AVAssetImageGenerator generator = new AVAssetImageGenerator(asset);

            generator.AppliesPreferredTrackTransform = true;    // ensure proper orientation

            CMTime time = new CMTime(100, 100);
            CMTime actualTime;
            NSError error = null;
            var thumb = generator.CopyCGImageAtTime(time, out actualTime, out error);
            if (error != null)
            {
                System.Diagnostics.Debug.WriteLine("Error: {0}", error.ToString());
            }
            else
            {
                _element.ThumbnailWidth = thumb.Width;
                _element.ThumbnailHeight = thumb.Height;

                System.Diagnostics.Debug.WriteLine("thumbnail size available");

                Device.BeginInvokeOnMainThread(() => {
                    _imageView.Image = UIImage.FromImage(thumb);
                    _imageView.ContentMode = UIViewContentMode.ScaleAspectFit;

                    //System.Diagnostics.Debug.WriteLine("thumbnail size: ({0},{1})", thumb.Width, thumb.Height);
                    //System.Diagnostics.Debug.WriteLine("element size: ({0}, {1})", _element.Width, _element.Height);
                    //_element.WidthRequest = thumb.Width;
                    //_element.HeightRequest = thumb.Height;

                    thumb.Dispose();
                    thumb = null;
                });

            }
        }
    }
}

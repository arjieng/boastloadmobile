﻿using System;
using boastloadapp.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Label), typeof(DefaultLabelRenderer))]
namespace boastloadapp.iOS
{
    public class DefaultLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            if(Control != null)
            {
            }
        }
    }
}

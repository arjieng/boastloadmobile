﻿using System;
using boastloadapp;
using boastloadapp.iOS.Renderers;
using CoreGraphics;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(EditorXF), typeof(EditorXFRenderer))]
namespace boastloadapp.iOS.Renderers
{
    public class EditorXFRenderer : EditorRenderer
    {
        NSObject _keyboardShowObserver;
        NSObject _keyboardHideObserver;

        public EditorXFRenderer()
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            this.Control.InputAccessoryView = null;
            if (e.NewElement != null)
                RegisterForKeyboardNotification();

            if (e.OldElement != null)
                UnregisterForKeyboardNotifications();
        }

        private void RegisterForKeyboardNotification()
        {
            if (_keyboardShowObserver == null)
                _keyboardShowObserver = UIKeyboard.Notifications.ObserveWillShow(OnKeyboardShow);
            if (_keyboardHideObserver == null)
                _keyboardHideObserver = UIKeyboard.Notifications.ObserveWillHide(OnKeyboardHide);
        }

        private void OnKeyboardHide(object sender, UIKeyboardEventArgs e)
        {
            if (Element != null)
            {
                Element.Margin = new Thickness(0);
            }
        }

        private void OnKeyboardShow(object sender, UIKeyboardEventArgs e)
        {
            NSValue result = (NSValue)e.Notification.UserInfo.ObjectForKey(new NSString(UIKeyboard.FrameEndUserInfoKey));
            CGSize keyboardSize = result.RectangleFValue.Size;
            if (Element != null)
            {
                Element.Margin = new Thickness(0, 0, 0, keyboardSize.Height);
            }
        }

        void UnregisterForKeyboardNotifications()
        {
            if (_keyboardShowObserver != null)
            {
                _keyboardShowObserver.Dispose();
                _keyboardShowObserver = null;
            }

            if (_keyboardHideObserver != null)
            {
                _keyboardHideObserver.Dispose();
                _keyboardHideObserver = null;
            }
        }
    }
}






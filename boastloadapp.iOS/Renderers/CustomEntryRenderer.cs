﻿using System;
using boastloadapp;
using boastloadapp.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRendererKeyboard))]
namespace boastloadapp.iOS
{
    public class CustomEntryRendererKeyboard : EntryRenderer
    {
        float animatedDistance;
        CustomEntry entry;
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            entry = (CustomEntry)this.Element;

            if (this.Control == null) return;

            if (e.NewElement != null)
            {
                Control.BorderStyle = UITextBorderStyle.None;
                Control.SpellCheckingType = UITextSpellCheckingType.No;
                Control.AutocorrectionType = UITextAutocorrectionType.No;
                Control.AutocapitalizationType = UITextAutocapitalizationType.None;

                //Element.TextChanged += OnElement_TextChanged;
                Control.EditingDidBegin += OnEntryDidEdit;
                Control.EditingDidEnd += OnEntryDidEnd;

                if (e.NewElement.StyleId == null)
                {
                    Control.TextAlignment = UITextAlignment.Left;
                }
                else if (e.NewElement.StyleId.Equals("C"))
                {
                    Control.TextAlignment = UITextAlignment.Center;
                }
                else if (e.NewElement.StyleId.Equals("LC"))
                {
                    Control.TextAlignment = UITextAlignment.Left;
                }

                if (entry.AutoCapitalization == AutoCapitalizationType.Words)
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.Words;
                }
                else if (entry.AutoCapitalization == AutoCapitalizationType.Sentences)
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
                }
                else if (entry.AutoCapitalization == AutoCapitalizationType.All)
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.AllCharacters;
                }
                else
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.None;
                }

                if (entry != null)
                {
                    SetReturnType(entry);

                    Control.ShouldReturn += (UITextField tf) =>
                    {
                        entry.InvokeCompleted();
                        return true;
                    };
                }
            }
        }

        private void OnElement_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void SetReturnType(CustomEntry entry)
        {
            ReturnType type = entry.ReturnType;

            switch (type)
            {
                case ReturnType.Go:
                    Control.ReturnKeyType = UIReturnKeyType.Go;
                    break;
                case ReturnType.Next:
                    Control.ReturnKeyType = UIReturnKeyType.Next;
                    break;
                case ReturnType.Send:
                    Control.ReturnKeyType = UIReturnKeyType.Send;
                    break;
                case ReturnType.Search:
                    Control.ReturnKeyType = UIReturnKeyType.Search;
                    break;
                case ReturnType.Done:
                    Control.ReturnKeyType = UIReturnKeyType.Done;
                    break;
                default:
                    Control.ReturnKeyType = UIReturnKeyType.Default;
                    break;
            }
        }

        private void OnEntryDidEnd(object sender, EventArgs e)
        {
            UIView parentView = getParentView();
            var viewFrame = parentView.Bounds;

            viewFrame.Y = 0.0f;

            UIView.BeginAnimations(null, (IntPtr)null);
            UIView.SetAnimationBeginsFromCurrentState(true);
            UIView.SetAnimationDuration(0.3);

            parentView.Frame = viewFrame;

            UIView.CommitAnimations();
        }

        private void OnEntryDidEdit(object sender, EventArgs e)
        {
            UIView parentWindow = getParentView();
            var textfieldRect = parentWindow.ConvertRectFromView(Control.Bounds, Control);
            var viewRect = parentWindow.ConvertRectFromView(parentWindow.Bounds, parentWindow);

            float midline = (float)(textfieldRect.Y + 0.5 * textfieldRect.Height);
            float numerator = (float)(midline - viewRect.Y - 0.2 * viewRect.Height);
            float denominator = (float)((1.0f - 0.2f) * viewRect.Height);
            float heightFraction = numerator / denominator;

            if (heightFraction < 0.0)
            {
                heightFraction = 0.0f;
            }
            else if (heightFraction > 1.0)
            {
                heightFraction = 1.0f;
            }

            UIInterfaceOrientation orientation = UIApplication.SharedApplication.StatusBarOrientation;
            if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown)
            {
                if (entry.Keyboard == Keyboard.Numeric || entry.Keyboard == Keyboard.Telephone)
                {
                    animatedDistance = (float)Math.Floor((216.0f + 44.0f) * heightFraction);
                }
                else
                {
                    animatedDistance = (float)Math.Floor(216.0f * heightFraction);
                }
            }
            else
            {
                if (entry.Keyboard == Keyboard.Numeric || entry.Keyboard == Keyboard.Telephone)
                {
                    animatedDistance = (float)Math.Floor((162.0f + 44.0f) * heightFraction);
                }
                else
                {
                    animatedDistance = (float)Math.Floor(162.0f * heightFraction);
                }
            }

            var viewFrame = parentWindow.Frame;
            viewFrame.Y -= animatedDistance;

            UIView.BeginAnimations(null, (IntPtr)null);
            UIView.SetAnimationBeginsFromCurrentState(true);
            UIView.SetAnimationDuration(0.3);

            parentWindow.Frame = viewFrame;

            UIView.CommitAnimations();
        }

        UIView getParentView()
        {
            UIView view = Control.Superview;

            while (view != null && !(view is UIWindow))
            {
                view = view.Superview;
            }

            return view;
        }
    }
}

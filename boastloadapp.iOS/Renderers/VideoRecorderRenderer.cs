﻿using System;
using System.IO;
using System.Threading.Tasks;
using AssetsLibrary;
using AVFoundation;
using AVKit;
using CoreAnimation;
using CoreGraphics;
using CoreMedia;
using Foundation;
using Plugin.Media.Abstractions;
using boastloadapp;
using boastloadapp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(VideoRecorder), typeof(VideoRecorderRenderer))]
namespace boastloadapp.iOS
{
    public class VideoRecorderRenderer : ViewRenderer<VideoRecorder, UIView>, IRendererFunctions, IAVCaptureFileOutputRecordingDelegate, IDisposable
    {
#region Private properties
        private bool _recording;
        bool _countdownEnabled = false;

        // Recording / playback
        AVCaptureSession _captureSession;
        AVCaptureMovieFileOutput _movieFileOutput;
        AVCaptureDeviceInput _videoInputDevice;
        AVPlayer _videoPlayer;
        AVPlayerViewController _videoPlayerController;

        // UI
        UIView _container;
        AVCaptureVideoPreviewLayer _previewLayer;
        UIView _cameraView;
        //UIView _videoPlaybackView;
        UIView _timerView;
        //UIButton recordButton;
        //UIButton retakeButton, finishButton;
        //UIButton toggleCameraButton;
        UIButton countdownTimerButton;
        //UILabel recordingDurationLabel;
        UILabel countdownTimerLabel;
        UIPickerView countdownPickerView;
        CAShapeLayer _recordButtonCircle;

        IntPtr tokenObserveStatus = (IntPtr)1;

        NSUrl _currentMovileFilePath;
        ALAssetRepresentation _savedAsset;

        NSTimer _countdownTimer;
        NSTimer _recordingTimer;

        TimeSpan _recordingDuration;
        TimeSpan _countdownDuration;

        CountdownPickerDataModel pickerDataModel;

        IRecorderFunctions _recorder;

        AVAudioPlayer _audioPlayer;
        NSUrl _soundFileUrl;

        bool _hasObservers = false;
#endregion

        protected override void OnElementChanged(ElementChangedEventArgs<VideoRecorder> e)
        {
            base.OnElementChanged(e);
            //System.Diagnostics.Debug.WriteLine("OnElementChanged");

            if (Control == null)
            {
                System.Diagnostics.Debug.WriteLine("Control == null");
            }

            if (e.OldElement != null)
            {
                System.Diagnostics.Debug.WriteLine("e.OldElement != null");
                e.OldElement.StartRecording -= RecordButton_TouchUpInside1;
                e.OldElement.RotateCamera -= ToggleCameraButton_TouchUpInside;
                e.OldElement.RetakeRecording -= RetakeButton_TouchUpInside;
                e.OldElement.SaveRecording -= FinishButton_TouchUpInside;
            }

            if (e.NewElement != null)
            {
                System.Diagnostics.Debug.WriteLine("e.NewElement != null");

                e.NewElement.StartRecording += RecordButton_TouchUpInside1;
                e.NewElement.RotateCamera += ToggleCameraButton_TouchUpInside;
                e.NewElement.RetakeRecording += RetakeButton_TouchUpInside;
                e.NewElement.SaveRecording += FinishButton_TouchUpInside;


                e.NewElement.SizeChanged += (sender, ev) => {
                    SetControl();

                    _recorder = (IRecorderFunctions)e.NewElement;
                    _recorder.SetRenderer(this);

                };
            }
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
        }

        protected async void SetControl()
        {
            System.Diagnostics.Debug.WriteLine("SetControl()");
            //System.Diagnostics.Debug.WriteLine("Element size: ({0}, {1})", Element.Width, Element.Height);
            _container = new UIView(new CGRect(0, 0, Element.Width, Element.Height));
            InitializeViews();

            // setup capture session
            _captureSession = new AVCaptureSession();

            // add video input
            SetVideoInput(AVCaptureDevicePosition.Back);

            // add audio input
            SetAudioInput();

            // add outputs
            // video preview layer
            _previewLayer = new AVCaptureVideoPreviewLayer(_captureSession);
            _previewLayer.Connection.VideoOrientation = AVCaptureVideoOrientation.Portrait;
            _previewLayer.VideoGravity = AVLayerVideoGravity.ResizeAspectFill;
  
            // add movie file output
            //System.Diagnostics.Debug.WriteLine("Adding movie file output");
            ConfigureOutput(60, 1024 * 1024);

            // set connection properties
            CameraSetOutputProperties();

            LayoutViews(UIDeviceOrientation.Portrait);

            _captureSession.StartRunning();

            SetNativeControl(_container);
            //System.Diagnostics.Debug.WriteLine("Native control set");

            /// DISABLED
            //InitializeAudio(); 

            SubscribeToEvents();

            // ask for photos permission
            System.Diagnostics.Debug.WriteLine("Should request photos library permission? " + Photos.PHPhotoLibrary.AuthorizationStatus);
            if (Photos.PHPhotoLibrary.AuthorizationStatus == Photos.PHAuthorizationStatus.NotDetermined)
            {
                await Photos.PHPhotoLibrary.RequestAuthorizationAsync();
            }

        }

#region IAVCaptureFileOutputRecordingDelegate implementation
        public void FinishedRecording(AVCaptureFileOutput captureOutput, NSUrl outputFileUrl, NSObject[] connections, NSError error)
        {
            //System.Diagnostics.Debug.WriteLine("Finished recording");

            bool successful = true;
            if (error != null)
            {
                // a problem occurred
                var value = error.UserInfo.ObjectForKey(AVErrorKeys.RecordingSuccessfullyFinished);
                if (value != null)
                {
                    //System.Diagnostics.Debug.WriteLine("value: {0}", value);
                    successful = false;
                }
            }

            if (successful)
            {
                System.Diagnostics.Debug.WriteLine("recording was a success!");

                // offer playback directly afterwards
                _currentMovileFilePath = outputFileUrl;
                FinishButton_TouchUpInside(this, EventArgs.Empty);
                //DisplayPlayback(outputFileUrl);

            }

            _recording = false;
            //recordButton.SetTitle("Start Recording", UIControlState.Normal);
        }

#endregion

#region Display
        protected void InitializeViews()
        {
            System.Diagnostics.Debug.WriteLine("Initialize views");
            // record button
            //recordButton = new UIButton(UIButtonType.System);
            //recordButton.SetTitle("Start Recording", UIControlState.Normal);
            //recordButton.BackgroundColor = UIColor.Red;
            //recordButton.Layer.CornerRadius = 25;

            // retake button
            //retakeButton = new UIButton(UIButtonType.System);
            //retakeButton.SetTitle("Retake", UIControlState.Normal);
   //         retakeButton.BackgroundColor = UIColor.Orange;
            //retakeButton.SetTitleColor(UIColor.White, UIControlState.Normal);

            // finish button
            //finishButton = new UIButton(UIButtonType.System);
            //finishButton.SetTitle("Add", UIControlState.Normal);
   //         finishButton.BackgroundColor = UIColor.Green;
            //finishButton.SetTitleColor(UIColor.White, UIControlState.Normal);

            // toggle camera button
            //toggleCameraButton = new UIButton(UIButtonType.InfoLight);
            //UIImage toggleImg = UIImage.FromBundle("toggle_camera_icon");
            //toggleCameraButton.SetImage(toggleImg, UIControlState.Normal);
            //toggleCameraButton.TintColor = UIColor.White;
            //toggleCameraButton.Enabled = CanToggleCamera();

            // recording duration label
            //recordingDurationLabel = new UILabel();
            //recordingDurationLabel.Text = _recordingDuration.ToString("c");
            //recordingDurationLabel.TextColor = UIColor.White;
            //recordingDurationLabel.TextAlignment = UITextAlignment.Center;

            _timerView = new UIView();
            _timerView.Bounds = new CGRect(0, 0, 40, 20);

            countdownTimerLabel = new UILabel();
            countdownTimerLabel.Text = _countdownDuration.ToString("ss") + "s";
            countdownTimerLabel.TextColor = UIColor.White;
            countdownTimerLabel.ShadowColor = UIColor.LightGray;
            countdownTimerLabel.TextAlignment = UITextAlignment.Center;

            countdownTimerButton = new UIButton(UIButtonType.System);
            UIImage timerImg = UIImage.FromBundle("timer_icon");
            countdownTimerButton.SetImage(timerImg, UIControlState.Normal);
            countdownTimerButton.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
            countdownTimerButton.TintColor = UIColor.White;


            if (_countdownDuration.TotalSeconds == 0)
            {
                countdownTimerLabel.Hidden = true;
            }

            pickerDataModel = new CountdownPickerDataModel();
            countdownPickerView = new UIPickerView();
            countdownPickerView.BackgroundColor = UIColor.White;
            countdownPickerView.Model = pickerDataModel;

        }

        protected void LayoutViews(UIDeviceOrientation orientation)
        {

            var frame = _container.Layer.Frame;
            CGRect layerRect = _container.Layer.Bounds;

            //finishButton.Frame = new CGRect(frame.Width / 2, (frame.Height - 40), frame.Width / 2, 40);

            countdownPickerView.Frame = new CGRect(0, frame.Height - 200, frame.Width, 200);
            _container.AddSubview(countdownPickerView);

            _previewLayer.Bounds = layerRect;
            _previewLayer.Position = new CGPoint(layerRect.GetMidX(), layerRect.GetMidY());

            _cameraView = new UIView(layerRect);
            _cameraView.AutoresizingMask = UIViewAutoresizing.All;
            _cameraView.Layer.AddSublayer(_previewLayer);
            _container.AddSubview(_cameraView);

            //recordButton.Frame = new CGRect((frame.Width - 50) / 2, (frame.Height - 70), 50, 50);
            //_container.AddSubview(recordButton);

            //retakeButton.Frame = new CGRect(0, (frame.Height - 40), frame.Width / 2, 40);

            //toggleCameraButton.Frame = new CGRect(frame.Width - 40, 20, 27, 20);
            //_container.AddSubview(toggleCameraButton);

            //recordingDurationLabel.Frame = new CGRect((frame.Width - 100) / 2, 20, 100, 20);
            //_container.AddSubview(recordingDurationLabel);

            countdownTimerButton.Bounds = new CGRect(0, 0, 30, 20);
            countdownTimerButton.Frame = new CGRect(0, 0, 30, 20);
            countdownTimerLabel.Bounds = new CGRect(0, 0, 30, 20);
            countdownTimerLabel.Frame = new CGRect(0, 0, 30, 20);
            _timerView.Frame = new CGRect(frame.Width - _timerView.Bounds.Width - 15, frame.Height - 50, _timerView.Bounds.Width, _timerView.Bounds.Height);
            _timerView.AddSubview(countdownTimerButton);
            _timerView.AddSubview(countdownTimerLabel);
            _container.AddSubview(_timerView);

            _container.SendSubviewToBack(_cameraView);
            //_container.BringSubviewToFront(recordButton);
            _container.SendSubviewToBack(countdownPickerView);

        }

        //protected void DisplayPlayback(NSUrl url)
        //{
        //    System.Diagnostics.Debug.WriteLine("RENDERER: Display playback");

        //    //System.Diagnostics.Debug.WriteLine("_previewLayer.Interrupted? {0}", _previewLayer.Session.Interrupted);

        //    Element.ShowingPlayback = true;

        //    if (_videoPlayer != null)
        //    {
        //        RemoveObservers();
        //        _videoPlayer.Dispose();
        //        _videoPlayer = null;
        //    }

        //    if (_videoPlayerController == null)
        //    {
        //        _videoPlayerController = new AVPlayerViewController()
        //        {
        //            ShowsPlaybackControls = true,
        //            View = {
        //                Frame = _container.Layer.Frame
        //            }
        //        };
        //    }

        //    //if (_videoPlaybackView == null)
        //    //{
        //    //    _videoPlaybackView = new UIView();
        //    //    _videoPlaybackView.Frame = _container.Layer.Frame;
        //    //    _videoPlaybackView.AddSubview(_videoPlayerController.View);

        //    //    var frame = _container.Layer.Frame;
        //    //    _videoPlayerController.View.Frame = new CGRect(0, 0, frame.Width, frame.Height);
        //    //    //_videoPlaybackView.AddSubview(retakeButton);
        //    //    //_videoPlaybackView.AddSubview(finishButton);

        //    //    _container.AddSubview(_videoPlaybackView);
        //    //}

        //    _videoPlayer = new AVPlayer(url);
        //    _videoPlayerController.Player = _videoPlayer;

        //    SetObservers();

        //    _container.BringSubviewToFront(_videoPlaybackView);
        //}

        //protected void HidePlayback()
        //{
        //    //System.Diagnostics.Debug.WriteLine("RENDERER: Hide playback");
        //    try
        //    {
        //        Element.ShowingPlayback = false;

        //        _container.SendSubviewToBack(_videoPlaybackView);
        //    }
        //    catch (Exception e)
        //    {
        //        System.Diagnostics.Debug.WriteLine("Could not hide playback");
        //    }
        //}

        //async void UpdateRecordButton(string state)
        //{
        //  nfloat init_corner_radius = recordButton.Layer.CornerRadius;
        //  nfloat delta = (50 / 2) / 10;

        //  switch (state)
        //  {
        //      case "counting":
        //          recordButton.BackgroundColor = UIColor.LightGray;
        //          recordButton.Enabled = false;
        //          break;
        //      case "start":
        //          recordButton.Enabled = true;
        //          recordButton.BackgroundColor = UIColor.Red;
        //          for (int i = 0; i < 10; i++)
        //          {
        //              recordButton.Layer.CornerRadius += delta;
        //              await Task.Delay(50);
        //          }
        //          break;
        //      case "stop":
        //          recordButton.Enabled = true;
        //          recordButton.BackgroundColor = UIColor.Red;
        //          for (int i = 0; i < 10; i++)
        //          {
        //              recordButton.Layer.CornerRadius -= delta;
        //              await Task.Delay(50);
        //          }
        //          break;
        //      default:
        //          recordButton.Enabled = true;
        //          recordButton.BackgroundColor = UIColor.Red;
        //          for (int i = 0; i < 10; i++)
        //          {
        //              recordButton.Layer.CornerRadius += delta;
        //              await Task.Delay(50);
        //          }
        //          break;
        //  }

        //}
#endregion

#region Event handlers
        void CountdownTimerButton_TouchUpInside(object sender, EventArgs e)
        {
            // show picker view
            _container.BringSubviewToFront(countdownPickerView);
        }

        void HandleCountdownTimerFired(NSTimer obj)
        {
            if (_countdownDuration.TotalSeconds > 0)
            {
                _countdownDuration = _countdownDuration.Add(TimeSpan.FromSeconds(-1));
                countdownTimerLabel.Text = string.Format("{0}s", _countdownDuration.TotalSeconds);

                if (_countdownDuration.TotalSeconds < 3)
                {
                    //System.Diagnostics.Debug.WriteLine("Playing sound");

                    if (Element.CountdownNotificationEnabled)
                    {
                        _audioPlayer.Play();
                    }
                }

            }
            else
            {
                _countdownTimer.Invalidate();

                // hide countdown label and show button
                countdownTimerButton.Hidden = false;
                countdownTimerLabel.Hidden = true;

                StartRecording();
                //UpdateRecordButton("stop");
                //recordButton.SetTitle("Stop Recording", UIControlState.Normal);
                //recordButton.Enabled = true;
            }
        }

        void PickerDataModel_ValueChanged(object sender, EventArgs e)
        {
            _countdownDuration = TimeSpan.FromSeconds(pickerDataModel.SelectedItem);
            countdownTimerLabel.Text = string.Format("{0}s", pickerDataModel.SelectedItem);
            if (pickerDataModel.SelectedItem == 0)
            {

                Element.CountingDown = false;

                countdownTimerLabel.Hidden = true;
                countdownTimerButton.TintColor = UIColor.White;
                countdownTimerButton.SetImage(UIImage.FromBundle("timer_icon"), UIControlState.Normal);
                countdownTimerButton.ImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
                countdownTimerButton.SetTitle("", UIControlState.Normal);
            }
            else
            {
                Element.CountingDown = true;

                countdownTimerLabel.Hidden = true;
                countdownTimerButton.SetImage(null, UIControlState.Normal);
                countdownTimerButton.SetTitle(string.Format("{0}s", pickerDataModel.SelectedItem), UIControlState.Normal);
            }

            _container.SendSubviewToBack(countdownPickerView);
        }

        void HandleRecordingTimerFired(NSTimer obj)
        {
            if (_recording)
            {
                _recordingDuration = _recordingDuration.Add(TimeSpan.FromSeconds(1));
                //recordingDurationLabel.Text = _recordingDuration.ToString("c");
            }
            else
            {
                _recordingTimer.Invalidate();
            }
        }

        void ToggleCameraButton_TouchUpInside(object sender, EventArgs e)
        {
            if (AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video).Length > 1)
            {
                NSError error;

                AVCaptureDeviceInput newVideoInput;
                AVCaptureDevicePosition position = _videoInputDevice.Device.Position;

                if (position == AVCaptureDevicePosition.Back)
                {
                    newVideoInput = AVCaptureDeviceInput.FromDevice(GetCameraWithPosition(AVCaptureDevicePosition.Front), out error);
                }
                else
                {
                    newVideoInput = AVCaptureDeviceInput.FromDevice(GetCameraWithPosition(AVCaptureDevicePosition.Back), out error);
                }

                if (newVideoInput != null)
                {
                    _captureSession.BeginConfiguration(); // we can now change the camera
                    _captureSession.RemoveInput(_videoInputDevice);
                    if (_captureSession.CanAddInput(newVideoInput))
                    {
                        _captureSession.AddInput(newVideoInput);
                        _videoInputDevice = newVideoInput;
                    }
                    else
                    {
                        _captureSession.AddInput(_videoInputDevice);
                    }

                    CameraSetOutputProperties();

                    _captureSession.CommitConfiguration();
                }
            }
        }

        void RecordButton_TouchUpInside1(object sender, EventArgs e)
        {
            if (!_recording)
            {
                if (_countdownDuration.TotalSeconds > 0)
                {
                    // hide countdown button and show label
                    countdownTimerButton.Hidden = true;
                    countdownTimerLabel.Hidden = false;

                    _countdownTimer = NSTimer.CreateRepeatingScheduledTimer(1.0, HandleCountdownTimerFired);
                    //recordButton.Enabled = false;
                    //UpdateRecordButton("counting");
                }
                else
                {
                    StartRecording();
                    //recordButton.SetTitle("Stop Recording", UIControlState.Normal);
                    //UpdateRecordButton("stop");
                }
            }
            else
            {
                StopRecording();
                //recordButton.SetTitle("Start Recording", UIControlState.Normal);
                //UpdateRecordButton("start");
            }
        }

        async Task<string> CompressVideo(string path)
        {
            TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();

            AVAssetExportSession encoder = new AVAssetExportSession(AVAsset.FromUrl(NSUrl.FromFilename(path)), AVAssetExportSession.Preset960x540);

            var outputURL = (NSString)(System.IO.Path.GetTempPath() + "temp_send.mp4");
            NSFileManager fileManager = NSFileManager.DefaultManager;
            if (fileManager.FileExists(outputURL))
            {
                System.Diagnostics.Debug.WriteLine("File exists, removing");
                NSError error;
                if (!fileManager.Remove(outputURL, out error))
                {
                    System.Diagnostics.Debug.WriteLine("Error removing file {0}", error);
                }
            }

            encoder.OutputUrl = NSUrl.FromFilename(outputURL);
            encoder.OutputFileType = AVFileType.Mpeg4;
            encoder.ShouldOptimizeForNetworkUse = true;

            encoder.ExportAsynchronously(() => {
                System.Diagnostics.Debug.WriteLine("status: {0}", encoder.Status);
                if (encoder.Status == AVAssetExportSessionStatus.Failed)
                {
                    System.Diagnostics.Debug.WriteLine("error: {0}", encoder.Error.Description);
                    tcs.SetCanceled();
                }
                else if (encoder.Status == AVAssetExportSessionStatus.Completed)
                {
                    tcs.SetResult(outputURL);

                }
            });

            return tcs.Task.Result;
        }

        async void FinishButton_TouchUpInside(object sender, EventArgs e)
        {

            _recorder.SetVideoCompressing(true);
            // make path
            var path = GetOutputPath(string.Empty, null);
            File.Move(_currentMovileFilePath.Path, path);
            var success = await SaveToFile(new NSUrl(path));    // original file

            System.Diagnostics.Debug.WriteLine("original file size: {0}MB", File.OpenRead(path).Length / (1024.0 * 1024));

            var tempPath = await CompressVideo(path);
            //var tempPath = path;

            var path2 = GetOutputPath(string.Empty, "temp.mp4");
            RemoveExistingFile(path2);
           File.Move(tempPath, path2);
            _recorder.SetVideoCompressing(false);

            System.Diagnostics.Debug.WriteLine("original file size: {0}MB", File.OpenRead(path2).Length / (1024.0 * 1024));

            if (success)
            {

                var mediaFile = new MediaFile(path,
                                              () => {
                                                  return File.OpenRead(path2);
                                              },
                                              true,
                                              (dispose) => {
                                                  if (dispose)
                                                  {
                                                      RemoveExistingFile(path2);
                                                  }
                                              });

                _recorder.SetVideoRecorded(mediaFile);
            }

            Reset();

            //HidePlayback();

            ReleaseVideoPlayer();
        }

        private static string GetOutputPath(string path, string name)
        {
            path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), path);
            Directory.CreateDirectory(path);

            if (String.IsNullOrWhiteSpace(name))
            {
                string timestamp = DateTime.Now.ToString("yyyMMdd_HHmmss");
                name = "VID_" + timestamp + ".mp4";
            }

            return Path.Combine(path, name);
        }

        void RetakeButton_TouchUpInside(object sender, EventArgs e)
        {
            _recordingDuration = new TimeSpan();
            //recordingDurationLabel.Text = _recordingDuration.ToString("c");

            //_container.SendSubviewToBack(_videoPlaybackView);




            RemoveExistingFile(_currentMovileFilePath.RelativePath);

            Reset();

            //HidePlayback();

            // disposing
            ReleaseVideoPlayer();

        }
#endregion

#region Lifecycle
        protected void SubscribeToEvents()
        {
            //recordButton.TouchUpInside += RecordButton_TouchUpInside1;
            //retakeButton.TouchUpInside += RetakeButton_TouchUpInside;
            //finishButton.TouchUpInside += FinishButton_TouchUpInside;
            //toggleCameraButton.TouchUpInside += ToggleCameraButton_TouchUpInside;
            countdownTimerButton.TouchUpInside += CountdownTimerButton_TouchUpInside;
            pickerDataModel.ValueChanged += PickerDataModel_ValueChanged;
        }

        protected void UnsubscribeFromEvents()
        {
            //recordButton.TouchUpInside -= RecordButton_TouchUpInside1;
            //retakeButton.TouchUpInside -= RetakeButton_TouchUpInside;
            //finishButton.TouchUpInside -= FinishButton_TouchUpInside;
            //toggleCameraButton.TouchUpInside -= ToggleCameraButton_TouchUpInside;
            countdownTimerButton.TouchUpInside -= CountdownTimerButton_TouchUpInside;
            pickerDataModel.ValueChanged -= PickerDataModel_ValueChanged;
        }

        protected void ReleaseVideoPlayer()
        {
            if (_videoPlayer != null)
            {
                RemoveObservers();
                _videoPlayer.Dispose();
                _videoPlayer = null;
            }
        }

        void RemoveObservers()
        {
            if (!_hasObservers)
                return;

            System.Diagnostics.Debug.WriteLine("RemoveObservers");

            _videoPlayer.RemoveObserver(this, "status", tokenObserveStatus);
            _videoPlayer.RemoveObserver(this, "rate", tokenObserveStatus);
            _videoPlayer.RemoveObserver(this, "interrupted", tokenObserveStatus);

            _hasObservers = false;
        }

        void SetObservers()
        {
            System.Diagnostics.Debug.WriteLine("SetObservers");
            _videoPlayer.AddObserver(this, "status", NSKeyValueObservingOptions.OldNew, tokenObserveStatus);
            _videoPlayer.AddObserver(this, "rate", NSKeyValueObservingOptions.OldNew, tokenObserveStatus);
            _videoPlayer.AddObserver(this, "interrupted", NSKeyValueObservingOptions.OldNew, tokenObserveStatus);

            _hasObservers = true;
        }

        /// <summary>
        /// Resets various video recording UI.
        /// </summary>
        protected void Reset()
        {
            _recordingDuration = TimeSpan.Zero;
            //recordingDurationLabel.Text = _recordingDuration.ToString("c");

            countdownTimerLabel.Text = _countdownDuration.ToString("ss") + "s";
            _countdownDuration = TimeSpan.FromSeconds(pickerDataModel.SelectedItem);

            _savedAsset = null;
        }
#endregion

#region Video recorder functions
        /// <summary>
        /// Configures the player that will play the countdown timer sound.
        /// </summary>
        void InitializeAudio()

        {
            NSError error;
            _soundFileUrl = NSUrl.FromFilename("beep.mp3");
            _audioPlayer = new AVAudioPlayer(_soundFileUrl, "mp3", out error);

            if (error != null)
            {
                System.Diagnostics.Debug.WriteLine("Could not initialize audio");
            }
            else
            {
                _audioPlayer.Volume = 1;
                _audioPlayer.NumberOfLoops = 0;
            }
        }

        /// <summary>
        /// Returns the correct camera given the indicated position (front/back).
        /// </summary>
        protected AVCaptureDevice GetCameraWithPosition(AVCaptureDevicePosition position)
        {
            var devices = AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video);
            foreach (var device in devices)
            {
                if (device.Position == position)
                {
                    return device;
                }
            }
            return null;
        }

        /// <summary>
        /// Sets the video input.
        /// </summary>
        protected void SetVideoInput(AVCaptureDevicePosition position)
        {
            NSError error = null;
            var device = GetCameraWithPosition(position);

            if (device != null)
            {
                _videoInputDevice = AVCaptureDeviceInput.FromDevice(device, out error);
            }
            else
            {
                // set default
                AVCaptureDevice _videoDevice = AVCaptureDevice.DefaultDeviceWithMediaType(AVFoundation.AVMediaType.Video);
                if (_videoDevice != null)
                {
                    _videoInputDevice = AVCaptureDeviceInput.FromDevice(_videoDevice, out error);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Couldn't create video capture device");
                }
            }

            if (error == null)
            {


                if (_captureSession.CanAddInput(_videoInputDevice))
                {
                    _captureSession.AddInput(_videoInputDevice);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Couldn't add video input");
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Couldn't create video input");
            }
        }

        protected void SetAudioInput()
        {
            AVCaptureDevice audioCaptureDevice = AVCaptureDevice.DefaultDeviceWithMediaType(AVFoundation.AVMediaType.Audio);
            NSError audioError;
            AVCaptureDeviceInput audioInput = AVCaptureDeviceInput.FromDevice(audioCaptureDevice, out audioError);
            if (audioInput != null)
            {
                if (_captureSession.CanAddInput(audioInput))
                {
                    _captureSession.AddInput(audioInput);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Couldn't add audio input");
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Couldn't create audio input");
            }
        }

        void CameraSetOutputProperties()
        {
            // set connection properties
            AVCaptureConnection captureConnection = _movieFileOutput.ConnectionFromMediaType(AVFoundation.AVMediaType.Video);
        }

        protected void ConfigureOutput(long durationMax, long diskSpaceLimit)
        {
            _movieFileOutput = new AVCaptureMovieFileOutput();

            _movieFileOutput.MaxRecordedDuration = CMTime.FromSeconds(durationMax, 1);
            _movieFileOutput.MinFreeDiskSpaceLimit = diskSpaceLimit;

            if (_captureSession.CanAddOutput(_movieFileOutput))
            {
                _captureSession.AddOutput(_movieFileOutput);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Could not add movie output");
            }

            // image quality
            if (_captureSession.CanSetSessionPreset(AVCaptureSession.PresetLow))
            {
                _captureSession.SessionPreset = AVCaptureSession.PresetLow;
            }
        }

        protected async Task<bool> SaveToFile(NSUrl path)
        {
            var tcs = new TaskCompletionSource<bool>();

            ALAssetsLibrary library = new ALAssetsLibrary();
            Action<ALAsset> successHandler = null;
            Action<NSError> errorHandler = null;

            successHandler = new Action<ALAsset>((obj) => {
                _savedAsset = obj.DefaultRepresentation;
                tcs.SetResult(true);
            });

            errorHandler = new Action<NSError>((obj) => {
                tcs.SetResult(false);
            });

            if (library.VideoAtPathIsIsCompatibleWithSavedPhotosAlbum(path))
            {
                var tempPath = await library.WriteVideoToSavedPhotosAlbumAsync(path);
                //library.AssetForUrl(tempPath, successHandler, errorHandler);
                tcs.SetResult(tempPath != null);
            }
            library.Dispose();

            return await tcs.Task;
        }

        protected void StartRecording()
        {
            //await Task.Delay(3000);

            // create temporary url to record to
            NSString outputPath = (NSString)(System.IO.Path.GetTempPath() + "output.mp4");
            NSUrl outputUrl = NSUrl.FromFilename(outputPath);
            NSFileManager fileManager = NSFileManager.DefaultManager;

            if (fileManager.FileExists(outputPath))
            {
                NSError error;
                if (!fileManager.Remove(outputPath, out error))
                {
                    System.Diagnostics.Debug.WriteLine("Error removing file {0}", error);
                    throw new System.Exception("Could not remove file");
                    //return;
                }
            }

            // start recording
            _movieFileOutput.StartRecordingToOutputFile(outputUrl, this);
            _recording = true;

            // setup timer
            //_recordingTimer = NSTimer.CreateRepeatingScheduledTimer(1.0, HandleRecordingTimerFired);
            //_recordingDuration = TimeSpan.Zero;
        }

        protected void StopRecording()
        {
            _movieFileOutput.StopRecording();
            _recording = false;
        }

        public override void ObserveValue(NSString keyPath, NSObject ofObject, NSDictionary change, IntPtr context)
        {
            if (keyPath == "status")
            {
                if (_videoPlayer.Status == AVPlayerStatus.ReadyToPlay)
                {
                    System.Diagnostics.Debug.WriteLine("ready to play");
                }
            }
            else if (keyPath == "rate")
            {
                if (Math.Abs(_videoPlayer.Rate) < 0.001)
                {
                    System.Diagnostics.Debug.WriteLine("Playback stopped");
                    //if (_videoPlaybackView != null && _videoPlayerController != null) {
                    //var frame = _container.Layer.Frame;
                    //_videoPlayerController.View.Frame = new CGRect(0, 0, frame.Width, frame.Height - 40);
                    //_videoPlaybackView.AddSubview(retakeButton);
                    //_videoPlaybackView.AddSubview(finishButton);
                    //}
                }
                else if (Math.Abs(_videoPlayer.Rate - 1.0) < 0.001)
                {
                    System.Diagnostics.Debug.WriteLine("Normal playback");
                    //retakeButton.RemoveFromSuperview();
                    //finishButton.RemoveFromSuperview();
                    //_videoPlayerController.View.Frame = new CGRect(0, 0, _container.Layer.Frame.Width, _container.Layer.Frame.Height);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Playback rate: {0}", _videoPlayer.Rate);
                }
            }
            else if (keyPath == "interrupted")
            {
                System.Diagnostics.Debug.WriteLine("INTERRUPTED");
            }
            else
            {
                base.ObserveValue(keyPath, ofObject, change, context);
            }
        }

        protected bool CanToggleCamera()
        {
            return AVCaptureDevice.DevicesWithMediaType(AVMediaType.Video).Length > 1;
        }

        void RemoveExistingFile(string path)
        {
            NSFileManager fileManager = NSFileManager.DefaultManager;
            if (fileManager.FileExists(path))
            {
                System.Diagnostics.Debug.WriteLine("File exists, removing");
                NSError error;
                if (!fileManager.Remove(path, out error))
                {
                    System.Diagnostics.Debug.WriteLine("Error removing file {0}", error);
                }
            }
        }
#endregion

#region IDisposable implementation
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {

                System.Diagnostics.Debug.WriteLine("Disposing of video recorder");
                ReleaseVideoPlayer();

                if (_audioPlayer != null)
                {
                    _audioPlayer.Stop();
                    _audioPlayer.Dispose();
                    _audioPlayer = null;
                }

                UnsubscribeFromEvents();

            }
        }
#endregion

    }
}

﻿using System;
using boastloadapp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ListView), typeof(ListViewNoScrollRenderer))]
namespace boastloadapp.iOS
{
    public class ListViewNoScrollRenderer : ListViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            Control.ShowsVerticalScrollIndicator = false;
            Control.ShowsHorizontalScrollIndicator = false;
            Control.Bounces = false;
            Control.SeparatorStyle = UITableViewCellSeparatorStyle.None;

        }


    }
}
﻿using System;
using AVFoundation;
using AVKit;
using CoreGraphics;
using Foundation;
using boastloadapp;
using boastloadapp.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomVideoPlayer), typeof(CustomVideoPlayerRenderer))]
namespace boastloadapp.iOS
{
    public class CustomVideoPlayerRenderer : ViewRenderer<CustomVideoPlayer, UIView>, IDisposable
    {
        AVPlayerViewController _playerController;
        AVPlayer _player;
        CustomVideoPlayer _element;
        IntPtr tokenObserveStatus = (IntPtr)1;
        UIView _container;
        bool _observerAttached = false;

        public CustomVideoPlayerRenderer()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<CustomVideoPlayer> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {

            }

            if (e.OldElement != null)
            {
                // unsubscribe

            }

            if (e.NewElement != null)
            {
                // subscribe
                _element = e.NewElement;
                SetControl();
            }
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "VideoSource")
            {
                UpdateVideoUrl(_element.VideoSource);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            System.Diagnostics.Debug.WriteLine("Disposing video player");

            if (_observerAttached)
            {
                _player.RemoveObserver(this, "status", tokenObserveStatus);
                _observerAttached = false;
            }

            if (_player != null)
            {
                _player.Pause();
                _player.Dispose();
                _player = null;
            }

            if (_playerController != null)
            {
                _playerController.Dispose();
                _playerController = null;
            }

        }

        void SetControl()
        {

            _container = new UIView(new CGRect(0, 0, Element.Width, Element.Height));

            if (_playerController == null)
            {
                _playerController = new AVPlayerViewController
                {
                    View = { Frame = _container.Frame },
                    ShowsPlaybackControls = false
                };
            }

            _container.AddSubview(_playerController.View);

            SetNativeControl(_container);
        }

        protected void UpdateVideoUrl(string url)
        {
            if (_player != null)
            {
                _player.Pause();
                _player.Dispose();
                _player = null;
            }
            _player = new AVPlayer(NSUrl.FromString(url));
            _playerController.Player = _player;
            _player.AddObserver(this, "status", NSKeyValueObservingOptions.OldNew, tokenObserveStatus);
            _observerAttached = true;

            _element.IsLoading = true;
        }

        public override void ObserveValue(NSString keyPath, NSObject ofObject, NSDictionary change, IntPtr context)
        {
            if (context == tokenObserveStatus)
            {
                if (_player.Status == AVPlayerStatus.ReadyToPlay)
                {
                    System.Diagnostics.Debug.WriteLine("ready to play");
                    _playerController.ShowsPlaybackControls = true;
                    _element.IsLoading = false;
                    _player.RemoveObserver(this, "status", tokenObserveStatus);
                    _observerAttached = false;
                }
            }
            else
            {
                base.ObserveValue(keyPath, ofObject, change, context);
            }
        }
    }
}

﻿using System;
using System.ComponentModel;
using boastloadapp;
using boastloadapp.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomListView), typeof(CustomListViewRenderer))]
namespace boastloadapp.iOS.Renderers
{
    public class CustomListViewRenderer : ListViewRenderer
    {
        CustomListView customListView { get; set; }
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);
            Control.ShowsVerticalScrollIndicator = false;
            Control.ShowsHorizontalScrollIndicator = false;
            Control.Bounces = true;
            Control.SeparatorStyle = UIKit.UITableViewCellSeparatorStyle.None;
            if (e.NewElement != null)
            {
                this.customListView = (CustomListView)this.Element;
                Control.ScrollEnabled = customListView.Scroll;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (Element == null)
                return;
            if (e.PropertyName == CustomListView.ScrollProperty.PropertyName)
            {
                Control.ScrollEnabled = customListView.Scroll;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarouselView.FormsPlugin.iOS;
using FFImageLoading.Forms.Platform;
using Foundation;
using HockeyApp.iOS;
using ImageCircle.Forms.Plugin.iOS;
using Octane.Xamarin.Forms.VideoPlayer.iOS;
using Plugin.CrossPlatformTintedImage.iOS;
using ProgressRingControl.Forms.Plugin.iOS;
using Syncfusion.ListView.XForms.iOS;
using UIKit;

namespace boastloadapp.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            // Scaling
            App.screenWidth = (float)UIScreen.MainScreen.Bounds.Width;
            App.screenHeight = (float)UIScreen.MainScreen.Bounds.Height;
            App.appScale = (float)UIScreen.MainScreen.Scale;

            // HockeyApp
            var manager = BITHockeyManager.SharedHockeyManager;
            manager.Configure("b56c245ef4e54f7391ae19207486f627");
            manager.StartManager();

            LoadApplication(new App());

            // FFImageLoading
            CachedImageRenderer.Init();

            // CarouselView
            CarouselViewRenderer.Init();

            /// Progress Ring
            ProgressRingRenderer.Init();

            // Circle Image
            ImageCircleRenderer.Init();

            // Tinted Image
            TintedImageRenderer.Init();

            // Octane Video Player
            FormsVideoPlayer.Init();

            return base.FinishedLaunching(app, options);
        }

    }
}

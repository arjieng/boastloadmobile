﻿using System;
using boastloadapp.iOS;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(StatusStyleService))]
namespace boastloadapp.iOS
{
    public class StatusStyleService : IStatusStyle
    {
        public void StatusStyle(int status)
        {
            switch (status)
            {
                case 0:
                    (UIApplication.SharedApplication).SetStatusBarStyle(UIStatusBarStyle.LightContent, true);
                    break;
                case 1:
                    (UIApplication.SharedApplication).SetStatusBarStyle(UIStatusBarStyle.Default, true);
                    break;
            }
        }
    }

}

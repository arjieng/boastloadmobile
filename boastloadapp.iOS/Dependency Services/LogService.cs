﻿using System;
using boastloadapp.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(LogService))]
namespace boastloadapp.iOS
{
    public class LogService : ILogServices
    {
        public void Log(string log)
        {
            Console.WriteLine(log);
        }
    }
}

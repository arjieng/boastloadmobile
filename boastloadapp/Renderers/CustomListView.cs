﻿using System;
using Xamarin.Forms;

namespace boastloadapp
{
    public class CustomListView : ListView
    {

        public static readonly BindableProperty ScrollProperty = BindableProperty.Create(nameof(Scroll), typeof(bool), typeof(CustomListView), true, BindingMode.Default, null);
        public bool Scroll{
            get { return (bool)GetValue(ScrollProperty); }
            set { SetValue(ScrollProperty, value); }
        }
    }
}

﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace boastloadapp
{
    public class VideoPreview : View
    {
        public VideoPreview()
        {
            var tapGestureRecognizer = new TapGestureRecognizer();
            tapGestureRecognizer.Tapped += TapGestureRecognizer_Tapped;
            this.GestureRecognizers.Add(tapGestureRecognizer);
        }

        public double ThumbnailWidth
        {
            get { return _thumbnailWidth; }
            set
            {
                _thumbnailWidth = value;
                //if (_thumbnailHeight > 0) {
                //this.WidthRequest = _thumbnailWidth;
                //}
            }
        }
        protected double _thumbnailWidth = -1;


        public double ThumbnailHeight
        {
            get { return _thumbnailHeight; }
            set
            {
                _thumbnailHeight = value;
                //if (_thumbnailWidth > 0) {
                //this.InvalidateMeasure();
                //}
                //this.HeightRequest = _thumbnailHeight;
            }
        }
        protected double _thumbnailHeight = -1;

        public static readonly BindableProperty ThumbnailProperty = BindableProperty.Create(nameof(Thumbnail), typeof(ImageSource), typeof(VideoPreview), default(ImageSource), BindingMode.OneWayToSource);
        public ImageSource Thumbnail
        {
            get { return (ImageSource)GetValue(ThumbnailProperty); }
            set { SetValue(ThumbnailProperty, value); }
        }

        public static readonly BindableProperty CommandProperty = BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(VideoPreview), default(ICommand),
                propertyChanged: (bindable, oldValue, newValue) => {
                    var view = bindable as VideoPreview;
                }
        );
        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public static readonly BindableProperty VideoSourceProperty = BindableProperty.Create(nameof(VideoSource), typeof(string), typeof(VideoPreview), default(string),
                propertyChanged: (bindable, oldValue, newValue) => {
                    var view = bindable as VideoPreview;
                    System.Diagnostics.Debug.WriteLine("VideoPreview::changed videosource property");
                }
        );
        public string VideoSource
        {
            get { return (string)GetValue(VideoSourceProperty); }
            set { SetValue(VideoSourceProperty, value); }
        }

        void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (Command != null && Command.CanExecute(VideoSource))
            {
                System.Diagnostics.Debug.WriteLine("Executing command...");
                Command.Execute(VideoSource);
            }
        }

        //protected override SizeRequest OnMeasure(double widthConstraint, double heightConstraint)
        //{
        //  if (ThumbnailWidth < 0 || ThumbnailHeight < 0) {
        //      return base.OnMeasure(widthConstraint, heightConstraint);
        //  }

        //  double w = ThumbnailWidth, h = ThumbnailHeight;
        //  if (Double.IsInfinity(heightConstraint)) {
        //      if (Double.IsInfinity(widthConstraint)) {
        //          return new SizeRequest(new Size(w, h));
        //      } else {
        //          h = widthConstraint * h / w;
        //      }
        //  } else if (Double.IsInfinity(widthConstraint)) {
        //      w = heightConstraint * w / h;
        //  } else {

        //      var outerRatio = widthConstraint / heightConstraint;
        //      var innerRatio = ThumbnailWidth / ThumbnailHeight;

        //      var resizeFactor = (innerRatio >= outerRatio) ? (widthConstraint / ThumbnailWidth) : (heightConstraint / ThumbnailHeight);
        //      w *= resizeFactor;
        //      h *= resizeFactor;
        //  }

        //  return new SizeRequest(new Size(w, h));
        //}
    }
}

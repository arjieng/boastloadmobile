﻿using System;
using Xamarin.Forms;

namespace boastloadapp
{
    public enum ReturnType
    {
        Go,
        Next,
        Done,
        Send,
        Search
    }

    public enum AutoCapitalizationType
    {
        Words,
        Sentences,
        None,
        All
    }

    public class CustomEntry : Entry
    {
        // Need to overwrite default handler because we cant Invoke otherwise
        public new event EventHandler Completed;

        public static readonly BindableProperty ReturnTypeProperty = BindableProperty.Create(nameof(ReturnType), typeof(ReturnType), typeof(CustomEntry), ReturnType.Done, BindingMode.TwoWay);
        public ReturnType ReturnType
        {
            get { return (ReturnType)GetValue(ReturnTypeProperty); }
            set { SetValue(ReturnTypeProperty, value); }
        }

        public static readonly BindableProperty AutoCapitalizationProperty = BindableProperty.Create(nameof(AutoCapitalization), typeof(AutoCapitalizationType), typeof(CustomEntry), AutoCapitalizationType.None);
        public AutoCapitalizationType AutoCapitalization
        {
            get { return (AutoCapitalizationType)GetValue(AutoCapitalizationProperty); }
            set { SetValue(AutoCapitalizationProperty, value); }
        }

        public void InvokeCompleted()
        {
            if (this.Completed != null)
                this.Completed.Invoke(this, null);
        }
    }
}

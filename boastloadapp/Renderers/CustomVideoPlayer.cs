﻿using System;
using System.IO;
using Xamarin.Forms;

namespace boastloadapp
{

    public class CustomVideoPlayer : View
    {
        public CustomVideoPlayer()
        {
        }

        public static readonly BindableProperty VideoSourceProperty =
            BindableProperty.Create(nameof(VideoSource), typeof(string), typeof(VideoPlayer), default(string));
        public string VideoSource
        {
            get { return (string)GetValue(VideoSourceProperty); }
            set { SetValue(VideoSourceProperty, value); }
        }

        public static readonly BindableProperty IsLoadingProperty =
            BindableProperty.Create(nameof(IsLoading), typeof(bool), typeof(VideoPlayer), default(bool), BindingMode.OneWayToSource);
        public bool IsLoading
        {
            get { return (bool)GetValue(IsLoadingProperty); }
            set { SetValue(IsLoadingProperty, value); }
        }
    }
}
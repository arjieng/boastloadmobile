﻿using System;
using FFImageLoading.Forms;

namespace boastloadapp
{
    public class CustomCachedImage : CachedImage
    {
        public CustomCachedImage()
        {
            this.DownsampleWidth = 300.ScaleWidth();
            this.DownsampleHeight = 113.ScaleHeight();
            this.CacheDuration = TimeSpan.FromDays(30);
            this.DownsampleToViewSize = true;
            this.RetryCount = 0;
            this.RetryDelay = 250;
            this.LoadingPlaceholder = "Loading";
            this.ErrorPlaceholder = "Failed to load image.";
            this.FadeAnimationForCachedImages = true;
            this.FadeAnimationEnabled = true;
            this.FadeAnimationDuration = 1;
            this.RetryCount = 3;
            this.RetryDelay = 3;
            //TransparencyEnabled = false;
            //CacheKeyFactory = new CustomCacheKeyFactory();
        }
    }
}

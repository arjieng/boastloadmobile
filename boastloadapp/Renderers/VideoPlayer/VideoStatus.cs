﻿namespace boastloadapp
{
    public enum VideoStatus
    {
        NotReady,
        Playing,
        Paused
    }
}

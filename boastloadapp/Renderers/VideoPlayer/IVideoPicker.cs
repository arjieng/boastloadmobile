﻿using System;
using System.Threading.Tasks;

namespace boastloadapp
{
    public interface IVideoPicker
    {
        Task<string> GetVideoFileAsync();
    }
}

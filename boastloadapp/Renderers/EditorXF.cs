﻿using System;
using Xamarin.Forms;

namespace boastloadapp
{
    public class EditorXF : Editor
    {
        public EditorXF()
        {
            this.TextChanged += EditorPlaceHolder_TextChanged;
            Text = Placeholder;
            TextColor = Color.LightGray;
            this.Focused += EditorPlaceHolder_Focused;
            this.Unfocused += EditorPlaceHolder_Unfocused;
        }

        void EditorPlaceHolder_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.InvalidateMeasure();
        }


        void EditorPlaceHolder_Focused(object sender, FocusEventArgs e)
        {
            if (Empty())
            {
                base.Text = "";
                this.TextColor = TextColors;
            }
        }
        void EditorPlaceHolder_Unfocused(object sender, FocusEventArgs e)
        {
            if (Empty())
            {
                this.Text = Placeholder;
                this.TextColor = Color.LightGray;
            }
        }

        public bool Empty()
        {
            return (this.Text.Equals("") || this.Text.Equals(this.Placeholder));
        }

        public virtual new string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
                if (Empty())
                {
                    this.TextColor = Color.LightGray;
                    base.Text = this.Placeholder;
                }
                else
                {
                    this.TextColor = TextColors;
                }

            }
        }

        public static readonly BindableProperty TextColorsProperty = BindableProperty.Create(nameof(TextColors), typeof(Color), typeof(EditorXF), Color.Transparent);
        public Color TextColors
        {
            get { return (Color)GetValue(TextColorsProperty); }
            set { SetValue(TextColorsProperty, value); }
        }


        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(EditorXF), "");
        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }
    }
}

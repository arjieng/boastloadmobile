﻿using System;
namespace boastloadapp
{
    public interface IRendererFunctions
    {
        // The renderer should implement this interface
        // And the XF class should hold a reference to the object that implements this interface
        void Dispose();
    }
}

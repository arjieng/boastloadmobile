﻿using System;
using System.Windows.Input;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace boastloadapp
{
    public enum CameraPosition
    {
        Rear, Front
    };

    public class VideoRecorder : View, IRecorderFunctions, IDisposable
    {
        // bindable properties
        public static readonly BindableProperty CameraFacingProperty = BindableProperty.Create(nameof(CameraFacing), typeof(CameraPosition), typeof(VideoRecorder), CameraPosition.Rear, BindingMode.TwoWay);
        public CameraPosition CameraFacing
        {
            get { return (CameraPosition)GetValue(CameraFacingProperty); }
            set { SetValue(CameraFacingProperty, value); }
        }

        public static readonly BindableProperty CountingDownProperty = BindableProperty.Create(nameof(CountingDown), typeof(bool), typeof(VideoRecorder), default(bool), BindingMode.OneWayToSource,
                propertyChanged: (bindable, oldValue, newValue) => {
                    var view = bindable as VideoRecorder;
                    System.Diagnostics.Debug.WriteLine("Counting down: {0}", (bool)newValue);
                }
        );
        public bool CountingDown
        {
            get { return (bool)GetValue(CountingDownProperty); }
            set { SetValue(CountingDownProperty, value); }
        }

        public static readonly BindableProperty CountdownNotificationEnabledProperty = BindableProperty.Create(nameof(CountdownNotificationEnabled), typeof(bool), typeof(VideoRecorder), true);
        public bool CountdownNotificationEnabled
        {
            get { return (bool)GetValue(CountdownNotificationEnabledProperty); }
            set { SetValue(CountdownNotificationEnabledProperty, value); }
        }

        public static readonly BindableProperty ShowingPlaybackProperty = BindableProperty.Create(nameof(ShowingPlayback), typeof(bool), typeof(VideoRecorder), false, BindingMode.OneWayToSource,
                propertyChanged: (bindable, oldValue, newValue) => {
                    var view = bindable as VideoRecorder;
                    System.Diagnostics.Debug.WriteLine("Showing playback {0}", (bool)newValue);
                }
        );
        public bool ShowingPlayback
        {
            get { return (bool)GetValue(ShowingPlaybackProperty); }
            set { SetValue(ShowingPlaybackProperty, value); }
        }


        // commands
        public static readonly BindableProperty RecordedCommandProperty = BindableProperty.Create(nameof(RecordedCommand), typeof(ICommand), typeof(VideoRecorder), default(ICommand));
        public ICommand RecordedCommand
        {
            get { return (ICommand)GetValue(RecordedCommandProperty); }
            set { SetValue(RecordedCommandProperty, value); }
        }

        public static readonly BindableProperty CompressingCommandProperty = BindableProperty.Create(nameof(CompressingCommand), typeof(ICommand), typeof(VideoRecorder), default(ICommand));
        public ICommand CompressingCommand
        {
            get { return (ICommand)GetValue(CompressingCommandProperty); }
            set { SetValue(CompressingCommandProperty, value); }
        }

        #region EventHandlers
        public static readonly BindableProperty StartRecordingProperty = BindableProperty.Create(nameof(StartRecording), typeof(EventHandler<EventArgs>), typeof(VideoRecorder), default(EventHandler<EventArgs>));
        public EventHandler<EventArgs> StartRecording
        {
            get { return (EventHandler<EventArgs>)GetValue(StartRecordingProperty); }
            set { SetValue(StartRecordingProperty, value); }
        }

        public static readonly BindableProperty RotateCameraProperty = BindableProperty.Create(nameof(RotateCamera), typeof(EventHandler<EventArgs>), typeof(VideoRecorder), default(EventHandler<EventArgs>));
        public EventHandler<EventArgs> RotateCamera
        {
            get { return (EventHandler<EventArgs>)GetValue(RotateCameraProperty); }
            set { SetValue(RotateCameraProperty, value); }
        }

        public static readonly BindableProperty RetakeRecordingProperty = BindableProperty.Create(nameof(RetakeRecording), typeof(EventHandler<EventArgs>), typeof(VideoRecorder), default(EventHandler<EventArgs>));
        public EventHandler<EventArgs> RetakeRecording
        {
            get { return (EventHandler<EventArgs>)GetValue(RetakeRecordingProperty); }
            set { SetValue(RetakeRecordingProperty, value); }
        }

        public static readonly BindableProperty SaveRecordingProperty = BindableProperty.Create(nameof(SaveRecording), typeof(EventHandler<EventArgs>), typeof(VideoRecorder), default(EventHandler<EventArgs>));
        public EventHandler<EventArgs> SaveRecording
        {
            get { return (EventHandler<EventArgs>)GetValue(SaveRecordingProperty); }
            set { SetValue(SaveRecordingProperty, value); }
        }
        #endregion

        public VideoRecorder()
        {

        }

        protected IRendererFunctions _functions;
        void IRecorderFunctions.SetRenderer(IRendererFunctions functions)
        {
            _functions = functions;
        }

        void IRecorderFunctions.SetVideoRecorded(MediaFile mediaFile)
        {
            System.Diagnostics.Debug.WriteLine("IRecorderFunctions.SetVideoRecorded");
            System.Diagnostics.Debug.WriteLine("File path: {0}", mediaFile.Path);

            if (RecordedCommand != null && RecordedCommand.CanExecute(mediaFile))
            {
                RecordedCommand.Execute(mediaFile);
            }
        }

        void IRecorderFunctions.SetVideoCompressing(bool compressing)
        {
            if (CompressingCommand != null && CompressingCommand.CanExecute(compressing))
            {
                CompressingCommand.Execute(compressing);
            }
        }

        public void Dispose()
        {
            if (_functions != null)
            {
                _functions.Dispose();
                _functions = null;
            }
        }
    }
}

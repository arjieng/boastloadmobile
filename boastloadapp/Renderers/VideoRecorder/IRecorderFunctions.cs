﻿using System;
using Plugin.Media.Abstractions;

namespace boastloadapp
{
    public interface IRecorderFunctions
    {
        void SetRenderer(IRendererFunctions renderer);
        void SetVideoRecorded(MediaFile mediaFile);
        void SetVideoCompressing(bool compressing);
    }
}

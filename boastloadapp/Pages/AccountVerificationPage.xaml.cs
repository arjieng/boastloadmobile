﻿using System;
using System.Collections.Generic;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

using Xamarin.Forms;

namespace boastloadapp
{
    public partial class AccountVerificationPage : ContentPage
    {
        public AccountVerificationPage()
        {
            InitializeComponent();

            // IphoneX Safe Area
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
    }
}

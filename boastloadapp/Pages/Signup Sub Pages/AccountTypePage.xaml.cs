﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class AccountTypePage : RootViewPage, IFileConnector, IRestConnector
    {
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        DataClass dataClass = DataClass.GetInstance;
        private CancellationTokenSource cts;
        private int serviceType = 0;

        private RadioButton radioAT;

        public AccountTypePage()
        {
            InitializeComponent();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif

            // Getting Account Types
            GetData();
        }

        async void GetData()
        {
            UserDialogs.Instance.ShowLoading(Title = "Loading Account Types...", MaskType.Black);
            serviceType = 1;
            cts = new CancellationTokenSource();
#if DEBUG
                await fileReader.ReadFile("AccountTypes.json", cts.Token);
#elif STAGING
            await restService.GetRequest(string.Format("{0}{1}", Constants.ROOT_URL, Constants.INDUSTRIES_URL), cts.Token);
#endif
            UserDialogs.Instance.HideLoading();
        }

        void OnRequest_Clicked(object sender, EventArgs e)
        {
            switch (int.Parse((sender as Button).StyleId.ToString()))
            {
                case 0: Navigation.PopAsync(); break;
                case 1: 
                    {
                        if (IsForm_Validated())
                        {
                            Constants.temp_user.account_type = new AccountTypeModel() { id = radioAT.RadioID, name = radioAT.Title};
                            Navigation.PushAsync(new IndustrySelectionPage());
                        }
                    }
                    break;
            }
        }

        bool IsForm_Validated()
        {
            bool isValid = true;

            if (radioAT == null)
            {
                isValid = false;
                DisplayAlert(string.Empty, "Must choose account type before you can proceed.", "Go back");
            }
            return isValid;
        }

        void OnRadio_Clicked(object sender, System.EventArgs e)
        {
            RadioButton radio = sender as RadioButton;

            switch (radio.RadioGroup)
            {
                case 0:
                    {
                        if (radioAT != null)
                            radioAT.IsSelected = false;
                        radioAT = radio;
                    }
                    break;
            }
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    switch (serviceType)
                    {
                        case 1:
                            var account_types = JsonConvert.DeserializeObject<ObservableCollection<AccountTypeModel>>(jsonData["account_types"].ToString());
                            listView.ItemsSource = account_types;
                            break;
                    }
                }
            });        
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            DisplayAlert(string.Empty, title, "Back");
#if DEBUG
            System.Diagnostics.Debug.WriteLine("Account Types Page : " + title);
#elif STAGING
            App.Log("Account Types Page : " + title);
#endif        
        }
    }
}

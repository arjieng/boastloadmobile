﻿ using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace boastloadapp
{
    public partial class SubscriptionTierPage : ContentPage
    {
        private RadioButton radioST;

        public SubscriptionTierPage()
        {
            InitializeComponent();
        }

        void OnRequest_Clicked(object sender, EventArgs e)
        {
            switch (int.Parse((sender as Button).StyleId.ToString()))
            {
                case 0: Navigation.PopAsync(); break;
                case 1: Navigation.PushAsync(new PaymentOptionPage()); break;
            }
        }

        void OnRadio_Clicked(object sender, System.EventArgs e)
        {
            RadioButton radio = sender as RadioButton;

            switch (radio.RadioGroup)
            {
                case 1:
                    {
                        if (radioST != null)
                            radioST.IsSelected = false;
                        radioST = radio;
                    }
                    break;
            }
        }
    }
}

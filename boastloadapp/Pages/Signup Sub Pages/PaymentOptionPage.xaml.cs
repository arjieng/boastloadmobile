﻿
using System;
using System.Collections.Generic;
using System.Threading;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class PaymentOptionPage : RootViewPage
    {
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        DataClass dataClass = DataClass.GetInstance;
        private CancellationTokenSource cts;
        private int serviceType = 0;

        private RadioButton radioPP;

        public PaymentOptionPage()
        {
            InitializeComponent();
        }

        void OnRequest_Clicked(object sender, EventArgs e)
        {
            switch (int.Parse((sender as Button).StyleId.ToString()))
            {
                case 0: Navigation.PopAsync(); break;
                case 1:
                    {
#if DEBUG
                        if (radioPP != null)
                        {
                            Constants.temp_user.subscription = radioPP.RadioID;

                            dataClass.user = Constants.temp_user;
                            dataClass.token = "testing_token";
                            //SigninPage.isRegistered = true;
                            //Navigation.PopToRootAsync(false);
                            if (Device.RuntimePlatform == Device.Android)
                                Navigation.PushAsync(new MainPage());
                            else
                                App.ShowMainPage();
                        }
                        else
                            DisplayAlert(string.Empty, "Must select a payment platform.", "Go back");
#elif STAGING
#endif
                    }
                    break;
            }
        }

        void OnRadio_Clicked(object sender, System.EventArgs e)
        {
            RadioButton radio = sender as RadioButton;

            switch (radio.RadioGroup)
            {
                case 3:
                    {
                        if (radioPP != null)
                            radioPP.IsSelected = false;
                        radioPP = radio;
                    }
                    break;
            }
        }
    }
}

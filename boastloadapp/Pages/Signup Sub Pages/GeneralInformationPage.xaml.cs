﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class GeneralInformationPage : ContentPage, IFileConnector, IRestConnector
    {
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        private CancellationTokenSource cts;
        private RadioButton radioAT, radioST, radioI, radioPP;
        private int serviceType = 0;

        private bool isSelectingCountry;

        public GeneralInformationPage()
        {
            InitializeComponent();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif

            if (isSelectingCountry)
            {
                entryCountry.Text = Constants.selected_country.name;
                isSelectingCountry = false;
            }
        }

        async void OnRequest_Clicked(object sender, EventArgs e)
        {
            switch (int.Parse((sender as Button).StyleId.ToString()))
            {
                case 0: await Navigation.PopAsync(); break;
                case 1:
                    {
                        if (IsForm_Validated())
                        {
                            Device.BeginInvokeOnMainThread(async () =>
                            {
                                UserDialogs.Instance.ShowLoading(Title = "Validating...", MaskType.Black);
                                serviceType = 1;
                                cts = new CancellationTokenSource();

                                var json = JsonConvert.SerializeObject(new
                                {
                                    first_name = entryFName.Text,
                                    last_name = entryLName.Text,
                                    email = entryEmail.Text,
                                    password = entryPassword.Text,
                                    username = entryUsername.Text,
                                    country = entryCountry.Text
                                });
#if DEBUG

#elif STAGING
                                await restService.PostRequestAsync(string.Format("{0}{1}", Constants.ROOT_URL, Constants.AUTH_URL), json, cts.Token);
#endif
                                UserDialogs.Instance.HideLoading();
                            });
                            //Navigation.PushAsync(new AccountTypePage());
                        }
                    }
                    break;
            }
        }

        bool IsForm_Validated()
        {
            bool isValid = true;
            List<string> error_message = new List<string>();

            // EMAIL 
            if (string.IsNullOrEmpty(entryEmail.Text))
            {
                isValid = false;
                error_message.Add("Email is required");
            }
            else if (!HelperMethods.IsValidEmail(entryEmail.Text))
            {
                isValid = false;
                error_message.Add("Email is not in valid format");
            }

            if (string.IsNullOrEmpty(entryUsername.Text))
            {
                isValid = false;
                error_message.Add("Username is required");
            }
            else if (!HelperMethods.IsValidUsername(entryUsername.Text))
            {
                isValid = false;
                error_message.Add("Username must contain 6 Characters");
            }

            if (string.IsNullOrEmpty(entryPassword.Text))
            {
                isValid = false;
                error_message.Add("Password is required");
            }            
            else
            {
                if (!HelperMethods.IsValidPassword(entryUsername.Text))
                {
                    isValid = false;
                    error_message.Add("Password must contain 6 Characters");
                }
                else if (!string.Equals(entryPassword.Text, entryRPassword.Text))
                {
                    isValid = false;
                    error_message.Add("Password did not matched");
                }            
            }

            if (string.IsNullOrEmpty(entryFName.Text))
            {
                isValid = false;
                error_message.Add("First name is required");
            }
            else
            {
                if (HelperMethods.IsValidName(entryFName.Text))
                {
                    isValid = false;
                    error_message.Add("First name must be valid");
                }
            }

            if (string.IsNullOrEmpty(entryLName.Text))
            {
                isValid = false;
                error_message.Add("Last name is required");
            }
            else
            {
                if(HelperMethods.IsValidName(entryLName.Text))
                {
                    isValid = false;
                    error_message.Add("Last name must be valid");
                }
            }

            if (string.IsNullOrEmpty(entryCountry.Text))
            {
                isValid = false;
                error_message.Add("Country is required");
            }

            if (string.IsNullOrEmpty(entryPostalCode.Text))
            {
                isValid = false;
                error_message.Add("Postal code is required");
            }

            if(!isValid)
                DisplayAlert(string.Empty, string.Join("\n", error_message), "Go back");

            return isValid;
        }

        void OnEntry_Completed(object sender, System.EventArgs e)
        {
            switch (int.Parse((sender as Xamarin.Forms.Entry).StyleId))
            {
                case 1: entryUsername.Focus(); break;
                case 2: entryPassword.Focus(); break;
                case 3: entryRPassword.Focus(); break;
                case 4: entryFName.Focus(); break;
                case 5: entryLName.Focus(); break;
                case 6: entryCountry.Focus(); break;
                case 7: entryPostalCode.Focus(); break;
                case 8: entryEducation.Focus(); break;
                case 9: OnRequest_Clicked(new Button() { StyleId = "1" }, null); break;

            }
        }

        async void OnCountry_Tapped(object sender, EventArgs e)
        {
            if (!IsBusy && !isSelectingCountry)
            {
                IsBusy = isSelectingCountry = true;
                await Navigation.PushModalAsync(new SelectCountryPage());
                IsBusy = false;
            }
        }

        public async void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                switch (serviceType)
                {
                    case 1:
                        {
                            //Constants.ExtractCountries(JsonConvert.DeserializeObject<List<CountryModel>>(jsonData["countries"].ToString()));
                        }
                        break;
                }
                serviceType = 0;
            }
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            DisplayAlert(string.Empty, error, "Back");
#if DEBUG
            Debug.WriteLine("General Information Page : " + title);
#elif STAGING
            App.Log("General Information Page : " + title);
#endif
        }
    }
}

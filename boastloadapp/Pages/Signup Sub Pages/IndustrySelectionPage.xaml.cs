﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class IndustrySelectionPage : RootViewPage, IFileConnector, IRestConnector
    {
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        DataClass dataClass = DataClass.GetInstance;
        private CancellationTokenSource cts;
        private int serviceType = 0;

        private List<RadioButton> selected_radiobuttons = new List<RadioButton>();
        //private RadioButton radioI;

        public IndustrySelectionPage()
        {
            InitializeComponent();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif

            // Getting Industries
            GetData();

            if (!Constants.temp_user.account_type.name.Contains("Premium"))
                btnNext.Text = "Signup";

            //switch(Constants.temp_user.account_type.id)
            //{
            //    case 1:
            //    case 3:
            //        btnNext.Text = "Signup";
            //        break;
            //}
        }

        async void GetData()
        {
            UserDialogs.Instance.ShowLoading(Title = "Loading Industries...", MaskType.Black);
            serviceType = 1;
            cts = new CancellationTokenSource();
#if DEBUG
                await fileReader.ReadFile("Industries.json", cts.Token);
#elif STAGING
            await restService.GetRequest(string.Format("{0}{1}", Constants.ROOT_URL, Constants.INDUSTRIES_URL), cts.Token);
#endif
            UserDialogs.Instance.HideLoading();
        }

        void OnRequest_Clicked(object sender, EventArgs e)
        {
            switch (int.Parse((sender as Button).StyleId.ToString()))
            {
                case 0: Navigation.PopAsync(); break;
                case 1:
                    {
                        if (selected_radiobuttons.Count <= 3 && selected_radiobuttons.Count != 0)
                        {
                            Constants.temp_user.industries = HelperMethods.ConvertToIndustriesOL(selected_radiobuttons);

                            if(Constants.temp_user.account_type.name.Contains("Premium"))
                                Navigation.PushAsync(new SubscriptionPage());
                            else
                            {
#if DEBUG
                                dataClass.user = Constants.temp_user;
                                dataClass.token = "testing_token";
                                //SigninPage.isRegistered = true;
                                //Navigation.PopToRootAsync(false);

                                if (Device.RuntimePlatform == Device.Android)
                                    Navigation.PushAsync(new MainPage());
                                else
                                    App.ShowMainPage();
#elif STAGING
#endif
                            }
//                            switch (Constants.temp_user.account_type.id)
//                            {
//                                case 2:
//                                case 4:
//                                case 5:
//                                    Navigation.PushAsync(new SubscriptionPage());
//                                    break;
//                                case 1:
//                                case 3:
//                                    {
//#if DEBUG
//                                        dataClass.user = Constants.temp_user;
//                                        dataClass.token = "testing_token";
//                                        SigninPage.isRegistered = true;
//                                        Navigation.PopToRootAsync(false);
//#elif STAGING
//#endif
                            //        }
                            //        break;
                            //}
                        }
                        else
                            DisplayAlert(string.Empty, "You should choose at least 3 industries.", "Go back");
                    }
                    break;
            }
        }

        void OnRadio_Clicked(object sender, System.EventArgs e)
        {
            RadioButton radio = sender as RadioButton;

            if (!selected_radiobuttons.Contains(radio))
            {
                if (selected_radiobuttons.Count < 3)
                {
                    selected_radiobuttons.Add(radio);
                }
                else
                {
                    radio.IsSelected = !radio.IsSelected;
                    DisplayAlert(string.Empty, "You can only choose 3.", "Go back");
                }
            }
            else
            {
                selected_radiobuttons.Remove(radio);
            }
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    switch(serviceType)
                    {
                        case 1:
                            var industries = JsonConvert.DeserializeObject<ObservableCollection<IndustryModel>>(jsonData["industries"].ToString());
                            listView.FlowItemsSource = industries;
                            break;
                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
                DisplayAlert(string.Empty, title, "Back");
#if DEBUG
            System.Diagnostics.Debug.WriteLine("Industry Selection Page : " + title);
#elif STAGING
            App.Log("Industry Selection Page : " + title);
#endif        
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class ViewVideoBoastPage : RootViewPage, IFileConnector, IRestConnector
    {
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        int serviceType = 0;
        DataClass dataClass = DataClass.GetInstance;
        private CancellationTokenSource cts;
        ObservableCollection<CommentModel> comments;

        private VideoBoastModel videoBoast;

        public ViewVideoBoastPage(VideoBoastModel boast)
        {
            InitializeComponent();

            this.NavBackgroundColor = Constants.THEME_COLOR;
            this.RightIcon = Constants.CLOSE_BLACK;
            this.RightIconTint = Color.White;

            // Set Video Source
            this.BindingContext = this.videoBoast = boast;
            listView.Header = boast.owner;

            //commentSection.BindingContext = dataClass;
            this.RightButtonCommand = new Command((obj) =>
            {
                vpDisplay?.StartRunning(false);
                Navigation.PopModalAsync();
            });

            vpDisplay?.StartRunning(true);
            vpDisplay.Source = VideoSource.FromUri(this.videoBoast.link);

        }


        protected async override void OnAppearing()
        {
            base.OnAppearing();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif

            serviceType = 1;
            cts = new CancellationTokenSource();
#if DEBUG
            try
            {
                await fileReader.ReadFile("Comments.json", cts.Token);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
#elif STAGING
            try{
            }
            catch(Exception e){
                Debug.WriteLine(e);
            }
#endif
        }

        void Comment_Tapped(object sender, TappedEventArgs e){
            comments.Insert(0, new CommentModel { comment = sender.ToString(), user = dataClass.user, id = dataClass.user.id });
        }

        void OnUser_Tapped(object sender, System.EventArgs e)
        {
            //FeedModel feed = sender as FeedModel;

            Navigation.PushModalAsync(new ViewUserProfilePage(this.videoBoast.user) { IsOwner = false }, true);
        }

        void OnPlayer_UpdateStatus(object sender, System.EventArgs e)
        {
            if (vpDisplay.Status == VideoStatus.NotReady)
            {

            }
            else if (vpDisplay.Status == VideoStatus.Paused)
            {

            }
            else if (vpDisplay.Status == VideoStatus.Playing)
            {
                int percentage = Convert.ToInt32((Math.Round(vpDisplay.Position.TotalSeconds) / 60) * 100);
                double progress = (percentage == 100) ? 1.0 : (percentage <= 9) ? Convert.ToDouble(string.Format(".0{0}", percentage)) : Convert.ToDouble(string.Format(".{0}", percentage));
                lblPercentage.Text = string.Format("{0}%", percentage);
                pbPercentage.ProgressTo(progress, 500, Easing.Linear);
            }
        }


        //void Comment_Tapped(object sender, TappedEventArgs e){
        //    if(!string.IsNullOrEmpty(commentEntry.Text)){
        //        comments.Insert(0, new CommentModel { comment = commentEntry.Text, user = dataClass.user, id = 1 });
        //        commentEntry.Text = "Add a public comment...";
        //    }
        //}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    switch (serviceType)
                    {
                        case 1:
                            comments = JsonConvert.DeserializeObject<ObservableCollection<CommentModel>>(jsonData["comments"].ToString());
                            listView.FlowItemsSource = comments;
                            //commentsListView.ItemsSource = comments;
                            break;
                    }
                }
                else
                {
                }

                serviceType = 0;
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {

            DisplayAlert(string.Empty, title, "Back");
#if DEBUG
            System.Diagnostics.Debug.WriteLine("Dummy Page : " + title);
#elif STAGING
            App.Log("Dummy Page : " + title);
#endif
        }
    }
}

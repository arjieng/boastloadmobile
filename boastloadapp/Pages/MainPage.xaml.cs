﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using CarouselView.FormsPlugin.Abstractions;
using Syncfusion.ListView.XForms;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace boastloadapp
{
    public partial class MainPage : ContentPage
    {
        private ObservableCollection<TabPageModel> pages = new ObservableCollection<TabPageModel>();
        //private ObservableCollection<int> tabs = new ObservableCollection<int>();
        private int recentTabIndex, activeTabIndex;

        public MainPage()
        {
            InitializeComponent();

            // IphoneX Safe Area
            //On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            // Change Status Bar Color
            if(Device.RuntimePlatform == Device.iOS)
                DependencyService.Get<IStatusStyle>().StatusStyle(0); 

            List<int> tabs = new List<int>();
            tabs.Add(1);
            tabs.Add(2);
            tabs.Add(3);
            tabs.Add(4);
            tabs.Add(5);
            tabs.Add(6);
            tabs.Add(7);
            tabs.Add(8);
            tabs.Add(9);
            tabs.Add(10);
            cvTabPages.ItemsSource = tabs;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            NewRecordingView.hideRecorder?.Invoke(this, true);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            NewRecordingView.hideRecorder?.Invoke(this, false);
        }

        async void OnTab_Tapped(object sender, System.EventArgs e)
        {
            TabImage img = sender as TabImage;

            cvTabPages.Position = int.Parse(img.StyleId);
        }

        void OnCarouselView_PositionSelected(object sender, CarouselView.FormsPlugin.Abstractions.PositionSelectedEventArgs e)
        {
            TabImage img = GetTabImage(e.NewValue);

            TabImage recentImage = GetTabImage(recentTabIndex);
            if (recentImage != null)
            {
                recentImage.IsSelected = false;
                recentTabIndex = int.Parse(img.StyleId.ToString());
                activeTabIndex = e.NewValue;
            }
            img.IsSelected = true;
        }


        TabImage GetTabImage(int index)
        {
            return (this.FindByName<TabImage>(string.Format("tab{0}", index)) as TabImage);
        }
    }
}

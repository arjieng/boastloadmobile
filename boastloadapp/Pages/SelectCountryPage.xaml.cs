﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class SelectCountryPage : RootViewPage, IFileConnector
    {
        private FileReader fileReader = FileReader.GetInstance;

        DataClass dataClass = DataClass.GetInstance;
        private CancellationTokenSource cts;
        private int serviceType = 0;

        public SelectCountryPage()
        {
            InitializeComponent();
            fileReader.FileReaderDelegate = this;

            Constants.selected_country = new CountryModel();

            this.NavBackgroundColor = Constants.THEME_COLOR;
            this.RightIcon = Constants.CLOSE_BLACK;
                this.RightIconTint = Color.White;
            this.RightButtonCommand = new Command((obj) =>
            {
                Navigation.PopModalAsync();
            });

            GetData();
        }

        void OnCountry_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            Constants.selected_country = e.Item as CountryModel;

            Navigation.PopModalAsync();
        }

        async void GetData()
        {
            UserDialogs.Instance.ShowLoading(Title = "Loading Countries...", MaskType.Black);
            serviceType = 1;
            cts = new CancellationTokenSource();

            await fileReader.ReadFile("Countries.json", cts.Token);

            UserDialogs.Instance.HideLoading();
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                switch (serviceType)
                {
                    case 1:
                        var countries = JsonConvert.DeserializeObject<ObservableCollection<CountryModel>>(jsonData["countries"].ToString());
                        listView.ItemsSource = countries;
                        break;
                }
            });    
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            DisplayAlert(string.Empty, title, "Back");
#if DEBUG
            System.Diagnostics.Debug.WriteLine("Select Country Page : " + title);
#elif STAGING
            App.Log("Signin Page : " + title);
#endif
        }
    }
}

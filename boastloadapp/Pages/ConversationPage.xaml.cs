﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class ConversationPage : RootViewPage, IFileConnector, IRestConnector
    {
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        int serviceType = 0;
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts;
        UserModel _user;
        ConversationModel a;

        public ConversationPage(UserModel _user)
        {
            InitializeComponent();
            this._user = _user;

            this.NavBackgroundColor = Constants.THEME_COLOR;
            this.RightIcon = Constants.CLOSE_BLACK;
            this.RightIconTint = Color.White;
            this.PageTitleImage = _user.avatar;
            this.PageTitle = _user.username;
            this.TitleFontColor = Color.White;

            this.RightButtonCommand = new Command((obj) =>
            {
                Navigation.PopModalAsync();
            });

            a = new ConversationModel();

            a.Messages.Add(new MessageModel { Text = "Whahahaha", User = "Sample" });
            a.Messages.Add(new MessageModel { Text = "Whohoho", User = "Wala" });
            conversationListView.ItemsSource = a.Messages;


        }

        void Message_Tapped(object sender, EventArgs e){
            a.Messages.Add(new MessageModel { Text = sender.ToString(), User = "Sample" });
            conversationListView.ScrollTo(conversationListView.ItemsSource.Cast<object>().LastOrDefault(), ScrollToPosition.MakeVisible, true);
        }

        void Editor_Focused(object sender, FocusEventArgs e){
            
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif

            serviceType = 1;
            cts = new CancellationTokenSource();

            try{
#if DEBUG
                await fileReader.ReadFile("Comments.json", cts.Token);
#elif STAGING

#endif
            }catch (Exception e){
                Debug.WriteLine(e);
            }

        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                //Debug.WriteLine(jsonData);
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            DisplayAlert(string.Empty, title, "Back");
#if DEBUG
            System.Diagnostics.Debug.WriteLine("Dummy Page : " + title);
#elif STAGING
            App.Log("Dummy Page : " + title);
#endif
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace boastloadapp
{
    public partial class CommentsPage : RootViewPage, IFileConnector, IRestConnector
    {
        private ObservableCollection<CommentModel> partialComments = new ObservableCollection<CommentModel>();
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        private CancellationTokenSource cts;
        private int serviceType = 0;

        public CommentsPage()
        {
            InitializeComponent();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif
            //this.NavBackgroundColor = Color.White;
            this.RightIcon = Constants.CLOSE_BLACK;
            this.RightButtonCommand = new Command((obj) =>
            {
                Navigation.PopModalAsync();
            });

            // IphoneX Safe Area
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            // Getting Data
            GetData();

            listViewComments.ItemsSource = partialComments;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        private void GetData()
        {
            serviceType = 1;
            cts = new CancellationTokenSource();
#if DEBUG
            fileReader.ReadFile("PartialComments.json", cts.Token);
#elif STAGING
            restService.WebServiceDelegate = this;
#endif
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            switch (serviceType)
            {
                case 1:
                    {
                        partialComments = JsonConvert.DeserializeObject<ObservableCollection<CommentModel>>(jsonData["partial_comments"].ToString());
                    }
                    break;
            }
            serviceType = 0;
        }

        public void ReceiveTimeoutError(string title, string error)
        {
#if DEBUG
            Debug.WriteLine("Comments Page: " + title);
#elif STAGING
            App.Log("Comments Page: " + title);
#endif        
        }
    }
}

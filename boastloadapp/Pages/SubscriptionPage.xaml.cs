﻿using System;
using System.Collections.Generic;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

using Xamarin.Forms;

namespace boastloadapp
{
    public partial class SubscriptionPage : ContentPage
    {
        private List<SubscriptionModel> listFreelancer = new List<SubscriptionModel>();
        private List<SubscriptionModel> listTO = new List<SubscriptionModel>();
        private List<SubscriptionModel> listSJ = new List<SubscriptionModel>();

        SubscriptionListView sample = new SubscriptionListView();
        public SubscriptionPage()
        {
            InitializeComponent();

            // IphoneX Safe Area
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            // Setup List
            SetupList();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        void SetupList()
        {
            listFreelancer.Add(new SubscriptionModel() { title = "1 year", sub = "(unlimited job submissions)", price = "149" });
            listFreelancer.Add(new SubscriptionModel() { title = "Pay as you go", sub = "(submit to interviews of your choice)", price = "10" });
            listViewFreelancer.ItemsSource = listFreelancer;

            listTO.Add(new SubscriptionModel() { title = "Self Employed", price = "The Lone Ranger Plan" });
            listTO.Add(new SubscriptionModel() { title = "1-10 Employees", price = "The Mighty Mouse Plan" });
            listTO.Add(new SubscriptionModel() { title = "11-15 Employees", price = "The Little Giants Plan" });
            listTO.Add(new SubscriptionModel() { title = "51-200 Employees", price = "The Crusaders Plan" });
            listTO.Add(new SubscriptionModel() { title = "201-500 Employees", price = "The Omega Plan" });
            listTO.Add(new SubscriptionModel() { title = "501-1000 Employees", price = "The Smoking Gun Plan" });
            listTO.Add(new SubscriptionModel() { title = "1001-5000 Employees", price = "The All American Plan" });
            listTO.Add(new SubscriptionModel() { title = "5001-10000 Employees", price = "The Holywood Plan" });
            listTO.Add(new SubscriptionModel() { title = "10,001 + Employees", price = "The King Kong Plan" });
            listViewTO.ItemsSource = listTO;

            listSJ.Add(new SubscriptionModel() { title = "The Lone Ranger Plan", price = "100" });
            listSJ.Add(new SubscriptionModel() { title = "The Mighty Mouse Plan", price = "200" });
            listSJ.Add(new SubscriptionModel() { title = "The Little Giants Plan", price = "300" });
            listSJ.Add(new SubscriptionModel() { title = "The Crusaders Plan", price = "400" });
            listSJ.Add(new SubscriptionModel() { title = "The Omega Plan", price = "500" });
            listSJ.Add(new SubscriptionModel() { title = "The Smoking Gun Plan", price = "600" });
            listSJ.Add(new SubscriptionModel() { title = "The All American Plan", price = "700" });
            listSJ.Add(new SubscriptionModel() { title = "The Hollywood Plan", price = "800" });
            listSJ.Add(new SubscriptionModel() { title = "The King Kong Plan", price = "1000" });
            listViewSJ.ItemsSource = listSJ;
        }

        void OnHandler_Scrolled(object sender, Xamarin.Forms.ScrolledEventArgs e)
        {

        }

        void OnRequest_Clicked(object sender, EventArgs e)
        {
            switch (int.Parse((sender as Button).StyleId.ToString()))
            {
                case 0: Navigation.PopAsync(); break;
            }
        }

        void OnList_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            //throw new NotImplementedException();
            Navigation.PushAsync(new PaymentOptionPage());
        }
    }
}

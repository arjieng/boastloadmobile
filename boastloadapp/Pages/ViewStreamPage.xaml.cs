﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace boastloadapp
{
    public partial class ViewStreamPage : RootViewPage
    {
        public ViewStreamPage(string videoPath)
        {
            InitializeComponent();

            this.NavBackgroundColor = Constants.THEME_COLOR;
            this.RightIcon = Constants.CLOSE_BLACK;
            this.RightIconTint = Color.White;
            this.RightButtonCommand = new Command((obj) =>
            {
                Navigation.PopModalAsync();
            });

            // Set Video Source
            vpDisplay.Source = videoPath;
        }
    }
}

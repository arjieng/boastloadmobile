﻿using System;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.MediaManager;
using Plugin.MediaManager.Abstractions.Enums;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace boastloadapp
{
    public partial class DummyPage : ContentPage, IFileConnector, IRestConnector
    {
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        DataClass dataClass = DataClass.GetInstance;
        private CancellationTokenSource cts;
        private int serviceType = 0;

        private Type currentPage;

        public DummyPage()
        {
            InitializeComponent();

            // IphoneX Safe Area
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif

            serviceType = 1;
            cts = new CancellationTokenSource();
#if DEBUG
            fileReader.ReadFile("TestUser.json", cts.Token);
#elif STAGING

#endif
        }

        async void OnRequest_Clicked(object sender, System.EventArgs e)
        {
            Button button = sender as Button;

            switch(int.Parse(button.StyleId))
            {
                case 1:
                    await Navigation.PushAsync(new SigninPage(), false);
                    break;
                case 2:
                    await Navigation.PushAsync(new GeneralInformationPage(), false);
                    break;
                case 3:
                    await Navigation.PushAsync(new AccountVerificationPage(), false);
                    break;
                case 4:
                    await Navigation.PushAsync(new SubscriptionPage(), false);
                    break;
                case 5:
                    await Navigation.PushAsync(new MainPage(), false);
                    break;
                case 6:
                    await Navigation.PushModalAsync(new ApplicationsPage(), false);
                    break;
                case 7:
                    await Navigation.PushAsync(new PostJobPage(), false);
                    break;
                case 8:
                    await Navigation.PushModalAsync(new ViewVideoBoastPage(new VideoBoastModel() { link = "http://206.189.223.233:53011/rails/active_storage/disk/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaEpJaDFTUm1od04ybzRhVkpGYVdwT1JtbGlVVGhCVkhaaE4wb0dPZ1pGVkE9PSIsImV4cCI6IjIwMTgtMDctMTBUMDI6MzY6MTcuNDY4WiIsInB1ciI6ImJsb2Jfa2V5In19--06438124003062cdb3136ecfcbd650d574c18247/boast-%23%7Bid%7D.mp4?content_type=video%2Fmp4&disposition=inline%3B+filename%3D%22boast-%2523%257Bid%257D.mp4%22%3B+filename%2A%3DUTF-8%27%27boast-%2523%257Bid%257D.mp4" }));
                    break;
            }

        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    switch (serviceType)
                    {
                        case 1:
                            {
                                var user = JsonConvert.DeserializeObject<UserModel>(jsonData["data"].ToString());
                                dataClass.user = user;
                                //dataClass.token = jsonData["user"]["token"].ToString();

                            }
                            break;
                    }
                }
                else
                {
                }

                serviceType = 0;
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            DisplayAlert(string.Empty, title, "Back");
#if DEBUG
            System.Diagnostics.Debug.WriteLine("Dummy Page : " + title);
#elif STAGING
            App.Log("Dummy Page : " + title);
#endif
        }
    }
}

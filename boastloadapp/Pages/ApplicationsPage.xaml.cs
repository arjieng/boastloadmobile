﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class ApplicationsPage : RootViewPage, IFileConnector, IRestConnector
    {
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        private CancellationTokenSource cts;
        private RadioButton radioAT, radioST, radioI, radioPP;
        private int serviceType = 0;

        public ApplicationsPage()
        {
            InitializeComponent();

#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif

            this.NavBackgroundColor = Constants.THEME_COLOR;
            this.RightIcon = Constants.CLOSE_BLACK;
            this.RightIconTint = Color.White;
            this.RightButtonCommand = new Command((obj) =>
            {
                Navigation.PopModalAsync();
            });

            // Getting Data
            GetData();
        }

        void GetData()
        {
            serviceType = 1;
            cts = new CancellationTokenSource();
#if DEBUG
            fileReader.ReadFile("Applications.json", cts.Token);
#elif STAGING
            restService.WebServiceDelegate = this;
#endif
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                switch (serviceType)
                {
                    case 1:
                        {
                            var applicants = JsonConvert.DeserializeObject<ObservableCollection<ApplicantsModel>>(jsonData["applicants"].ToString());
                            listView.ItemsSource = applicants;                        
                        }
                        break;
                }
                serviceType = 0;
            }        
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            DisplayAlert(string.Empty, error, "Back");
#if DEBUG
            Debug.WriteLine("General Information Page : " + title);
#elif STAGING
            App.Log("General Information Page : " + title);
#endif        
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace boastloadapp
{
    public partial class SigninPage : ContentPage, IFileConnector, IRestConnector
    {
        public static bool isRegistered;
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        DataClass dataClass = DataClass.GetInstance;
        private CancellationTokenSource cts;
        private int serviceType = 0;

        public SigninPage()
        {
            InitializeComponent();

            // Check logged-in accounts
            IsLogIn();

            // IphoneX Safe Area
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif

            // Clear Temporary User || Temporary
            Constants.temp_user = new UserModel();

            if (isRegistered)
            {
                isRegistered = false;

                if (Device.RuntimePlatform == Device.Android)
                    Navigation.PushAsync(new MainPage());
                else
                    App.ShowMainPage();
            }
            else
                contentHolder.Opacity = 1;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        async void OnRequest_Tapped(object sender, System.EventArgs e)
        {
            Label label = sender as Label;

            switch (int.Parse(label.StyleId))
            {
                case 1:
                    await Navigation.PushAsync(new GeneralInformationPage(), true);
                    contentHolder.Opacity = 0;
                    break;
                case 2: break;
                case 3: OnSignin_Clicked(this, null); break;
            }
        }

        void OnEntry_Completed(object sender, System.EventArgs e)
        {
            switch(int.Parse((sender as Xamarin.Forms.Entry).StyleId))
            {
                case 1: entryPassword.Focus(); break;
                case 2: OnSignin_Clicked(this, null); break;
            }
        }

        async void OnSignin_Clicked(object sender, EventArgs e)
        {
            bool isValid = true;
            List<string> error_message = new List<string>();

            if (string.IsNullOrEmpty(entryUsername.Text))
            {
                isValid = false;
                error_message.Add("Username is required");
            }

            if (string.IsNullOrEmpty(entryPassword.Text))
            {
                isValid = false;
                error_message.Add("Password is required");
            }

            if (isValid && !IsBusy)
            {
                IsBusy = true;

                Device.BeginInvokeOnMainThread(async () =>
                {
                    UserDialogs.Instance.ShowLoading(Title = "Signing in", MaskType.Black);
                    serviceType = 1;
                    cts = new CancellationTokenSource();

                    var json = JsonConvert.SerializeObject(new
                    {
                        username = entryUsername.Text,
                        password = entryPassword.Text
                    });

#if DEBUG
                    await fileReader.ReadFile("TestUser.json", cts.Token);
#elif STAGING
                    await restService.PostRequestAsync(string.Format("{0}{1}{2}", Constants.ROOT_URL, Constants.AUTH_URL, Constants.SIGNIN_URL), json, cts.Token);
#endif
                    IsBusy = false;
                    UserDialogs.Instance.HideLoading();
                });
            }
            else
                DisplayAlert(string.Empty, string.Join("\n", error_message), "Go back");
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    switch (serviceType)
                    {
                        case 1:
                            {
                                var user = JsonConvert.DeserializeObject<UserModel>(jsonData["data"].ToString());
                                dataClass.user = user;

                                if(Device.RuntimePlatform == Device.Android)
                                    await Navigation.PushAsync(new MainPage());
                                else
                                    App.ShowMainPage();
                            }
                            break;
                    }
                }
                else
                {
                }

                serviceType = 0;
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            DisplayAlert(string.Empty, error, "Back");
#if DEBUG
            System.Diagnostics.Debug.WriteLine("Signin Page : " + error);
#elif STAGING
            App.Log("Signin Page : " + error);
#endif
        }

        async void IsLogIn()
        {            if (!string.IsNullOrEmpty(dataClass.token))
            {
                await Navigation.PushModalAsync(new MainPage(), false);

                await Task.Delay(TimeSpan.FromSeconds(5.0));
                this.Opacity = 1;
            }
            else
            {
                this.Opacity = 1;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class CompileBoastPage : RootViewPage, IFileConnector, IRestConnector
    {
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        DataClass dataClass = DataClass.GetInstance;
        private CancellationTokenSource cts;
        private int serviceType = 0;
        private ObservableCollection<Stream> videos = new ObservableCollection<Stream>();

        public CompileBoastPage(ObservableCollection<Stream> getVideos)
        {
            InitializeComponent();

            this.PageTitle = "Compile Videos";
            this.TitleFontFamily = Constants.LATO_MEDIUM;
            this.TitleFontColor = Color.White;
            this.LeftIcon = Constants.ARROW_LEFT_THIN;
            //this.RightIcon = Constants.ARROW_LEFT_THIN;

            this.LeftButtonCommand = new Command((obj) =>
            {
                Navigation.PopModalAsync();
            });

            this.videos = getVideos;

            // Compile Videos
            CompileVideos();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        void OnPlayer_UpdateStatus(object sender, System.EventArgs e)
        {
            if (vpDisplay.Status == VideoStatus.NotReady)
            {
                
            }
            else if (vpDisplay.Status == VideoStatus.Paused)
            {

            }
            else if (vpDisplay.Status == VideoStatus.Playing)
            {

            }        
        }

        private async void CompileVideos()
        {
#if DEBUG
#elif STAGING
            try
            {
                //UserDialogs.Instance.ShowLoading("Compiling Videos, please wait...", MaskType.Black);
                lvVideo.Text = "Compiling Videos. Please wait...";
                serviceType = 1;
                cts = new CancellationTokenSource();

                var url = string.Format("{0}{1}{2}", Constants.ROOT_URL, Constants.ACCOUNT_URL, Constants.BOASTS_URL);
                await restService.MultiPartDataContentAsync_Default(url, "videos", videos, cts.Token, 0);
                //UserDialogs.Instance.HideLoading();
            }
            catch
            {

            }                                                  
#endif
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    switch (serviceType)
                    {
                        case 1:
                            {
                                var boast = JsonConvert.DeserializeObject<VideoBoastModel>(jsonData["boast"].ToString());
                                vpDisplay.Source = VideoSource.FromUri(boast.video_url);
                                lvVideo.IsVisible = false;
                                //vpDisplay?.StartRunning(true);
                                //vpDisplay.Source = VideoSource.FromUri("https://clips.vorwaerts-gmbh.de/VfE_html5.mp4");
                            }
                            break;
                    }
                }
                else
                {
                }

                serviceType = 0;
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {

            //DisplayAlert(string.Empty, error, "Back");
#if DEBUG
            System.Diagnostics.Debug.WriteLine("Compile Boast Page : " + error);
#elif STAGING
            App.Log("Compile Boast Page : " + error);
#endif
        }
    }
}

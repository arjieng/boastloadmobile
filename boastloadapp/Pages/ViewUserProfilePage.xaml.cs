﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace boastloadapp
{
    public partial class ViewUserProfilePage : RootViewPage
    {
        public static readonly BindableProperty IsOwnerProperty = BindableProperty.Create(nameof(IsOwner), typeof(bool), typeof(ViewUserProfilePage), false, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as ViewUserProfilePage;
            view.gridAccountInfo.IsVisible = view.IsOwner;
        });
        public bool IsOwner
        {
            get { return (bool)GetValue(IsOwnerProperty); }
            set { SetValue(IsOwnerProperty, value); }
        }

        public ViewUserProfilePage(UserModel user)
        {
            InitializeComponent();

            // IphoneX Safe Area
            //On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            this.NavBackgroundColor = Constants.THEME_COLOR;
            this.RightIcon = Constants.CLOSE_BLACK;
            this.RightIconTint = Color.White;
            this.RightButtonCommand = new Command((obj) => 
            {
                Navigation.PopModalAsync();
            });

            BindingContext = viewProfileHeader.User = user;
        }
    }
}

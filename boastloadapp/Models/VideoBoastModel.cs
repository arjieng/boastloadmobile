﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace boastloadapp
{
    public class VideoBoastModel : INotifyPropertyChanged
    {
        // Main
        private int _id, _video_aspect_ratio, _your_rating, _score;
        private string _video_url, _thumbnail_url, _caption;
        private UserModel _owner;
        private bool _has_rating;

        public int id
        {
            set { _id = value; OnPropertyChanged(); }
            get { return _id; }
        }  

        public int video_aspect_ratio
        {
            set { _video_aspect_ratio = value; OnPropertyChanged(); }
            get { return _video_aspect_ratio; }
        }  

        public string video_url
        {
            set { _video_url = value; OnPropertyChanged(); }
            get { return string.Format("{0}{1}", Constants.URL, _video_url); }
        }  

        public string thumbnail_url
        {
            set { _thumbnail_url = value; OnPropertyChanged(); }
            get { return string.Format("{0}{1}", Constants.URL, _thumbnail_url); }
        }  

        public string caption
        {
            set { _caption = value; OnPropertyChanged(); }
            get { return _caption; }
        }  

        public UserModel owner
        {
            set { _owner = value; OnPropertyChanged(); }
            get { return _owner; }
        }  

        public int your_rating
        {
            set { _your_rating = value; OnPropertyChanged(); }
            get { return _your_rating; }
        }  

        public int score
        {
            set { _score = value; OnPropertyChanged(); }
            get { return _score; }
        }  

        public bool has_rating
        {
            get { return your_rating > 0; }
        }  

        // Old
        private string _link, _preview, _date;
        private double _rating;
        private bool _isRated;
        private UserModel _user;

        public string link
        {
            set { _link = value; OnPropertyChanged(); }
            get { return _link; }
        }  

        public string preview
        {
            set { _preview = value; OnPropertyChanged(); }
            get { return _preview; }
        }  

        public double rating
        {
            set { _rating = value; OnPropertyChanged(); }
            get { return _rating; }
        }  

        public string date
        {
            set { _date = value; OnPropertyChanged(); }
            get { return _date; }
        }  

        public bool isRated
        {
            set { _isRated = value; OnPropertyChanged(); }
            get { return _isRated; }
        }  

        public UserModel user
        {
            set { _user = value; OnPropertyChanged(); }
            get { return _user; }
        }  

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

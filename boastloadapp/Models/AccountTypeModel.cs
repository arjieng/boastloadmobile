﻿using System;
namespace boastloadapp
{
    public class AccountTypeModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string user_type { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }

    }
}

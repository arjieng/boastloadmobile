﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;

namespace boastloadapp
{
    public class StreamModel : INotifyPropertyChanged
    {
        private int _id;
        private string _path;
        private bool _isEmpty, _hasWatermark;
        private Stream _stream;

        public int id { get { return _id; } set { _id = value; OnPropertyChanged(); } }
        public string path
        {
            get { return _path; }
            set
            {
                _path = value;
                isEmpty = string.IsNullOrEmpty(_path);
                OnPropertyChanged();
            }
        }
        public bool isEmpty
        {
            set { _isEmpty = value; OnPropertyChanged(); }
            get { return string.IsNullOrEmpty(path); }
        }

        public bool hasWatermark
        {
            set { _hasWatermark = value; OnPropertyChanged(); }
            get { return _hasWatermark; }
        }

        public Stream stream
        {
            set { _stream = value; OnPropertyChanged(); }
            get { return _stream; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

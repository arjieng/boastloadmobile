﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace boastloadapp
{
    public class FeedModel : INotifyPropertyChanged
    {
        // Main

        // Old
        private int _id;
        private UserModel _user;
        private VideoBoastModel _boast;

        public int id
        {
            set { _id = value; OnPropertyChanged(); }
            get { return _id; }
        }  

        public UserModel user
        {
            set { _user = value; OnPropertyChanged(); }
            get { return _user; }
        }  

        public VideoBoastModel boast
        {
            set { _boast = value; OnPropertyChanged(); }
            get { return _boast; }
        }  

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

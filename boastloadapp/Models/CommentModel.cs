﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace boastloadapp
{
    public class CommentModel : INotifyPropertyChanged
    {
        private int _id;
        private string _comment;
        private UserModel _user;

        public int id
        {
            set { _id = value; OnPropertyChanged(); }
            get { return _id; }
        }  

        public string comment
        {
            set { _comment = value; OnPropertyChanged(); }
            get { return _comment; }
        }  

        public UserModel user
        {
            set { _user = value; OnPropertyChanged(); }
            get { return _user; }
        }  

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

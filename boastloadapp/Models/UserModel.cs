﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace boastloadapp
{
    public class UserModel : INotifyPropertyChanged
    {
        private string _email, _username, _password, _first_name, _last_name, _country, _country_code, _postal_code, _avatar, _education;
        private int _id, _role, _subscription, _following, _followers, _monthly, _weekly, _score, _likes, _is_verified, _submissions, _payment_platform;
        private double _latitude, _longitude;
        private AccountTypeModel _account_type;
        private ObservableCollection<IndustryModel> _industries = new ObservableCollection<IndustryModel>();

        public string email
        {
            set { _email = value; OnPropertyChanged(); }
            get { return _email; }
        } 

        public string username
        {
            set { _username = value; OnPropertyChanged(); }
            get { return _username; }
        } 

        public string password
        {
            set { _password = value; OnPropertyChanged(); }
            get { return _password; }
        } 

        public string first_name
        {
            set { _first_name = value; OnPropertyChanged(); }
            get { return _first_name; }
        } 

        public string last_name
        {
            set { _last_name = value; OnPropertyChanged(); }
            get { return _last_name; }
        } 

        public string country
        {
            set { _country = value; OnPropertyChanged(); }
            get { return _country; }
        }

        public string country_code
        {
            set { _country_code = value; OnPropertyChanged(); }
            get { return _country_code; }
        } 

        public string postal_code
        {
            set { _postal_code = value; OnPropertyChanged(); }
            get { return _postal_code; }
        } 

        public string avatar
        {
            set { _avatar = value; OnPropertyChanged(); }
            get { return !string.IsNullOrEmpty(_avatar) ? _avatar : Constants.DEFAULT_USER_AVATAR; }
        } 

        //public string industry
        //{
        //    set { _industry = value; OnPropertyChanged(); }
        //    get { return _industry; }
        //} 

        public string education
        {
            set { _education = value; OnPropertyChanged(); }
            get { return _education; }
        } 

        //public string community
        //{
        //    set { _community = value; OnPropertyChanged(); }
        //    get { return _community; }
        //} 

        public int id
        {
            set { _id = value; OnPropertyChanged(); }
            get { return _id; }
        } 

        public int role
        {
            set { _role = value; OnPropertyChanged(); }
            get { return _role; }
        } 

        //public int account
        //{
        //    set { _account = value; OnPropertyChanged(); }
        //    get { return _account; }
        //} 

        public int subscription
        {
            set { _subscription = value; OnPropertyChanged(); }
            get { return _subscription;   }
        } 

        //public int type
        //{
        //    set { _type = value; OnPropertyChanged(); }
        //    get { return _type;   }
        //} 

        public int following
        {
            set { _following = value; OnPropertyChanged(); }
            get { return _following; }
        }  

        public int followers
        {
            set { _followers = value; OnPropertyChanged(); }
            get { return _followers; }
        }    

        public int monthly
        {
            set { _monthly = value; OnPropertyChanged(); }
            get { return _monthly; }
        }    

        public int weekly
        {
            set { _weekly = value; OnPropertyChanged(); }
            get { return _weekly; }
        }   

        public int score
        {
            set { _score = value; OnPropertyChanged(); }
            get { return _score; }
        }   
          
        public int likes
        {
            set { _likes = value; OnPropertyChanged(); }
            get { return _likes; }
        }   

        public int is_verified
        {
            set { _is_verified = value; OnPropertyChanged(); }
            get { return _is_verified; }
        }   

        public int submissions
        {
            set { _submissions = value; OnPropertyChanged(); }
            get { return _submissions; }
        }   

        public int payment_platform
        {
            set { _payment_platform = value; OnPropertyChanged(); }
            get { return _payment_platform; }
        }   

        public double latitude
        {
            set { _latitude = value; OnPropertyChanged(); }
            get { return _latitude; }
        }  

        public double longitude
        {
            set { _longitude = value; OnPropertyChanged(); }
            get { return _longitude; }
        }  

        public AccountTypeModel account_type
        {
            set { _account_type = value; OnPropertyChanged(); }
            get { return _account_type; }
        }

        public ObservableCollection<IndustryModel> industries
        {
            set { _industries = value; OnPropertyChanged(); }
            get { return _industries; }
        }

        public string convert_industries
        {
            get 
            { 
                List<string> result = new List<string>();

                foreach (var industry in industries)
                    result.Add(industry.name);

                return string.Join("/", result);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace boastloadapp
{
    public interface IRestService
    {
        Task PostRequestAsync(string url, string dictionary, CancellationToken ct);
        Task GetRequest(string url, CancellationToken ct);
        Task PutRequestAsync(string url, string dictionary, CancellationToken ct);
        Task DeleteRequestAsync(string url, CancellationToken ct);
        Task MultiPartDataContentAsync(string url, string key, string dictionary, System.IO.Stream image);
    }
}

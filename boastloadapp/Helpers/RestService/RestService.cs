﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace boastloadapp
{
    public class RestServices
    {
        NetworkHelper networkHelper = NetworkHelper.GetInstance;
        HttpClient client;

        static RestServices _restService;
        WeakReference<IRestConnector> _webServiceDelegate;
        public IRestConnector WebServiceDelegate
        {
            get
            {
                IRestConnector webServiceDelegate;
                return _webServiceDelegate.TryGetTarget(out webServiceDelegate) ? webServiceDelegate : null;
            }

            set
            {
                _webServiceDelegate = new WeakReference<IRestConnector>(value);
            }
        }

        public static RestServices GetInstance
        {
            get { if (_restService == null) _restService = new RestServices(); return _restService; }
        }


        public RestServices()
        {
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.MaxResponseContentBufferSize = 256000;

            // Default Headers
            client.DefaultRequestHeaders.Add("access-token", DataClass.GetInstance.token);
            client.DefaultRequestHeaders.Add("uid", DataClass.GetInstance.uid);
            client.DefaultRequestHeaders.Add("client", DataClass.GetInstance.clientId);
        }

        public async Task GetRequest(string url, CancellationToken ct, int type = 0)
        {
            App.Log("Request URL: " + url);
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable() == true)
                {
                    if (type == 1)
                    {

                        //client.DefaultRequestHeaders.Add("access-token", "FZSrRR7tQtEcAK_9ZGjnIg");
                        //client.DefaultRequestHeaders.Add("uid", "user_001@boastload.com");
                        //client.DefaultRequestHeaders.Add("client", "YaVt0Bq-nXoWksAab9NSQA");
                    }

                    var uri = new Uri(url);

                    HttpResponseMessage response = await client.GetAsync(uri, ct);

                    await RequestAsync(response, ct);
                }
                else
                {
                    WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Rentury cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }
        }

        public async Task PostRequestAsync(string url, string dictionary, CancellationToken ct, int type = 0)
        {
            App.Log("URL: " + url);
            App.Log("Contents: " + dictionary);
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable() == true)
                {
                    var uri = new Uri(url);
                    var content = new StringContent(dictionary, Encoding.UTF8, "application/json");
                    content.Headers.Add("access-token", DataClass.GetInstance.token);
                    content.Headers.Add("uid", DataClass.GetInstance.uid);
                    content.Headers.Add("client", DataClass.GetInstance.clientId);
                    HttpResponseMessage response = await client.PostAsync(uri, content, ct);

                    await RequestAsync(response, ct);
                }
                else
                {
                    WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Rentury cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }

        }

        async Task RequestAsync(HttpResponseMessage response, CancellationToken ct)
        {
            var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                App.Log("Response: " + result);
                // ===========
                if (response.Headers.Contains("access-token"))
                {
                    var token = response.Headers.GetValues("access-token").FirstOrDefault();
                    DataClass.GetInstance.token = token;
                    App.Log("Token: " + DataClass.GetInstance.token);
                }

                if (response.Headers.Contains("client"))
                {
                    var clientId = response.Headers.GetValues("client").FirstOrDefault();
                    DataClass.GetInstance.clientId = clientId;
                    App.Log("ClientId: " + DataClass.GetInstance.clientId);
                }

                if (response.Headers.Contains("uid"))
                {
                    var uid = response.Headers.GetValues("uid").FirstOrDefault();
                    DataClass.GetInstance.uid = uid;
                    App.Log("Uid: " + DataClass.GetInstance.uid);
                }

                // ===========
                WebServiceDelegate?.ReceiveJSONData(JObject.Parse(result), ct);
            }
            else
            {
                //300, 404, 500]
                var error_result = JObject.Parse(result);
                string errors = string.Empty;

                if (error_result["errors"] != null)
                {
                    var list = JsonConvert.DeserializeObject<List<string>>(error_result["errors"].ToString());
                    errors = string.Join("\n", list);
                }
                else
                    errors += error_result["error"].ToString();

                WebServiceDelegate?.ReceiveTimeoutError("Error!", errors);
            }
        }

        public async Task PutRequestAsync(string url, string dictionary, CancellationToken ct)
        {
            App.Log("URL: " + url);
            App.Log("Contents: " + dictionary);
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable() == true)
                {
                    var uri = new Uri(url);
                    var content = new StringContent(dictionary, Encoding.UTF8, "application/json");

                    HttpResponseMessage response = await client.PutAsync(uri, content, ct);

                    await RequestAsync(response, ct);
                }
                else
                {
                    WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Rentury cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }

        }

        public async Task DeleteRequestAsync(string url, CancellationToken ct)
        {
            App.Log("URL: " + url);
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable() == true)
                {
                    var uri = new Uri(url);

                    HttpResponseMessage response = await client.DeleteAsync(uri, ct);

                    await RequestAsync(response, ct);
                }
                else
                {
                    WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Rentury cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }

        }

        public async Task MultiPartDataContentAsync(string url, string key, string dictionary, System.IO.Stream image = null, CancellationToken ct = new CancellationToken(), int type = 0, string key1 = "token", string key2 = "image")
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable() == true)
                {
                    var uri = new Uri(url);
                    var json = JObject.Parse(dictionary)[key];
                    var tokenJson = JObject.Parse(dictionary)[key1];
                    var multipartFormData = new MultipartFormDataContent();

                    foreach (var obj in json)
                    {
                        string[] keys = obj.Path.Split('.');

                        if (keys[1].ToString() == key2 && image != null)
                        {
                            string name = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
                            //StreamContent content = new StreamContent(file.GetStream());
                            StreamContent content = new StreamContent(image);
                            content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { FileName = "\"upload-image.jpeg\"", Name = name };
                            //content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/jpeg");
                            multipartFormData.Add(content);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(obj.First.ToString()))
                            {
                                string keyName = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
                                StringContent content = new StringContent(obj.First.ToString(), System.Text.Encoding.UTF8);
                                content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = keyName };
                                multipartFormData.Add(content);
                            }

                        }

                    }


                    if (key1 == "device")
                    {
                        foreach (var obj in tokenJson)
                        {
                            string[] keys = obj.Path.Split('.');
                            if (!string.IsNullOrEmpty(obj.First.ToString()))
                            {
                                string key1Name = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
                                StringContent content = new StringContent(obj.First.ToString(), System.Text.Encoding.UTF8);
                                content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = key1Name };
                                multipartFormData.Add(content);
                            }
                        }

                    }
                    else
                    {
                        string tokenName = '"' + key1 + '"';
                        StringContent contentTokens = new StringContent(tokenJson.ToString(), System.Text.Encoding.UTF8);
                        contentTokens.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = tokenName };
                        multipartFormData.Add(contentTokens);
                    }


                    HttpResponseMessage response = type == 0 ? await client.PostAsync(uri, multipartFormData, ct) : await client.PutAsync(uri, multipartFormData, ct);

                    await RequestAsync(response, ct);
                }
                else
                {
                    WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Rentury cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }

        }

        public async Task MultiPartDataContentAsync_Default(string url, string key, ObservableCollection<System.IO.Stream> media, CancellationToken ct = new CancellationToken(), int type = 0)
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable() == true)
                {
                    var uri = new Uri(url);
                    var multipartFormData = new MultipartFormDataContent();

                    multipartFormData.Headers.Add("access-token", DataClass.GetInstance.token);
                    multipartFormData.Headers.Add("uid", DataClass.GetInstance.uid);
                    multipartFormData.Headers.Add("client", DataClass.GetInstance.clientId);

                    foreach (var medium in media)
                    {
                        StreamContent content = new StreamContent(medium);
                        string name = "\"" + key + "[" + "]" + "\"";

                        content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { FileName = "\"upload-video.mp4\"", Name = name };
                        multipartFormData.Add(content);
                    }
                    HttpResponseMessage response = type == 0 ? await client.PostAsync(uri, multipartFormData, ct) : await client.PutAsync(uri, multipartFormData, ct);

                    await RequestAsync(response, ct);
                }
                else
                {
                    WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Boastload cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }

        }
    }
}

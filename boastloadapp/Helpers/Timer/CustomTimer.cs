﻿using System;
using System.Threading.Tasks;

namespace boastloadapp
{
    public class CountdownTimer
    {
        private int seconds, limit;

        public CountdownTimer(int limit)
        {
            this.seconds = this.limit = limit;
        }

        private bool _isRunning;
        public bool IsRunning
        {
            get { return _isRunning; }
            set { _isRunning = value; }
        }

        public event EventHandler<int> Elapsed;
        protected virtual void OnTimerElapsed()
        {
            if (Elapsed != null)
                Elapsed(this, seconds);
        }

        public void Reset(int seconds = 0)
        {

            this.IsRunning = false;
            this.seconds = (seconds == 0) ? this.limit : seconds;
        }

        public async void Start()
        {
            IsRunning = true;
            while (IsRunning)
            {
                if (seconds != -1)
                {
                    OnTimerElapsed();
                }
                await Task.Delay(1000);
                seconds--;
            }
        }

        public void Stop()
        {
            this.IsRunning = false;
            this.seconds = 0;
        }
    }
}

﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace boastloadapp
{
    public class FileReader : IFileReader
    {
        static FileReader fileReader;
        WeakReference<IFileConnector> _fileReaderDelegate;
        public IFileConnector FileReaderDelegate
        {
            get
            {
                IFileConnector fileReaderDelegate;
                return _fileReaderDelegate.TryGetTarget(out fileReaderDelegate) ? fileReaderDelegate : null;
            }

            set
            {
                _fileReaderDelegate = new WeakReference<IFileConnector>(value);
            }
        }

        public static FileReader GetInstance
        {
            get
            {
                if (fileReader == null)
                    fileReader = new FileReader();

                return fileReader;
            }

        }

        public async Task ReadFile(string fileName, CancellationToken ct)
        {
            var assembly = typeof(FileReader).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream("boastloadapp.Files." + fileName);
            using (var reader = new System.IO.StreamReader(stream))
            {
                var json = await reader.ReadToEndAsync();
                FileReaderDelegate?.ReceiveJSONData(JObject.Parse(json), ct);
            }
        }

        public async Task WriteFile(string fileName, string json, CancellationToken ct)
        {
            //IFolder rootFolder = FileSystem.Current.LocalStorage;
            //IFolder folder = await rootFolder.CreateFolderAsync("ProjectFolder", CreationCollisionOption.OpenIfExists);
            //IFile file = await folder.CreateFileAsync(fileName, CreationCollisionOption.ReplaceExisting);
            //await file.WriteAllTextAsync(json);
        }
    }
}

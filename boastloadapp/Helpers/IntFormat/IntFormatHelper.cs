﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace boastloadapp
{
    public class IntFormatHelper : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = string.Empty;

            try
            {
                result = Constants.IntFormatter(int.Parse(value.ToString()), 0);
            }
            catch
            {
                result = value.ToString();
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = string.Empty;

            try
            {
                Constants.IntFormatter(int.Parse(value.ToString()), 0);
            }
            catch
            {
                result = value.ToString();
            }

            return result;
        }
    }
}

﻿using System;
using Xamarin.Forms;

namespace boastloadapp
{
    public class ChatTemplateSelector : DataTemplateSelector
    {
        DataTemplate incomingDataTemplate;
        DataTemplate outgoingDataTemplate;

        public ChatTemplateSelector()
        {
            this.incomingDataTemplate = new DataTemplate(typeof(IncomingViewCell));
            this.outgoingDataTemplate = new DataTemplate(typeof(OutgoingViewCell));
        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            var messageVm = item as MessageModel;
            if (messageVm == null)
                return null;
            return (messageVm.User == "Sample") ? outgoingDataTemplate : incomingDataTemplate;
        }
    }
}

﻿using System;
using Xamarin.Forms;

namespace boastloadapp
{
    public class TabPageDataTemplateSelectors : DataTemplateSelector
    {
        public DataTemplate page1 { get; private set; }
        public DataTemplate page2 { get; private set; }
        public DataTemplate page3 { get; private set; }
        public DataTemplate page4 { get; private set; }
        public DataTemplate page5 { get; private set; }
        public DataTemplate page6 { get; private set; }
        public DataTemplate page7 { get; private set; }
        public DataTemplate page8 { get; private set; }
        public DataTemplate page9 { get; private set; }
        public DataTemplate page10 { get; private set; }

        public TabPageDataTemplateSelectors()
        {
            page1 = new DataTemplate(typeof(BoastFeedView));
            page2 = new DataTemplate(typeof(WeeklyBoastView));
            page3 = new DataTemplate(typeof(NewRecordingView));
            //page3 = new DataTemplate(typeof(DummyView));
            page4 = new DataTemplate(typeof(BoastActivityView));
            page5 = new DataTemplate(typeof(MessagesView));
            page6 = new DataTemplate(typeof(FindHashtagView));
            page7 = new DataTemplate(typeof(FriendSearchView));
            page8 = new DataTemplate(typeof(GeographicalUsersView));
            page9 = new DataTemplate(typeof(RankingView));
            page10 = new DataTemplate(typeof(VerifiedView));

        }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            DataTemplate selected = new DataTemplate();
            int tab = (int)item;

            switch (tab)
            {
                case 1: selected = page1; break;
                case 2: selected = page2; break;
                case 3: selected = page3; break;
                case 4: selected = page4; break;
                case 5: selected = page5; break;
                case 6: selected = page6; break;
                case 7: selected = page7; break;
                case 8: selected = page8; break;
                case 9: selected = page9; break;
                case 10: selected = page10; break;
                default: selected = page1; break;
            }

            return selected;
        } 
    }
}

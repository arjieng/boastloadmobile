﻿using System;
using System.Threading.Tasks;

namespace boastloadapp
{
    public interface iNetworkHelper
    {
        bool HasInternet();
        Task<bool> IsHostReachable();
    }
}

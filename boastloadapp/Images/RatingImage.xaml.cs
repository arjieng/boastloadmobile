﻿using System;
using System.Collections.Generic;
using Plugin.CrossPlatformTintedImage.Abstractions;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class RatingImage : TintedImage
    {
        public static readonly BindableProperty IsActiveProperty = BindableProperty.Create(nameof(IsActive), typeof(bool), typeof(RatingImage), false, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var image = bindable as RatingImage;

            if (image.IsActive)
            {
                image.Source = "Like_Filled";
                image.AnimateImage();
            }
            else
                image.Source = "Like_Unfilled";
            
            //image.TintColor = Color.White;
        });

        private async void AnimateImage()
        {
            try
            {
                await this.ScaleTo(1.3, 70);
                await this.ScaleTo(1, 70);
            }
            catch
            {
                
            }
        }

        public bool IsActive
        {
            get { return (bool)GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }

        public RatingImage()
        {
            InitializeComponent();
        }

        public event EventHandler Tapped;
        void OnImage_Tapped(object sender, EventArgs e)
        {
            IsActive = true;
            Tapped(this, null);
        }
    }
}

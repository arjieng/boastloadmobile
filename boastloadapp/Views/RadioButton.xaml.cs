﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
namespace boastloadapp
{
    public partial class RadioButton : ContentView
    {
        public int RadioGroup { get; set; }

        public static readonly BindableProperty RadioIDProperty = BindableProperty.Create(nameof(RadioID), typeof(int), typeof(RadioButton), 0, propertyChanged: (bindable, oldValue, newValue) => {});

        public int RadioID
        {
            get { return (int)GetValue(RadioIDProperty); }
            set { SetValue(RadioIDProperty, value); }
        }

        public static readonly BindableProperty TitleProperty = BindableProperty.Create(nameof(Title), typeof(string), typeof(RadioButton), string.Empty, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as RadioButton;
            view.labelTitle.Text = view.Title;
        });
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly BindableProperty SubTitleProperty = BindableProperty.Create(nameof(SubTitle), typeof(string), typeof(RadioButton), string.Empty, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as RadioButton;
            view.labelSubTitle.Text = view.SubTitle;
        });
        public string SubTitle
        {
            get { return (string)GetValue(SubTitleProperty); }
            set { SetValue(SubTitleProperty, value); }
        }

        public static readonly BindableProperty SelectedImageProperty = BindableProperty.Create(nameof(SelectedImage), typeof(string), typeof(RadioButton), string.Empty,
               propertyChanged: (bindable, oldValue, newValue) =>
               {
                    var view = bindable as RadioButton;
               }
          );
        public string SelectedImage
        {
            get { return (string)GetValue(SelectedImageProperty); }
            set { SetValue(SelectedImageProperty, value); }
        }
        public static readonly BindableProperty DefaultImageProperty = BindableProperty.Create(nameof(DefaultImage), typeof(string), typeof(RadioButton), string.Empty, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as RadioButton;
            view.radioImage.Source = view.DefaultImage;
        });
        public string DefaultImage
        {
            get { return (string)GetValue(DefaultImageProperty); }
            set
            {
                SetValue(DefaultImageProperty, value);
            }
        }
        public static readonly BindableProperty IsSelectedProperty = BindableProperty.Create(nameof(IsSelected), typeof(bool), typeof(RadioButton), false, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as RadioButton;
            view.radioImage.Source = view.IsSelected ? view.SelectedImage : view.DefaultImage;
        });
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        //private string _selectedImage;
        //public string SelectedImage
        //{
        //  get
        //  {
        //      return _selectedImage;
        //  }
        //  set
        //  {
        //      _selectedImage = value;
        //  }
        //}
        //private string _defaultImage;
        //public string DefaultImage
        //{
        //  get
        //  {
        //      return _defaultImage;
        //  }
        //  set
        //  {
        //      _defaultImage = value;
        //      radioImage.Source = _defaultImage;
        //              //radioImage.LoadingPlaceholder = _defaultImage;
        //  }
        //}
        //private bool _isSelected;
        //public bool IsSelected
        //{
        //  get
        //  {
        //      return _isSelected;
        //  }
        //  set
        //  {
        //              //tapped();
        //              radioImage.Source = value ? SelectedImage : DefaultImage;
        //              _isSelected = value;
        //  }
        //}
        public RadioButton()
        {
            InitializeComponent();
            IsSelected = false;
            //radioImage.LoadingPlaceholder = DefaultImage;
        }
        public event EventHandler RadioClicked;
        protected void OnRadioClicked()
        {
            RadioClicked(this, null);
        }
        void RadioTapped(object sender, System.EventArgs e)
        {
            OnRadioClicked();
            tapped();
        }
        async void tapped()
        {
            IsSelected = !IsSelected;

            //if (IsSelected)
            //    radioImage.Source = DefaultImage;
            //else
                //radioImage.Source = SelectedImage;

            await radioImage.ScaleTo(1.3, 70);
            await radioImage.ScaleTo(1, 70);


        }
        ///// <summary>
        ///// Default radio behaviour, once se
        ///// </summary>
        //void rbTapped(){
        //}
    }
}
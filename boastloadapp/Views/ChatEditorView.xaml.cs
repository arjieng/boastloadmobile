﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class ChatEditorView : ContentView
    {
        public ChatEditorView()
        {
            InitializeComponent();
            chatTextInput.Focus();
        }

        public event EventHandler MessageTapped;
        void MessageSend_Tapped(object sender, TappedEventArgs e)
        {
            if (!string.IsNullOrEmpty(chatTextInput.Text) && !chatTextInput.Text.Equals(chatTextInput.Placeholder))
                MessageTapped(chatTextInput.Text, null);

            chatTextInput.Text = string.Empty;
        }

        public event EventHandler EditorFocused;
        void Editor_Focused(object sender, FocusEventArgs e){
            EditorFocused(sender, e);
        }
    }
}

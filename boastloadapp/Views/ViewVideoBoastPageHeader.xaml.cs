﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class ViewVideoBoastPageHeader : ContentView
    {
        DataClass dataClass = DataClass.GetInstance;
        public ViewVideoBoastPageHeader()
        {
            InitializeComponent();
            commentSection.BindingContext = dataClass;
        }

        public static readonly BindableProperty UserProperty = BindableProperty.Create(nameof(User), typeof(UserModel), typeof(ViewVideoBoastPageHeader), new UserModel(), propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as ViewVideoBoastPageHeader;
            view.BindingContext = view.User;
        });

        public UserModel User
        {
            get { return (UserModel)GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }

        public event EventHandler CommentTapped;
        void Comment_Tapped(object sender, TappedEventArgs e)
        {
            if (!commentEntry.Text.Equals(commentEntry.Placeholder))
            {
                CommentTapped(commentEntry.Text, e);
                commentEntry.Text = commentEntry.Placeholder;
                commentEntry.Unfocus();
            }
        }
    }
}

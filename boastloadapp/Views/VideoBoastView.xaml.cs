﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class VideoBoastView : ContentView, IFileConnector, IRestConnector
    {
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        int serviceType = 0;
        DataClass dataClass = DataClass.GetInstance;
        private CancellationTokenSource cts;

        public static readonly BindableProperty RatingsProperty = BindableProperty.Create(nameof(Ratings), typeof(double), typeof(VideoBoastView), 0.0, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as VideoBoastView;

            if(view.Ratings >= 1)
                view.rating_1.IsActive = true;
            
            if (view.Ratings >= 2)
                view.rating_2.IsActive = true;
            
            if (view.Ratings >= 3)
                view.rating_3.IsActive = true;

            if (view.Ratings >= 4)
                view.rating_4.IsActive = true;

            if (view.Ratings >= 5)
                view.rating_5.IsActive = true;
        });
        public double Ratings
        {
            get { return (double)GetValue(RatingsProperty); }
            set { SetValue(RatingsProperty, value); }
        }

        public static readonly BindableProperty ImagePreviewProperty = BindableProperty.Create(nameof(ImagePreview), typeof(string), typeof(VideoBoastView), string.Empty, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as VideoBoastView;
            view.imgPreview.Source = view.ImagePreview;
        });
        public string ImagePreview
        {
            get { return (string)GetValue(ImagePreviewProperty); }
            set { SetValue(ImagePreviewProperty, value); }
        }

        public static readonly BindableProperty HasRatingsProperty = BindableProperty.Create(nameof(HasRatings), typeof(bool), typeof(VideoBoastView), true, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as VideoBoastView;
            view.gridRatings.IsVisible = view.HasRatings;
        });
        public bool HasRatings
        {
            get { return (bool)GetValue(HasRatingsProperty); }
            set { SetValue(HasRatingsProperty, value); }
        }

        public static readonly BindableProperty ViewHeightProperty = BindableProperty.Create(nameof(ViewHeight), typeof(double), typeof(VideoBoastView), 0.0, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as VideoBoastView;
            view.HeightRequest = view.imgPreview.HeightRequest = view.ViewHeight.ScaleHeight();
        });
        public double ViewHeight
        {
            get { return (double)GetValue(ViewHeightProperty); }
            set { SetValue(ViewHeightProperty, value); }
        }

        public static readonly BindableProperty ViewWidthProperty = BindableProperty.Create(nameof(ViewWidth), typeof(double), typeof(VideoBoastView), 0.0, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as VideoBoastView;
            view.WidthRequest = view.imgPreview.WidthRequest = view.ViewWidth.ScaleWidth();
        });
        public double ViewWidth
        {
            get { return (double)GetValue(ViewWidthProperty); }
            set { SetValue(ViewWidthProperty, value); }
        }

        public static readonly BindableProperty VideoBoastProperty = BindableProperty.Create(nameof(VideoBoast), typeof(VideoBoastModel), typeof(VideoBoastView), new VideoBoastModel(), propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as VideoBoastView;
            view.BindingContext = view.VideoBoast;
        });
        public VideoBoastModel VideoBoast
        {
            get { return (VideoBoastModel)GetValue(VideoBoastProperty); }
            set { SetValue(VideoBoastProperty, value); }
        }

        public VideoBoastView()
        {
            InitializeComponent();

#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif
        }

        async void OnRating_Tapped(object sender, System.EventArgs e)
        {
            ResetRating();

            RatingImage tapped_image = sender as RatingImage;
            int styleId = int.Parse(tapped_image.StyleId);

            for (int x = 1; x <= 5; x++)
            {
                string name = string.Format("{0}{1}", "rating_", x);
                RatingImage rating_image = gridRatings.FindByName<RatingImage>(name);

                if (x <= styleId)
                    rating_image.IsActive = true;
                else
                    rating_image.IsActive = false;
            }

            await Task.Delay(500);

            var choice = await UserDialogs.Instance.ConfirmAsync(string.Format("You are about to give {0} rank to this video.", styleId), "Rate Video", "Submit", "Cancel");

            if(choice)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    serviceType = 1;
                    cts = new CancellationTokenSource();
#if DEBUG

#elif STAGING
                    string url = string.Format("{0}{1}/{2}{3}", Constants.ROOT_URL, Constants.BOASTS_URL, VideoBoast.id, Constants.RANK_URL);

                    var json = JsonConvert.SerializeObject(new
                    {
                        rating = styleId
                    });

                    await restService.PostRequestAsync(url, json, cts.Token, 1);
#endif
                });
            }
        }

        private void ResetRating()
        {
            rating_1.IsActive = rating_2.IsActive = rating_3.IsActive = rating_4.IsActive = rating_5.IsActive = false;
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    switch (serviceType)
                    {
                        case 1:

                            break;
                    }
                }
                else
                {
                }

                serviceType = 0;
            });        
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            UserDialogs.Instance.Alert(error, string.Empty, "Go Back");
#if DEBUG
            System.Diagnostics.Debug.WriteLine("Video Boast View : " + error);
#elif STAGING
            App.Log("Video Boast View : " + error);
#endif        
        }
    }
}

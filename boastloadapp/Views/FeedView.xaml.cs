﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Acr.UserDialogs;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class FeedView : ContentView
    {
        public static readonly BindableProperty VideoBoastProperty = BindableProperty.Create(nameof(VideoBoast), typeof(VideoBoastModel), typeof(FeedView), new VideoBoastModel(), propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as FeedView;
            view.BindingContext = view.vbvPreview.VideoBoast= view.VideoBoast;

            if(view.VideoBoast != null)
                view.IsRated = view.VideoBoast.has_rating;
        });
        public VideoBoastModel VideoBoast
        {
            get { return (VideoBoastModel)GetValue(VideoBoastProperty); }
            set { SetValue(VideoBoastProperty, value); }
        }

        public static readonly BindableProperty IsRatedProperty = BindableProperty.Create(nameof(IsRated), typeof(bool), typeof(FeedView), false, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as FeedView;
            view.imgLike.TintColor = view.IsRated ? Constants.ACCENT_COLOR : Color.Transparent;
        });


        public bool IsRated
        {
            get { return (bool)GetValue(IsRatedProperty); }
            set { SetValue(IsRatedProperty, value); }
        }

        public event EventHandler LikeTapped;
        async void OnLike_Tapped(object sender, System.EventArgs e)
        {
            //IsRated = !IsRated;


            if (!IsRated)
            {
                var choice = await UserDialogs.Instance.ConfirmAsync("You can only rate this boast once. You cannot change it.", "Rate Video", "Continue", "Cancel");

                if (choice)
                {
                    IsRated = true;

                    await imgLike.ScaleTo(1.3, 70);
                    await imgLike.ScaleTo(1, 70);

                    vbvPreview.HasRatings = true;
                    vbvPreview.Ratings = 0;

                    LikeTapped(new Button() { CommandParameter = VideoBoast.id }, null);
                }
            }
        }

        public event EventHandler TabHammerTapped;
        void TabHammer_Tapped(object sender, TappedEventArgs e){
            TabHammerTapped(VideoBoast, e);
        }

        public event EventHandler SendMessageTapped;
        void OnSendMessage_Tapped(object sender, TappedEventArgs e)
        {
            SendMessageTapped(VideoBoast, null);
            //Navigation.PushAsync(new ConversationPage(), true);
            //Navigation.PushModalAsync(new ConversationPage(){ User = FeedBoast.user }, true);
        }

        public event EventHandler UserProfileTapped;
        void OnUser_Tapped(object sender, System.EventArgs e)
        {
            UserProfileTapped(VideoBoast, null);
        }

        public FeedView()
        {
            InitializeComponent();
        }
    }
}

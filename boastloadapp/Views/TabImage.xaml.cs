﻿using System;
using System.Collections.Generic;
using Plugin.CrossPlatformTintedImage.Abstractions;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class TabImage : TintedImage
    {
        public static readonly BindableProperty IsSelectedProperty = BindableProperty.Create(nameof(IsSelected), typeof(bool), typeof(TabImage), false, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var img = bindable as TabImage;
            img.TintColor = img.IsSelected ? Constants.ACCENT_COLOR : Color.Transparent;
        });
        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }

        public TabImage()
        {
            InitializeComponent();
        }

        public event EventHandler ImageClicked;

        void ImageTapped(object sender, System.EventArgs e)
        {
            ImageClicked(this, null);
        }
    }
}

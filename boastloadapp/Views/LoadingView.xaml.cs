﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class LoadingView : ContentView
    {
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(LoadingView), string.Empty, propertyChanged: (bindable, oldValue, newValue) => 
        {
            var view = bindable as LoadingView;
            view.lblDisplay.Text = view.Text;
        });
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly BindableProperty ActivityIndicatorColorProperty = BindableProperty.Create(nameof(ActivityIndicatorColor), typeof(Color), typeof(LoadingView), Constants.ACCENT_COLOR, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as LoadingView;
            view.aiLoading.Color = view.ActivityIndicatorColor;
        });
        public Color ActivityIndicatorColor
        {
            get { return (Color)GetValue(ActivityIndicatorColorProperty); }
            set { SetValue(ActivityIndicatorColorProperty, value); }
        }

        public static readonly BindableProperty TextColorProperty = BindableProperty.Create(nameof(TextColor), typeof(Color), typeof(LoadingView), Constants.THEME_COLOR, propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as LoadingView;
            view.lblDisplay.TextColor = view.TextColor;
        });
        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }


        public LoadingView()
        {
            InitializeComponent();
        }
    }
}

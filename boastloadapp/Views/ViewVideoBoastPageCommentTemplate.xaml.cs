﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class ViewVideoBoastPageCommentTemplate : ContentView
    {
        public ViewVideoBoastPageCommentTemplate()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty CommentProperty = BindableProperty.Create(nameof(Comment), typeof(CommentModel), typeof(ProfileHeaderView), new CommentModel(), propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as ViewVideoBoastPageCommentTemplate;
            view.BindingContext = view.Comment;
        });

        public CommentModel Comment
        {
            get { return (CommentModel)GetValue(CommentProperty); }
            set { SetValue(CommentProperty, value); }
        }
    }
}

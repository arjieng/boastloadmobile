﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class GeographicalUsersView : ContentView
    {
        private ObservableCollection<PaymentPlatformModel> platforms = new ObservableCollection<PaymentPlatformModel>();

        public GeographicalUsersView()
        {
            InitializeComponent();

            platforms.Add(new PaymentPlatformModel() { image = "Payment_Credit_Debit_Card", image_height = 41 });
            platforms.Add(new PaymentPlatformModel() { image = "Payment_Paypal", image_height = 31 });
            platforms.Add(new PaymentPlatformModel() { image = "Payment_iTunes", image_height = 39 });
            platforms.Add(new PaymentPlatformModel() { image = "Payment_Venmo", image_height = 20 });
            platforms.Add(new PaymentPlatformModel() { image = "Payment_Stripe", image_height = 32 });
            platforms.Add(new PaymentPlatformModel() { image = "Payment_SquareCash", image_height = 25 });

            listViewPM.ItemsSource = platforms;
        }
    }
}

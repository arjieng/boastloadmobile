﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Syncfusion.ListView.XForms;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class NewRecordingView : ContentView
    {
        public static EventHandler<bool> hideRecorder;
        private ObservableCollection<StreamModel> streams = new ObservableCollection<StreamModel>();
        private CountdownTimer recordTimer = new CountdownTimer(10);
        private bool isRecording, isUsingGallery;
        private MediaFile file;
        private int videoProvided = 0;

        public NewRecordingView()
        {
            InitializeComponent();

            hideRecorder = new EventHandler<bool>(OnRecorder_Hidden);
            this.viewRecord.RecordedCommand = new Command((obj) =>
            {
                AddToStreamList(obj as MediaFile);
            });
                                                          
            this.recordTimer.Elapsed += OnTimer_Elapsed;
            hideRecorder += OnRecorder_Hidden;
            /// Initiate ListView
            SetupListView();

        }

        void OnRecorder_Hidden(object sender, bool isAppearing)
        {
            if (isAppearing)
            {
                //this.viewRecord = new VideoRecorder();
                gridRecorderHandler.Children.Add(this.viewRecord);
            }
            else
            {
                if (!isRecording && !isUsingGallery && (Device.RuntimePlatform != Device.iOS))
                    gridRecorderHandler.Children.Remove(this.viewRecord);
            }
        }

        async void OnRecording_Tapped(object sender, EventArgs e)
        {
            Image image = sender as Image;

            switch (int.Parse(image.StyleId))
            {
                case 0:
                    {
                        if (!isRecording)
                        {
                            isRecording = true;
                            viewRecord.StartRecording.Invoke(this, e);

                            recordTimer.Reset(10);
                            recordTimer.Start();

                            imgNewRecord.Opacity = 0;

                            await prRecord.ScaleTo(1.3, 70);
                            await prRecord.ScaleTo(1, 70);

                            await prRecord.ProgressTo(1, 10000, Easing.Linear);

                            viewRecord.StartRecording.Invoke(this, e);
                            /// Stops Timer
                            recordTimer.Stop();
                            imgNewRecord.Opacity = 1;
                            labelTimer.Text = string.Empty;

                            await prRecord.ProgressTo(0, 1000, Easing.Linear);

                            isRecording = false;
                            //prRecord.Progress = 0;
                        }
                        //else
                        //{
                        //    recordTimer.Stop();

                        //    imgNewRecord.Opacity = 1;
                        //    labelTimer.Text = string.Empty;

                        //    await prRecord.ProgressTo(0, 1000, Easing.Linear);
                        //}
                    }
                    break;
                case 2:
                    {
                        await image.ScaleTo(1.3, 70);
                        await image.ScaleTo(1, 70);

                        try
                        {
                            AddToStreamList(await ImportVideoFromGallery());
                        }
                        catch
                        {
                            
                        }
                    }
                    break;
            }
        }

        private async void OnStream_ItemTapped(object sender, Syncfusion.ListView.XForms.ItemTappedEventArgs e)
        {
            StreamModel stream = e.ItemData as StreamModel;
            int index = streams.IndexOf(stream);

            var choice = await UserDialogs.Instance.ActionSheetAsync("Choose", "Cancel", null, null, "View", "Delete");

            switch (choice)
            {
                case "View":
                    {
                        await Navigation.PushModalAsync(new ViewStreamPage(stream.path));
                    }
                    break;
                case "Delete":
                    {
                        videoProvided--;
                        streams[index] = new StreamModel();
                    }
                    break;
            }
        }

        private async void OnStream_ItemDragging(object sender, ItemDraggingEventArgs e)
        {
            if (e.Action == DragAction.Start)
            {
                if (string.IsNullOrEmpty((e.ItemData as StreamModel).path))
                    e.Cancel = true;
            }

            if (e.Action == DragAction.Dragging)
            {

            }

            if (e.Action == DragAction.Drop)
            {

            }
        }

        private async Task<MediaFile> ImportVideoFromGallery()
        {
            isUsingGallery = true;
            if (!CrossMedia.Current.IsPickVideoSupported)
            {
#if DEBUG
                Debug.WriteLine("Permission is not granted.");
#elif STAGING
#endif
                //DisplayAlert(string.Empty, "Permission is not granted.", "Go back");
                return file;
            }
            file = await CrossMedia.Current.PickVideoAsync();

            if (file == null)
                return file;

            isUsingGallery = false;
            return file;
        }

        private void AddToStreamList(MediaFile file)
        {
            videoProvided++;
            for (int index = 0; index < this.streams.Count; index++)
            {
                if (string.IsNullOrEmpty(this.streams[index].path) && this.streams[index].stream == null)
                {
                    this.streams[index].path = file.Path;
                    this.streams[index].stream = file.GetStream();
                    break;
                }
            }
        }

        private void SetupListView()
        {
            /// Item Source
            this.streamList.ItemsSource = this.streams = CreateStreamList();
            /// Update Source when item(s) are dragged
            this.streamList.DragDropController.UpdateSource = true;

            this.streamList.DragDropController = new DragDropControllerExt(this.streamList);
        }

        private ObservableCollection<StreamModel> CreateStreamList()
        {
            ObservableCollection<StreamModel> newList = new ObservableCollection<StreamModel>();
            //path = "/storage/emulated/0/2018510182724.mp4"

            /// Providing slots for streams
            newList.Add(new StreamModel() { id = 1 });
            newList.Add(new StreamModel() { id = 2 });
            newList.Add(new StreamModel() { id = 3 });
            newList.Add(new StreamModel() { id = 4 });
            newList.Add(new StreamModel() { id = 5 });
            newList.Add(new StreamModel() { id = 6 });

            return newList;
        }

        private async void OnTimer_Elapsed(object sender, int e)
        {
            //if(e <= 0)
            //{
                labelTimer.Text = (e).ToString();
                if (e == 1)
                {

                    recordTimer.Stop();

                    //await Task.Delay(1000);

                    //OnRecording_Tapped(new Image() { StyleId = "2" }, null);
                }
            //}
        }

        async void OnCompile_Clicked(object sender, System.EventArgs e)
        {
            if (videoProvided == 6)
            {
                ObservableCollection<System.IO.Stream> videos = new ObservableCollection<System.IO.Stream>();

                foreach (var video in streams)
                    videos.Add(video.stream);
                
                Navigation.PushModalAsync(new CompileBoastPage(videos));
            }
            else
                UserDialogs.Instance.Alert("Must provide at least 6 videos", string.Empty, "Go Back");
        }
    }

    public class DragDropControllerExt : DragDropController
    {
        public DragDropControllerExt(SfListView listView) : base(listView)
        {

        }

        protected override Task<bool> OnLayoutItem(View element, Rectangle rect)
        {
            return element.LayoutTo(rect, 250, Easing.SinIn);
        }
    }
}

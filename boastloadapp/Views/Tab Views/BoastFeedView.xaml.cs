﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class BoastFeedView : ContentView, IFileConnector, IRestConnector
    {
        private ObservableCollection<VideoBoastModel> feeds = new ObservableCollection<VideoBoastModel>();
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
        //private RestServices restService = new RestServices();
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        private DataClass dataClass = DataClass.GetInstance;
        private CancellationTokenSource cts;
        private int serviceType = 0;

        public BoastFeedView()
        {
            InitializeComponent();
#if DEBUG
            fileReader.FileReaderDelegate = this;
            //restService.WebServiceDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
            viewProfileFooter.IsVisible = true;
#endif

            listView.Header = dataClass.user;
            listView.FlowItemsSource = feeds;

            // Getting Data
            GetData();
        }

        private async void GetData()
        {
            Debug.WriteLine("Triggered");
            Device.BeginInvokeOnMainThread(async () =>
            {
                serviceType = 1;
                cts = new CancellationTokenSource();
#if DEBUG
                fileReader.ReadFile("BoastFeed.json", cts.Token);
#elif STAGING
                await restService.GetRequest(string.Format("{0}{1}{2}", Constants.ROOT_URL, Constants.ACCOUNT_URL, Constants.BOASTS_URL), cts.Token, 1);
#endif
            });
        }

        void Items_FlowItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
#if DEBUG == false
            if(feeds.Count != 0){
                if ((e.Item as VideoBoastModel) == feeds[feeds.Count - 1])
                {
                    viewProfileFooter.IsVisible = true;
                    if (current != pages)
                    {
                        Device.BeginInvokeOnMainThread(async () =>
                        {
                            serviceType = 1;
                            cts = new CancellationTokenSource();
                            await restService.GetRequest(string.Format("{0}{1}", Constants.URL, nextURL), cts.Token, 1);
                        });
                    }else{
                        viewProfileFooter.IsVisible = false;
                    }
                }else{
                    viewProfileFooter.IsVisible = false;
                }
            }
#endif
        }

        int current = 0, pages = 0;
        string nextURL = "";
        public async void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Debug.WriteLine(jsonData);
            App.Log(jsonData.ToString());
            Device.BeginInvokeOnMainThread(async () =>
            {
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                switch (serviceType)
                {
                    case 1:
                        {
                            var feedsTEMP = JsonConvert.DeserializeObject<ObservableCollection<VideoBoastModel>>(jsonData["boasts"].ToString());
                            foreach (VideoBoastModel feed in feedsTEMP)
                            {
                                feeds.Add(feed);
                            }
#if DEBUG == false
                                current++;
                                pages = int.Parse(jsonData["pagination"]["pages"].ToString());
                                nextURL = jsonData["pagination"]["next"].ToString();
#endif

                            }
                            break;
                    }
                    serviceType = 0;
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
#if DEBUG
            Debug.WriteLine("Boast Feed View : " + error);
#elif STAGING
            App.Log("Boast Feed View : " + error);
#endif
        }

        void OnProfileHeader_ViewTapped(object sender, System.EventArgs e)
        {
            Navigation.PushModalAsync(new ViewUserProfilePage(dataClass.user) { IsOwner = true }, true);
        }

        void OnFeed_FlowItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            Navigation.PushModalAsync(new ViewVideoBoastPage(e.Item as VideoBoastModel));
        }

        void SendMessage_Tapped(object sender, EventArgs e){
            
            Navigation.PushModalAsync(new ConversationPage(((VideoBoastModel)sender).owner), true);
        }

        void OnLike_Tapped(object sender, System.EventArgs e)
        {
            //throw new NotImplementedException();
        }

        void TabHammer_Tapped(object sender, TappedEventArgs e){
            var boast = (VideoBoastModel)sender;
            //UserDialogs.Instance.ActionSheetAsync("Choose", "Cancel", null, null, "View", "Delete")
            UserDialogs.Instance.Alert("You have challenged " + boast.owner.username, "", "OK");
        }

        void OnUser_Tapped(object sender, System.EventArgs e)
        {
            var boast = sender as VideoBoastModel;
            Navigation.PushModalAsync(new ViewUserProfilePage(boast.owner) { IsOwner = false }, true);
        }
    }
}

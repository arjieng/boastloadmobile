﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class VerifiedView : ContentView, IFileConnector, IRestConnector
    {
        private ObservableCollection<CommentModel> partialComments = new ObservableCollection<CommentModel>();
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        private CancellationTokenSource cts;
        private int serviceType = 0;

        public VerifiedView()
        {
            InitializeComponent();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif
            vbv_1.VideoBoast = new VideoBoastModel() { preview = "https://www.androidcentral.com/sites/androidcentral.com/files/styles/xlarge_wm_brw/public/article_images/2017/10/YouTube-App-Autoplay-Videos_0.JPG?itok=O02U4vKb" };
            vbv_2.VideoBoast = new VideoBoastModel() { preview = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcROMmjPA1bYQ6EEkNhS_6wNFFY1Y3c3Zv_K7cRQN8h8TW1THdrd" };

            // Getting Data
            GetData();

            listViewComments.ItemsSource = partialComments;
        }

        void OnRequest_Clicked(object sender, System.EventArgs e)
        {
            switch(int.Parse((sender as Button).StyleId))
            {
                case 1:
                    Navigation.PushModalAsync(new CommentsPage());
                    break;
            }
        }

        private void GetData()
        {
            serviceType = 1;
            cts = new CancellationTokenSource();
#if DEBUG
            fileReader.ReadFile("PartialComments.json", cts.Token);
#elif STAGING
            restService.WebServiceDelegate = this;
#endif
        }

        public async void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            switch (serviceType)
            {
                case 1:
                    {
                        partialComments = JsonConvert.DeserializeObject<ObservableCollection<CommentModel>>(jsonData["partial_comments"].ToString());
                    }
                    break;
            }
            serviceType = 0;
        }

        public void ReceiveTimeoutError(string title, string error)
        {
#if DEBUG
            Debug.WriteLine("Verified View : " + title);
#elif STAGING
            App.Log("Verified View : " + title);
#endif
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class RankingView : ContentView, IFileConnector, IRestConnector
    {
        private ObservableCollection<VideoBoastModel> videoBoasts = new ObservableCollection<VideoBoastModel>();
#if DEBUG
        private FileReader fileReader = FileReader.GetInstance;
#elif STAGING
        private RestServices restService = new RestServices();
#endif
        private CancellationTokenSource cts;
        private int serviceType = 0;

        public RankingView()
        {
            InitializeComponent();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#elif STAGING
            restService.WebServiceDelegate = this;
#endif
            // Getting Data
            GetData();

            listView.FlowItemsSource = videoBoasts;
        }


        private void GetData()
        {
            serviceType = 1;
            cts = new CancellationTokenSource();
#if DEBUG
            fileReader.ReadFile("VideoBoasts.json", cts.Token);
#elif STAGING
            restService.WebServiceDelegate = this;
#endif
        }

        public async void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            switch (serviceType)
            {
                case 1:
                    {
                        videoBoasts = JsonConvert.DeserializeObject<ObservableCollection<VideoBoastModel>>(jsonData["boasts"].ToString());
                    }
                    break;
            }
            serviceType = 0;
        }

        public void ReceiveTimeoutError(string title, string error)
        {
#if DEBUG
            Debug.WriteLine("Ranking View : " + error);
#elif STAGING
            App.Log("Ranking View : " + error);
#endif
        }
    }
}

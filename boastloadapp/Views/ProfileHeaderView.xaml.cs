﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace boastloadapp
{
    public partial class ProfileHeaderView : ContentView
    {
        public static readonly BindableProperty UserProperty = BindableProperty.Create(nameof(User), typeof(UserModel), typeof(ProfileHeaderView), new UserModel(), propertyChanged: (bindable, oldValue, newValue) =>
        {
            var view = bindable as ProfileHeaderView;
            view.BindingContext = view.User;
        });
        public UserModel User
        {
            get { return (UserModel)GetValue(UserProperty); }
            set { SetValue(UserProperty, value); }
        }

        public event EventHandler ViewTapped;
        void OnView_Tapped(object sender, EventArgs e)
        {
            ViewTapped(this, null);
        }

        public ProfileHeaderView()
        {
            InitializeComponent();
        }
    }
}

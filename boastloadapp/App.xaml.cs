﻿using System;
using System.Collections.Generic;
using System.Threading;
using DLToolkit.Forms.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace boastloadapp
{
    public partial class App : Application
    {
        // Scaling
        public static float screenWidth { get; set; }
        public static float screenHeight { get; set; }
        public static float appScale { get; set; }
        public static double screenScale;

        public App()
        {
            InitializeComponent();

            // FlowListView
            FlowListView.Init(); 

            MainPage = new NavigationPage(new DummyPage());
        }

        public static void ShowMainPage()
        {
            Current.MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public static void Log(string msg, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
        {
            msg = DateTime.Now.ToString("HH:mm:ss tt") + " [Rentury]-[" + memberName + "]: " + msg;
#if DEBUG || STAGING
            System.Diagnostics.Debug.WriteLine(msg);
            DependencyService.Get<ILogServices>().Log(msg);
#else
            DependencyService.Get<ILogServices>().Log(msg);
#endif
        }
    }
}

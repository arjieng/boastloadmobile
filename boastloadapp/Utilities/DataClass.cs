﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;

namespace boastloadapp
{
    public class DataClass : INotifyPropertyChanged
    {
        static DataClass dataClass;
        //public ObservableCollection<Inbox> inbox { get; set; }

        public static DataClass GetInstance
        {
            get
            {
                if (dataClass == null)
                {
                    dataClass = new DataClass();
                }
                return dataClass;
            }
        }

        public bool isLogin = false;


        UserModel _user;
        public UserModel user
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("user") && _user == null)
                {
                    JObject json = JObject.Parse(Application.Current.Properties["user"].ToString());
                    _user = JsonConvert.DeserializeObject<UserModel>(json.ToString());
                }
                return _user;
            }
            set
            {
                _user = value;
                Application.Current.Properties["user"] = JsonConvert.SerializeObject(_user);
                Application.Current.SavePropertiesAsync();
                OnPropertyChanged();
            }
        }


        string _token;
        public string token
        {
            get
            {
                OnPropertyChanged(nameof(token));
                if (Application.Current.Properties.ContainsKey("token") && string.IsNullOrEmpty(_token))
                {
                    _token = Application.Current.Properties["token"]?.ToString();
                }
                return _token;
            }
            set
            {
                _token = value;
                Application.Current.Properties["token"] = _token;
                Application.Current.SavePropertiesAsync();
                OnPropertyChanged();
            }
        }


        string _clientId { get; set; }
        public string clientId
        {
            set
            {
                _clientId = value;
                Application.Current.Properties["client_id"] = _clientId;
                Application.Current.SavePropertiesAsync();
            }
            get
            {
                if (String.IsNullOrEmpty(_clientId) && Application.Current.Properties.ContainsKey("client_id"))
                {
                    _clientId = Application.Current.Properties["client_id"].ToString();
                }
                return _clientId;
            }
        }

        string _uid { get; set; }
        public string uid
        {
            set
            {
                _uid = value;
                Application.Current.Properties["uid"] = _uid;
                Application.Current.SavePropertiesAsync();
            }
            get
            {
                if (String.IsNullOrEmpty(_uid) && Application.Current.Properties.ContainsKey("uid"))
                {
                    _uid = Application.Current.Properties["uid"].ToString();
                }
                return _uid;
            }
        }

        public async void SaveCurrentLocation(Position position)
        {
            user.latitude = position.Latitude;
            user.longitude = position.Longitude;
            //Application.Current.Properties["token"] = _token;
            //Application.Current.Properties["token"] = _token;
            await Save();
        }

        public async void Logout()
        {
            Application.Current.Properties.Remove("token");
            //token = null;
            await Save();
        }

        public async Task Save()
        {
            await Application.Current.SavePropertiesAsync();
            //token = null;
        }

        double _latitude;
        public double latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
                OnPropertyChanged();
            }
        }

        double _longitude;
        public double longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
                OnPropertyChanged();
            }
        }

        bool _hasLocation;
        public bool hasLocation
        {
            get
            {
                return _hasLocation;
            }
            set
            {
                _hasLocation = value;
                OnPropertyChanged();
            }
        }

        string _stripeToken;
        public string stripeToken
        {
            get
            {
                return _stripeToken;
            }
            set
            {
                _stripeToken = value;
                OnPropertyChanged();
            }
        }

        bool _is_admin;
        public bool is_admin
        {
            get
            {
                return _is_admin;
            }
            set
            {
                _is_admin = value;
                OnPropertyChanged();
            }
        }
        //Badges _badges = new Badges() { messages = 0, total = 0, interested = 0 };
        //public Badges badges
        //{
        //    get
        //    {
        //        if (Application.Current.Properties.ContainsKey("badges"))
        //        {
        //            JObject json = JObject.Parse(Application.Current.Properties["badges"].ToString());
        //            _badges = JsonConvert.DeserializeObject<Badges>(json.ToString());
        //        }
        //        return _badges;
        //    }
        //    set
        //    {
        //        _badges = value;
        //        Application.Current.Properties["badges"] = JsonConvert.SerializeObject(_badges);
        //        Application.Current.SavePropertiesAsync();
        //        OnPropertyChanged();
        //    }
        //}

        //public ObservableCollection<Conversation> conversations { get; set; }
        //public ObservableCollection<Interest> notifications { get; set; }
        //public ObservableCollection<Property> properties { get; set; }

        public DataClass()
        {
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName]string name = "")
        {
            if (PropertyChanged == null)
                return;
            PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace boastloadapp
{
    public class Constants : ContentPage
    {
        //NAVIGATION HEIGHT
        public static readonly double NAVIGATION_HEIGHT = Device.RuntimePlatform == Device.iOS ? 54.ScaleHeight() : 46.ScaleHeight();

        // COLORS 
        public static readonly Color THEME_COLOR = Color.FromHex("000000");     // OLD THEME COLOR : AB0B4B
        public static readonly Color ACCENT_COLOR = Color.FromHex("AB0B4B");
        public static readonly Color DIVIDER_COLOR = Color.FromHex("FFFFFF");
        public static readonly Color GRAY_COLOR = Color.FromHex("666666");
        public static readonly Color SILVER_COLOR = Color.FromHex("8F8F8F");
        public static readonly Color LISTVIEW_COLOR = Color.FromHex("ECF0F1");

        // FONTS
        public static readonly string LATO_LIGHT = Device.RuntimePlatform == Device.iOS ? "Lato-Light" : "Fonts/Lato-Light.ttf#Lato Light";
        public static readonly string LATO_MEDIUM = Device.RuntimePlatform == Device.iOS ? "Lato-Medium" : "Fonts/Lato-Medium.ttf#Lato Medium";
        public static readonly string LATO_BOLD = Device.RuntimePlatform == Device.iOS ? "Lato-Bold" : "Fonts/Lato-Bold.ttf#Lato Bold";

        // HEIGHTREQUESTS
        public static readonly double TAB_PAGE_CAROUSELVIEW_HEIGHT = Device.RuntimePlatform == Device.iOS ? 450.ScaleHeight() : 473.ScaleHeight();


        // EXTRAS
        public static UserModel temp_user = new UserModel();
        public static readonly string DEFAULT_USER_AVATAR = "https://storage.googleapis.com/tcg-content/infomania/users/May2017/9rVG0DF36ra0FpKmsu6Q.jpg";
        public static CountryModel selected_country = new CountryModel();

        // ICONS
        public static readonly string CLOSE_BLACK = "Close_Black";
        //public static readonly string LIKE_UNFILLED = "Like_Unfilled";
        public static readonly string HEART_UNFILLED_BLACK = "Heart_Unfilled_Black";
        public static readonly string HEART_FILLED_BLACK = "Heart_Filled_Black";
        public static readonly string ARROW_LEFT_THIN = "Arrow_Left_Thin";

        // METHODS
        public static List<string> COUNTRIES_LIST = new List<string>();
        public static List<string> ExtractCountries(List<CountryModel> countries)
        {
            for (int x = 0; x < countries.Count; x++)
                COUNTRIES_LIST.Add(countries[x].name);

            return COUNTRIES_LIST;
        }

        // INT FORMAT
        public static char[] subCharacters = new char[] { 'K', 'M', 'B', 'T' };
        public static string IntFormatter(int n, int iteration)
        {
            if (n >= 1000)
            {
                int d = (n / 100) / 10;
                bool isRound = (d * 10) % 10 == 0;
                return (d < 1000 ? ((d > 99.9 || isRound || (!isRound && d > 9.99) ? (d * 10 / 10).ToString() : d + string.Empty) + string.Empty + subCharacters[iteration]) : IntFormatter(d, iteration + 1));
            }
            else
                return n.ToString();
        }

        // API
        public static readonly string URL = "http://206.189.223.233:53011";

#if STAGING
        public static readonly string URL = "http://206.189.223.233:53011";
        public static readonly string PORT = "53011";
        public static readonly string ROOT = URL + ":" + PORT;
        public static readonly string ROOT_URL = URL + "/api/v1";
#elif PRODUCTION
        public static readonly string URL = "--please provide--";
        public static readonly string PORT = "443";
        public static readonly string ROOT = URL + ":" + PORT;
        public static readonly string ROOT_URL = URL + "/api/v1";
#endif


#if DEBUG == false
        public static readonly string AUTH_URL = "/auth";
        public static readonly string SIGNIN_URL = "/sign_in";
        public static readonly string INDUSTRIES_URL = "/industries";
        public static readonly string ACCOUNT_URL = "/account";
        public static readonly string BOASTS_URL = "/boasts";
        public static readonly string RANK_URL = "/rank";

#endif
    }
}


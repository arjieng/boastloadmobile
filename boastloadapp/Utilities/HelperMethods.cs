﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace boastloadapp
{
    public class HelperMethods
    {
        // CHECK EMAIL IF VALID
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        // CHECK USERNAME IF VALID
        public static bool IsValidUsername(string username)
        {
            if (username.Length >= 6)
                return true;
            else
                return false;
        }

        // CHECK USERNAME IF VALID
        public static bool IsValidPassword(string password)
        {
            if (password.Length >= 6)
                return true;
            else
                return false;
        }

        // CHECK NAME IF VALID
        public static bool IsValidName(string name)
        {
            return Regex.IsMatch(name, @"^[A-Za-z]+[\s][A-Za-z]+[.][A-Za-z]+$");
        }

        public static ObservableCollection<IndustryModel> ConvertToIndustriesOL(List<RadioButton> list)
        {
            ObservableCollection<IndustryModel> result = new ObservableCollection<IndustryModel>();

            foreach(var radiobutton in list)
                result.Add(new IndustryModel() { id = radiobutton.RadioID, name = radiobutton.Title });
            
            return result;
        }
    }
}

﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using FFImageLoading.Forms.Platform;
using HockeyApp.Android;
using CarouselView.FormsPlugin.Android;
using Plugin.Permissions;
using Plugin.CurrentActivity;
using Acr.UserDialogs;
using ImageCircle.Forms.Plugin.Droid;
using Plugin.CrossPlatformTintedImage.Android;
using Octane.Xamarin.Forms.VideoPlayer.Android;
using Android;
using System.Threading.Tasks;

namespace boastloadapp.Droid
{
    [Activity(Label = "Boast Load", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        // EventHandlers
        public static event EventHandler<EventArgs> Paused;
        public static event EventHandler<EventArgs> Resumed;
        public static event EventHandler<EventArgs> Destroyed;

        // Field, properties, and method for Video Picker
        public static MainActivity Current { private set; get; }
        public static readonly int PickImageId = 1000;
        public TaskCompletionSource<string> PickImageTaskCompletionSource { set; get; }

        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            // Scaling
            var density = Resources.DisplayMetrics.Density;
            App.screenWidth = Resources.DisplayMetrics.WidthPixels / density;
            App.screenHeight = (Resources.DisplayMetrics.HeightPixels / density) - 26;
            App.appScale = density;

            base.OnCreate(bundle);
            global::Xamarin.Forms.Forms.Init(this, bundle);

            // FFImageLoading
            CachedImageRenderer.Init(enableFastRenderer: true);

            // CarouselView
            CarouselViewRenderer.Init();

            // Circle Image
            ImageCircleRenderer.Init();

            /// ACR User Dialogs
            UserDialogs.Init(this);

            // Tinted Image
            TintedImageRenderer.Init();


            // Octane Video Player
            FormsVideoPlayer.Init();

            LoadApplication(new App());

            if (Build.VERSION.SdkInt >= Build.VERSION_CODES.M)
            {
                if (CheckSelfPermission(Manifest.Permission.Camera) != Permission.Granted || CheckSelfPermission(Manifest.Permission.RecordAudio) != Permission.Granted || CheckSelfPermission(Manifest.Permission.ReadExternalStorage) != Permission.Granted || CheckSelfPermission(Manifest.Permission.WriteExternalStorage) != Permission.Granted)
                    RequestPermissions(new String[] { Manifest.Permission.Camera, Manifest.Permission.RecordAudio, Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage }, 1);
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
			PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
             
            switch (requestCode)
            {
                case 1:
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                    {
                        
                    }
                    else
                    {
                        
                    }
                    break;
            }
        }

        public override void OnBackPressed()
        {
            base.OnBackPressed();
        }

        protected override void OnPause()
        {
            base.OnPause();

            var handler = Paused;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        protected override void OnResume()
        {
            base.OnResume();
            CrashManager.Register(this, "b56c245ef4e54f7391ae19207486f627");

            var handler = Resumed;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            var handler = Destroyed;
            if (handler != null)
            {
                handler(this, EventArgs.Empty);
            }
        }
    }
}


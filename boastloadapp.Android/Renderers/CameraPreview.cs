﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Hardware;
using Android.Views;

namespace boastloadapp.Droid
{
    public class CameraPreview : ViewGroup, ISurfaceHolderCallback
    {
        SurfaceView mSurfaceView;
        ISurfaceHolder mHolder;
        Camera.Size mPreviewSize;
        IList<Camera.Size> mSupportedPreviewSizes;
        Camera _camera;
        Context _context;
        SurfaceOrientation _orientation = SurfaceOrientation.Rotation0;
        int _cameraId;

        public Camera PreviewCamera
        {
            get { return _camera; }
            set
            {
                _camera = value;
                if (_camera != null)
                {
                    mSupportedPreviewSizes = PreviewCamera.GetParameters().SupportedPreviewSizes;
                    RequestLayout();
                }
            }
        }

        public int CameraId
        {
            get { return _cameraId; }
            set { _cameraId = value; }
        }

        public SurfaceView Surface
        {
            get { return mSurfaceView; }
        }

        public SurfaceOrientation Orientation
        {
            get { return _orientation; }
        }

        public Camera.Size PreviewSize
        {
            get { return mPreviewSize; }
        }

        public CameraPreview(Context context) : base(context)
        {
            mSurfaceView = new SurfaceView(context);
            AddView(mSurfaceView);

            // Install a SurfaceHolder.Callback so we get notified when the
            // underlying surface is created and destroyed.
            mHolder = mSurfaceView.Holder;
            mHolder.AddCallback(this);
            mHolder.SetType(SurfaceType.PushBuffers);

            _context = context;
        }

        public void SwitchCamera(Camera camera)
        {
            PreviewCamera = camera;

            try
            {
                camera.SetPreviewDisplay(mHolder);
            }
            catch (Java.IO.IOException exception)
            {
                System.Diagnostics.Debug.WriteLine("SetPreview exception {0}", exception.Message);
            }

            Camera.Parameters parameters = camera.GetParameters();
            mPreviewSize = GetOptimalPreviewSize(mSupportedPreviewSizes, 720, 560);

            // get screen orientation
            _orientation = (_context as Activity).WindowManager.DefaultDisplay.Rotation;
            //System.Diagnostics.Debug.WriteLine("Device orientation: {0}", _orientation;
            Camera.CameraInfo camInfo = new Camera.CameraInfo();
            Camera.GetCameraInfo(CameraId, camInfo);

            int degrees = 0;
            switch (_orientation)
            {
                case SurfaceOrientation.Rotation90:
                    degrees = 90;
                    break;
                case SurfaceOrientation.Rotation180:
                    degrees = 180;
                    break;
                case SurfaceOrientation.Rotation270:
                    degrees = 270;
                    break;
                case SurfaceOrientation.Rotation0:
                default:
                    degrees = 0;
                    break;
            }

            int result;
            if (camInfo.Facing == Camera.CameraInfo.CameraFacingFront)
            {
                result = (camInfo.Orientation + degrees) % 360;
                result = (360 - result) % 360;  // compensate the mirror
            }
            else
            {  // back-facing
                result = (camInfo.Orientation - degrees + 360) % 360;
            }
            PreviewCamera.SetDisplayOrientation(result);

            //RequestLayout();

            parameters.SetRotation(result);
            System.Diagnostics.Debug.WriteLine("Setting preview size to " + mPreviewSize.Width + ", " + mPreviewSize.Height);
            parameters.SetPreviewSize(mPreviewSize.Width, mPreviewSize.Height);
            parameters.SetRecordingHint(true);
            parameters.Zoom = 0;

            try
            {
                camera.SetParameters(parameters);
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Camera set parameters failed with error: " + e.Message);
            }
        }

        protected override void OnMeasure(int widthMeasureSpec, int heightMeasureSpec)
        {
            // We purposely disregard child measurements because act as a
            // wrapper to a SurfaceView that centers the camera preview instead
            // of stretching it.
            int width = ResolveSize(SuggestedMinimumWidth, widthMeasureSpec);
            int height = ResolveSize(SuggestedMinimumHeight, heightMeasureSpec);
            SetMeasuredDimension(width, height);
                           
            //System.Diagnostics.Debug.WriteLine("OnMeasure size: {0}, {1}", width, height);

            if (mSupportedPreviewSizes != null)
            {
                mPreviewSize = GetOptimalPreviewSize(mSupportedPreviewSizes, width, height);
                //System.Diagnostics.Debug.WriteLine("OnMeasure: mPreviewSize {0}, {1}", mPreviewSize.Width, mPreviewSize.Height);
            }
        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            if (changed && ChildCount > 0)
            {
                View child = GetChildAt(0);

                int width = r - l;
                int height = b - t;

                //System.Diagnostics.Debug.WriteLine("OnLayout");
                //System.Diagnostics.Debug.WriteLine("\twidth: {0}\theight: {1}", width, height);

                int previewWidth = width;
                int previewHeight = height;
                if (mPreviewSize != null)
                {
                    previewWidth = mPreviewSize.Width;
                    previewHeight = mPreviewSize.Height;
                    //System.Diagnostics.Debug.WriteLine("\tpreview.width: {0}\tpreview.height: {1}", previewWidth, previewHeight);
                }

                //// Center the child SurfaceView within the parent.
                //if (width * previewHeight > height * previewWidth) {
                //  int scaledChildWidth = previewWidth * height / previewHeight;
                //  System.Diagnostics.Debug.WriteLine("adjusting width {0}", scaledChildWidth);
                //  child.Layout((width - scaledChildWidth) / 2, 0,
                //               (width + scaledChildWidth) / 2, height);
                //} else {
                //  int scaledChildHeight = previewHeight * width / previewWidth;
                //  System.Diagnostics.Debug.WriteLine("adjusting height {0}", scaledChildHeight);
                //  child.Layout(0, (height - scaledChildHeight) / 2,
                //               width, (height + scaledChildHeight) / 2);
                //}

                child.Layout(0, 0, width, height);
            }
        }

        public void SurfaceCreated(ISurfaceHolder holder)
        {
            System.Diagnostics.Debug.WriteLine("SurfaceCreated");
            // The Surface has been created, acquire the camera and tell it where
            // to draw.
            try
            {
                if (PreviewCamera != null)
                {
                    PreviewCamera.SetPreviewDisplay(holder);
                }
            }
            catch (Java.IO.IOException exception)
            {
                System.Diagnostics.Debug.WriteLine("SurfaceCreated error: {0}", exception.Message);
            }
        }

        public void SurfaceDestroyed(ISurfaceHolder holder)
        {
            System.Diagnostics.Debug.WriteLine("SurfaceDestroyed");
            // Surface will be destroyed when we return, so stop the preview.
            if (PreviewCamera != null)
            {
                PreviewCamera.StopPreview();
            }
        }

        private Camera.Size GetOptimalPreviewSize(IList<Camera.Size> sizes, int w, int h)
        {
            //System.Diagnostics.Debug.WriteLine("GetOptimalPreviewSize");
            const double ASPECT_TOLERANCE = 0.1;
            double targetRatio = (double)h / w;

            if (sizes == null)
                return null;

            Camera.Size optimalSize = null;
            double minDiff = Double.MaxValue;

            int targetHeight = h;

            // Try to find an size match aspect ratio and size
            foreach (Camera.Size size in sizes)
            {
                double ratio = (double)size.Height / size.Width;

                if (Math.Abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                    continue;

                if (Math.Abs(size.Height - targetHeight) < minDiff)
                {
                    optimalSize = size;
                    minDiff = Math.Abs(size.Height - targetHeight);
                }
            }

            // Cannot find the one match the aspect ratio, ignore the requirement
            if (optimalSize == null)
            {
                minDiff = Double.MaxValue;
                foreach (Camera.Size size in sizes)
                {
                    if (Math.Abs(size.Height - targetHeight) < minDiff)
                    {
                        optimalSize = size;
                        minDiff = Math.Abs(size.Height - targetHeight);
                    }
                }
            }

            System.Diagnostics.Debug.WriteLine("Optimal size: {0}, {1}", optimalSize.Width, optimalSize.Height);
            return optimalSize;
        }

        public void SurfaceChanged(ISurfaceHolder holder, Android.Graphics.Format format, int w, int h)
        {
            // Now that the size is known, set up the camera parameters and begin
            // the preview.
            System.Diagnostics.Debug.WriteLine("SurfaceChanged");
            try
            {
                Camera.Parameters parameters = PreviewCamera.GetParameters();

                // get screen orientation
                _orientation = (_context as Activity).WindowManager.DefaultDisplay.Rotation;
                //System.Diagnostics.Debug.WriteLine("Device orientation: {0}", orientation);

                Camera.CameraInfo camInfo = new Camera.CameraInfo();
                Camera.GetCameraInfo(CameraId, camInfo);

                int degrees = 0;
                switch (_orientation)
                {
                    case SurfaceOrientation.Rotation90:
                        degrees = 90;
                        break;
                    case SurfaceOrientation.Rotation180:
                        degrees = 180;
                        break;
                    case SurfaceOrientation.Rotation270:
                        degrees = 270;
                        break;
                    case SurfaceOrientation.Rotation0:
                    default:
                        degrees = 0;
                        break;
                }

                int result;
                if (camInfo.Facing == Camera.CameraInfo.CameraFacingFront)
                {
                    result = (camInfo.Orientation + degrees) % 360;
                    result = (360 - result) % 360;  // compensate the mirror
                }
                else
                {  // back-facing
                    result = (camInfo.Orientation - degrees + 360) % 360;
                }
                PreviewCamera.SetDisplayOrientation(result);

                RequestLayout();

                parameters.SetRotation(result);
                parameters.SetPreviewSize(mPreviewSize.Width, mPreviewSize.Height);
                parameters.SetRecordingHint(true);
                parameters.Zoom = 0;

                PreviewCamera.SetParameters(parameters);
                PreviewCamera.StartPreview();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("SurfaceChanged exception: {0}", e.Message);
            }
        }
    }
}

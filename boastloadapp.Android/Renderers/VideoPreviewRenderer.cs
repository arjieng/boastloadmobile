﻿using System;
using System.Collections.Generic;
using System.IO;
using Android.Graphics;
using Android.Media;
using Android.Provider;
using Android.Widget;
using boastloadapp;
using boastloadapp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(VideoPreview), typeof(VideoPreviewRenderer))]
namespace boastloadapp.Droid
{
    public class VideoPreviewRenderer : ViewRenderer<VideoPreview, ImageView>, IDisposable
    {
        ImageView _imageView;
        VideoPreview _element;


        public VideoPreviewRenderer()
        {
            
        }

        protected override void OnElementChanged(ElementChangedEventArgs<VideoPreview> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
            {
                System.Diagnostics.Debug.WriteLine("Setting native control");
                _imageView = new ImageView(Forms.Context);
                _imageView.SetBackgroundColor(Android.Graphics.Color.Black);
                _imageView.SetMinimumWidth(100);
                _imageView.SetMinimumHeight(100);
                SetNativeControl(_imageView);
            }

            if (e.OldElement != null)
            {
                // unsubscribe
            }

            if (e.NewElement != null)
            {
                _element = e.NewElement;

                if (!string.IsNullOrEmpty(_element.VideoSource))
                    RetrieveVideoThumbnail(_element.VideoSource);
            }
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "VideoSource" && !string.IsNullOrEmpty(_element.VideoSource))
            {
                System.Diagnostics.Debug.WriteLine("VideoPreviewRenderer:: VideoSource property changed");
                RetrieveVideoThumbnail(_element.VideoSource);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _imageView.Dispose();
            _imageView = null;
        }

        protected async void RetrieveVideoThumbnail(string url)
        {
            //System.Diagnostics.Debug.WriteLine("Retrieving thumbnail");
   //         Bitmap thumb = null;
            //MediaMetadataRetriever mediaMetadataRetriever = null;
            //try
            //{
            //  mediaMetadataRetriever = new MediaMetadataRetriever();
            //  // for SDK >= 14
            //  await mediaMetadataRetriever.SetDataSourceAsync(url, new Dictionary<string, string>());
            //  thumb = mediaMetadataRetriever.GetFrameAtTime(100);

            //}
            //catch (Exception ex)
            //{
            //  System.Diagnostics.Debug.WriteLine("Could not get video thumbnail");
            //  System.Diagnostics.Debug.WriteLine("{0}", ex.Message);
            //}
            //finally
            //{
            //  if (mediaMetadataRetriever != null)
            //  {
            //      mediaMetadataRetriever.Release();
            //  }
            //}





            ///The following is for local files
            using (Bitmap thumbnail = await ThumbnailUtils.CreateVideoThumbnailAsync(_element.VideoSource, ThumbnailKind.MiniKind)) 
            {
                using (MemoryStream memoryStream = new MemoryStream()) 
               {
                    await thumbnail.CompressAsync(Android.Graphics.Bitmap.CompressFormat.Jpeg, 80, memoryStream);
                    _element.Thumbnail = ImageSource.FromStream(() => memoryStream);

                    if (_element.Thumbnail != null)
                    {
                        //MemoryStream memoryStream = new MemoryStream();
                        //await thumb.CompressAsync(Android.Graphics.Bitmap.CompressFormat.Png, 50, memoryStream);
                        //memoryStream.Seek(0, SeekOrigin.Begin);
                        //_element.Thumbnail = ImageSource.FromStream(() => memoryStream);

                        // TODO: Downsize bitmap?
                        _imageView.SetImageBitmap(thumbnail);

                        System.Diagnostics.Debug.WriteLine("thumbnail size: ({0},{1})", thumbnail.Width, thumbnail.Height);
                        System.Diagnostics.Debug.WriteLine("element size: ({0}, {1})", _element.Width, _element.Height);
                        _element.WidthRequest = Math.Max(thumbnail.Width, _element.Width);
                        _element.HeightRequest = (_element.Width / thumbnail.Width) * thumbnail.Height;

                        _element.ThumbnailWidth = thumbnail.Width;
                        _element.ThumbnailHeight = thumbnail.Height;

                        thumbnail.Dispose();
                    }
                }
            }
        }
    }
}

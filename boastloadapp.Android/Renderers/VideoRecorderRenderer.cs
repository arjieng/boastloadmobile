﻿using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Hardware;
using Android.Media;
using Android.OS;
using Android.Provider;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Java.IO;
using Java.Util;
using Plugin.Media.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using System.IO;
using System.Collections.Generic;
using boastloadapp;
using boastloadapp.Droid;

[assembly: ExportRenderer(typeof(VideoRecorder), typeof(VideoRecorderRenderer))]
namespace boastloadapp.Droid
{
#pragma warning disable 0618
    public class VideoRecorderRenderer : ViewRenderer<VideoRecorder, Android.Widget.RelativeLayout>, IRendererFunctions, IDisposable
    {
        #region Private properties
        ///VideoConverter videoConverter = new VideoConverter();

        // UI
        //Android.Widget.Button _recordButton;
        Android.Widget.Button _countdownButton;
        TextView _durationLabel;
        TextView _countdownLabel;
        //ImageView _toggleCameraButton;
        CameraPreview _cameraPreview;
        Camera _camera;
        Android.Support.V7.App.AlertDialog _countdownDialog;
        Android.Widget.RelativeLayout relativeLayout;
        Android.Widget.RelativeLayout _videoPreviewLayout;
        ImageView _videoThumbnail;
        //Android.Widget.Button _retakeButton, _finishButton;

        // timers
        TimeSpan _durationSpan;
        Timer _durationTimer;

        CountDownTimer _countdownTimer;
        int _countdownSeconds = 0;

        // handlers
        Handler durationHandler;
        Handler countdownHandler;

        string[] countdownItems = new string[] {
                    "off", "1s", "2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "10s", "11s", "12s", "13s", "14s", "15s"
    };

        // misc
        IRecorderFunctions _recorder;
        bool _subscribed = false;
        Android.Net.Uri _ringtoneUri;
        Ringtone _ringtone;
        MediaRecorder _mediaRecorder;
        int _numberOfCameras;
        int _defaultCameraId; // first rear facing camera
        int _cameraCurrentlyLocked = -1;    // don't set at first
        bool _recording = false;
        string _filePath;
        int _countdownValue;

        VideoRecorder _element;
        #endregion


        protected override void OnElementChanged(ElementChangedEventArgs<VideoRecorder> e)
        {
            base.OnElementChanged(e);
            System.Diagnostics.Debug.WriteLine("OnElementChanged");

            if (Control == null)
            {
                System.Diagnostics.Debug.WriteLine("Control == null");
            }

            if (e.OldElement != null)
            {
                System.Diagnostics.Debug.WriteLine("e.OldElement != null");
                UnsubscribeFromEvents();
                _element.SizeChanged -= HandleElementSizeChanged;
                _element.SizeChanged -= HandleElementSizeChanged;
                _element.StartRecording -= HandleRecordingButtonClicked;
                _element.RetakeRecording -= _retakeButton_Click;
                _element.SaveRecording -= _finishButton_Click;
                _element.RotateCamera -= (sender, arg) => ToggleCamera();


            }

            if (e.NewElement != null)
            {
                System.Diagnostics.Debug.WriteLine("e.NewElement != null");
                System.Diagnostics.Debug.WriteLine("Size: {0}, {1}", e.NewElement.Width, e.NewElement.Height);

                _element = e.NewElement;
                _element.SizeChanged += HandleElementSizeChanged;
                _element.StartRecording += HandleRecordingButtonClicked;
                _element.RetakeRecording += _retakeButton_Click;
                _element.SaveRecording += _finishButton_Click;
                _element.RotateCamera += (sender, arg) => ToggleCamera();


                if (_element.Height > 0 && _element.Width > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Setting native control because size is not (-1,-1)");
                    _recorder = (IRecorderFunctions)e.NewElement;
                    _recorder.SetRenderer(this);
                    SetControl();
                    SubscribeToEvents();
                }
            }
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "CountdownNotificationEnabled")
            {
                System.Diagnostics.Debug.WriteLine("countdown notification enabled: {0}", Element.CountdownNotificationEnabled);
            }
        }

        void HandleElementSizeChanged(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Setting native control bc event");
            SetControl();

            _recorder = (IRecorderFunctions)_element;
            _recorder.SetRenderer(this);

            SubscribeToEvents();
        }

        void SetControl()
        {
            // camera API
            _numberOfCameras = Camera.NumberOfCameras;

            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            for (int i = 0; i < _numberOfCameras; i++)
            {
                Camera.GetCameraInfo(i, cameraInfo);
                if (cameraInfo.Facing == CameraFacing.Back)
                {
                    _defaultCameraId = i;
                }
            }

            ReleaseCamera();

            InitializeViews();

            LayoutViews();

            SetCameraControl();

            SetNativeControl(relativeLayout);

            // setup handler for duration timer
            durationHandler = new Handler(HandleDurationTimerFired);

            // setup handler for countdown timer
            countdownHandler = new Handler(HandleCountdownTimerFired);

            // setup dialog
            Android.Support.V7.App.AlertDialog.Builder builder = new Android.Support.V7.App.AlertDialog.Builder(Forms.Context);
            builder.SetTitle("Countdown").SetItems(new string[] { "off", "1s", "2s", "3s", "4s", "5s", "6s", "7s", "8s", "9s", "10s", "11s", "12s", "13s", "14s", "15s" }, HandleCountdownValueSelected);

            _countdownDialog = builder.Create();

            // ringtone for beeping sound
            _ringtoneUri = RingtoneManager.GetDefaultUri(RingtoneType.Notification);
            _ringtone = RingtoneManager.GetRingtone(Forms.Context, _ringtoneUri);
        }

        #region Lifecycle
        void SubscribeToEvents()
        {
            if (_subscribed)
            {
                UnsubscribeFromEvents();
            }

            System.Diagnostics.Debug.WriteLine("subscribing");
            MainActivity.Paused += MainActivity_Paused;
            MainActivity.Resumed += MainActivity_Resumed;
            MainActivity.Destroyed += MainActivity_Destroyed;

            //_recordButton.Click += HandleRecordingButtonClicked;
            _countdownLabel.Click += _countdownLabel_Click;
            _countdownButton.Click += _countdownLabel_Click;
            //_finishButton.Click += _finishButton_Click;
            //_videoThumbnail.Click += _videoThumbnail_Click;
            //_retakeButton.Click += _retakeButton_Click;

            _subscribed = true;
        }

        void UnsubscribeFromEvents()
        {
            System.Diagnostics.Debug.WriteLine("unsubscribing");
            MainActivity.Paused -= MainActivity_Paused;
            MainActivity.Resumed -= MainActivity_Resumed;
            MainActivity.Destroyed -= MainActivity_Destroyed;

            //_recordButton.Click -= HandleRecordingButtonClicked;
            _countdownLabel.Click -= _countdownLabel_Click;
            _countdownButton.Click -= _countdownLabel_Click;
            //_finishButton.Click -= _finishButton_Click;
            //_videoThumbnail.Click -= _videoThumbnail_Click;
            //_retakeButton.Click -= _retakeButton_Click;

            _subscribed = false;
        }

        void MainActivity_Paused(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("OnPause");
            ReleaseCamera();
        }

        void MainActivity_Resumed(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("OnResume");

            SetCameraControl();

        }

        void MainActivity_Destroyed(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("OnDestroy");
            ReleaseMediaRecorder();
        }

        void SetCameraControl()
        {
            if (_camera == null)
            {
                _camera = Camera.Open(0);
                if (_cameraCurrentlyLocked == -1)
                {
                    _cameraCurrentlyLocked = _defaultCameraId;
                }
                _cameraPreview.PreviewCamera = _camera;
                _cameraPreview.CameraId = _cameraCurrentlyLocked;

            }
        }

        void ReleaseCamera()
        {
            if (_camera != null)
            {
                _cameraPreview.PreviewCamera = null;
                _camera.Release();
                _camera = null;
            }
        }

        void ReleaseMediaRecorder()
        {
            if (_mediaRecorder != null)
            {
                _mediaRecorder.Reset();
                _mediaRecorder.Release();
                _mediaRecorder = null;
            }
        }

        void Reset()
        {

            _countdownSeconds = _countdownValue;

            // update ui
            UpdateCountdownLabel(_countdownValue);

            // initialize countdown timer
            _countdownTimer = new MyCountdownTimer(_countdownSeconds * 1000, 1000, countdownHandler);

            _durationSpan = TimeSpan.Zero;
            _durationLabel.Text = _durationSpan.ToString("c");
        }
        #endregion

        #region Video recording functions
        async void StartRecording()
        {
            if (_mediaRecorder == null)
            {
                _mediaRecorder = new MediaRecorder();
                _mediaRecorder.Error += (sender, e) => {
                    System.Diagnostics.Debug.WriteLine("MediaRecorder::Error: WHAT: " + e.What.ToString());
                    System.Diagnostics.Debug.WriteLine("MediaRecorder::Error: EXTRA: " + e.Extra.ToString());

                };

                _mediaRecorder.Info += (sender, e) => {
                    System.Diagnostics.Debug.WriteLine("MediaRecorder::Info: " + e.ToString());
                };
            }

            try
            {
                ConfigureRecorder();

                _durationSpan = TimeSpan.Zero;
                _durationTimer = new Timer("duration");
                _durationTimer.ScheduleAtFixedRate(new DurationTimerTask(durationHandler), 0, 10000);

                _mediaRecorder.Start();
                _recording = true;
            }
            catch (Exception e)
            {

                System.Diagnostics.Debug.WriteLine("{0}", e.ToString());
                await Acr.UserDialogs.UserDialogs.Instance.AlertAsync("Could not start recording.", "Oops!");

                // restore UI
                if (_durationTimer != null)
                    _durationTimer.Cancel();
                Reset();
                _recording = false;
                //UpdateRecordButton("start");

                // remove file
                if (_filePath != null && _filePath != "")
                {
                    Java.IO.File file = new Java.IO.File(_filePath);
                    bool deleted = file.Delete();
                }

                // release media recorder and restore camera
                ReleaseMediaRecorder();
                _camera.Lock();
            }
        }

        async Task StopRecording()
        {
            if (!_recording)
                return;

            System.Diagnostics.Debug.WriteLine("Stop Recording");

            try
            {

                _durationTimer.Cancel();

                _mediaRecorder.Stop();

                _finishButton_Click(this, null);
                //DisplayPlayback();

            }
            catch (Exception e)
            {

                await Acr.UserDialogs.UserDialogs.Instance.AlertAsync("Your video could not be saved.", "Oops!");

                //System.Diagnostics.Debug.WriteLine("StopRecording exception: "+ e.ToString());

                Reset();

            }
            finally
            {
                if (_mediaRecorder != null)
                    _mediaRecorder.Reset();
                _camera.Lock();
                _recording = false;

            }
        }

        void ConfigureRecorder(bool sizeSet = true)
        {
            if (_mediaRecorder == null)
                return;

            try
            {
                _camera.Unlock();
                _mediaRecorder.SetCamera(_camera);
                _mediaRecorder.SetVideoSource(Android.Media.VideoSource.Camera);
                _mediaRecorder.SetAudioSource(AudioSource.Default);
                //_mediaRecorder.SetOutputFormat(OutputFormat.OutputFormat.Mpeg4); 
                //_mediaRecorder.SetVideoEncoder(VideoEncoder.H264); 
                //_mediaRecorder.SetOutputFile(Environment.getExternalStorageDirectory().getPath() + "/test.3gp");
                //_mediaRecorder.SetMaxDuration(15000); 
                //_mediaRecorder.SetMaxFileSize(5000000); 
                //_mediaRecorder.SetVideoSize(640, 480); 
                //_mediaRecorder.SetVideoFrameRate(30);
                //_mediaRecorder.SetVideoEncodingBitRate(3000000);
                //_mediaRecorder.SetVideoSize(600, 600);

                // set video quality (no need to set encoders and output format)
                CamcorderProfile _camcorderProfile;

                if (_cameraCurrentlyLocked == 1)
                {
                    // front cam
                    if (CamcorderProfile.HasProfile(_cameraCurrentlyLocked, CamcorderQuality.Low))
                    {
                        _camcorderProfile = CamcorderProfile.Get(_cameraCurrentlyLocked, CamcorderQuality.Low);

                    }
                    else
                    {
                        _camcorderProfile = CamcorderProfile.Get(_cameraCurrentlyLocked, CamcorderQuality.Low );
                    }
                    System.Diagnostics.Debug.WriteLine("cpHigh is null ? " + _camcorderProfile == null);
                }
                else
                {
                    // high quality is specific to back camera
                    _camcorderProfile = CamcorderProfile.Get(CamcorderQuality.Low);
                }

                //_camcorderProfile.VideoFrameHeight = 420;
                //_camcorderProfile.VideoFrameWidth = 420;

                // set camera parameters to be same as video camcorder profile
                //var p = _camera.GetParameters();
                //p.SetPreviewSize(_camcorderProfile.VideoFrameWidth, _camcorderProfile.VideoFrameHeight);
                //_camera.SetParameters(p);


                _mediaRecorder.SetProfile(_camcorderProfile);

                SetRecorderOrientation(_cameraPreview.Orientation);

                var path = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath + string.Format("/{0}.mp4", DateTime.Now.ToString("yyyyMddHmmss"));
                _filePath = path;
                _mediaRecorder.SetOutputFile(path);
                _mediaRecorder.SetPreviewDisplay(_cameraPreview.Surface.Holder as Surface);

                _mediaRecorder.Prepare();

            }
            catch (Java.Lang.IllegalStateException e)
            {
                System.Diagnostics.Debug.WriteLine("Illegal state exception :(");
                throw e;
            }
            catch (Java.IO.IOException e)
            {
                System.Diagnostics.Debug.WriteLine("IOException preparing media recorder :(");
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        void SetRecorderOrientation(SurfaceOrientation orientation)
        {
            if (_mediaRecorder == null)
                return;

            Camera.CameraInfo camInfo = new Camera.CameraInfo();
            Camera.GetCameraInfo(_cameraCurrentlyLocked, camInfo);

            int degrees = 0;
            switch (orientation)
            {
                case SurfaceOrientation.Rotation90:
                    degrees = 90;
                    break;
                case SurfaceOrientation.Rotation180:
                    degrees = 180;
                    break;
                case SurfaceOrientation.Rotation270:
                    degrees = 270;
                    break;
                case SurfaceOrientation.Rotation0:
                default:
                    degrees = 0;
                    if (camInfo.Facing == Camera.CameraInfo.CameraFacingFront)
                    {
                        degrees = 180;
                    }
                    break;
            }

            int result;
            if (camInfo.Facing == Camera.CameraInfo.CameraFacingFront)
            {
                result = (camInfo.Orientation + degrees) % 360;
                result = (360 - result) % 360;  // compensate the mirror
            }
            else
            {  // back-facing
                result = (camInfo.Orientation - degrees + 360) % 360;
            }

            _mediaRecorder.SetOrientationHint(result);
        }

        void PlayNotificationSound()
        {
            if (!Element.CountdownNotificationEnabled)
                return;

            try
            {
                _ringtone.Play();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Error playing sound");
            }
        }

        void ToggleCamera()
        {

            if (_camera != null)
            {
                _camera.StopPreview();
                _cameraPreview.PreviewCamera = null;
                _camera.Release();
                _camera = null;
            }

            try
            {
                // acquire the next camera
                _camera = Camera.Open((_cameraCurrentlyLocked + 1) % _numberOfCameras);
                _cameraCurrentlyLocked = (_cameraCurrentlyLocked + 1) % _numberOfCameras;
                _cameraPreview.SwitchCamera(_camera);
                _cameraPreview.CameraId = _cameraCurrentlyLocked;

                // start preview
                _camera.StartPreview();
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("StartPreview::Exception: " + e.ToString());
            }
        }
        #endregion

        #region Display
        void InitializeViews()
        {
            var context = Forms.Context;

            _cameraPreview = new CameraPreview(context);
            relativeLayout = new Android.Widget.RelativeLayout(context);
            relativeLayout.SetBackgroundColor(Android.Graphics.Color.Aqua);

            //_recordButton = new Android.Widget.Button(context);

            var drawable = new GradientDrawable();
            drawable.SetShape(ShapeType.Rectangle);
            drawable.SetColor(Android.Graphics.Color.Red);
            drawable.SetCornerRadius(50);

            //_recordButton.SetBackground(drawable);
            //_recordButton.SetPadding(0, 0, 0, 0);
            //_recordButton.Text = "";
            //_recordButton.SetTextSize(Android.Util.ComplexUnitType.Dip, 0);

            _durationLabel = new TextView(Forms.Context);
            _durationLabel.Text = _durationSpan.ToString("c");
            _durationLabel.SetTextColor(Android.Graphics.Color.White);

            _countdownLabel = new TextView(Forms.Context);
            _countdownLabel.Text = "";
            _countdownLabel.SetTextColor(Android.Graphics.Color.White);
            _countdownLabel.Visibility = ViewStates.Gone;

            _countdownButton = new Android.Widget.Button(context);
            _countdownButton.SetBackgroundResource(boastloadapp.Droid.Resource.Drawable.xamarin_logo);

            //_toggleCameraButton = new ImageView(context);
            //         _toggleCameraButton.SetImageResource(testapp.Droid.Resource.Drawable.icon);
            //_toggleCameraButton.SetScaleType(ImageView.ScaleType.CenterInside);
            //_toggleCameraButton.Click += (sender, e) => {
            //  ToggleCamera();
            //};

            _videoPreviewLayout = new Android.Widget.RelativeLayout(context);
            _videoPreviewLayout.SetBackgroundColor(Android.Graphics.Color.Black);
            _videoThumbnail = new ImageView(context);

            //_retakeButton = new Android.Widget.Button(context);
            //_retakeButton.Text = "Retake Video";
            //         _retakeButton.SetBackgroundColor(Android.Graphics.Color.Orange);
            //_retakeButton.SetTextColor(Android.Graphics.Color.White);

            //_finishButton = new Android.Widget.Button(context);
            //_finishButton.Text = "Add";
            //         _finishButton.SetBackgroundColor(Android.Graphics.Color.Teal);
            //_finishButton.SetTextColor(Android.Graphics.Color.White);

        }

        void LayoutViews()
        {

            Android.Widget.RelativeLayout.LayoutParams buttonParams = new Android.Widget.RelativeLayout.LayoutParams(100, 100);

            buttonParams.AddRule(LayoutRules.CenterHorizontal);
            buttonParams.AddRule(LayoutRules.AlignParentBottom);
            buttonParams.BottomMargin = 20;

            Android.Widget.RelativeLayout.LayoutParams previewParams = new Android.Widget.RelativeLayout.LayoutParams(
                Android.Widget.RelativeLayout.LayoutParams.MatchParent,
                Android.Widget.RelativeLayout.LayoutParams.MatchParent
            );

            previewParams.AddRule(LayoutRules.CenterVertical);
            previewParams.AddRule(LayoutRules.CenterHorizontal);


            Android.Widget.RelativeLayout.LayoutParams durationLabelParams = new Android.Widget.RelativeLayout.LayoutParams(
                Android.Widget.RelativeLayout.LayoutParams.WrapContent,
                Android.Widget.RelativeLayout.LayoutParams.WrapContent
            );

            durationLabelParams.AddRule(LayoutRules.CenterHorizontal);
            durationLabelParams.AddRule(LayoutRules.AlignParentTop);
            durationLabelParams.TopMargin = 15;


            Android.Widget.RelativeLayout.LayoutParams countdownLabelParams = new Android.Widget.RelativeLayout.LayoutParams(
                Android.Widget.RelativeLayout.LayoutParams.WrapContent,
                Android.Widget.RelativeLayout.LayoutParams.WrapContent
            );
            countdownLabelParams.AddRule(LayoutRules.AlignParentRight);
            countdownLabelParams.AddRule(LayoutRules.AlignParentBottom);
            countdownLabelParams.BottomMargin = 20;
            countdownLabelParams.RightMargin = 20;

            Android.Widget.RelativeLayout.LayoutParams countdownButtonParams = new Android.Widget.RelativeLayout.LayoutParams(40, 40);
            countdownButtonParams.AddRule(LayoutRules.AlignParentRight);
            countdownButtonParams.AddRule(LayoutRules.AlignParentBottom);
            countdownButtonParams.BottomMargin = 20;
            countdownButtonParams.RightMargin = 20;


            Android.Widget.RelativeLayout.LayoutParams toggleParams = new Android.Widget.RelativeLayout.LayoutParams(50, 50);
            toggleParams.AddRule(LayoutRules.AlignParentRight);
            toggleParams.AddRule(LayoutRules.AlignParentTop);
            toggleParams.TopMargin = 10;
            toggleParams.RightMargin = 20;

            relativeLayout.AddView(_cameraPreview, previewParams);

            //relativeLayout.AddView(_recordButton, buttonParams);
            //relativeLayout.AddView(_durationLabel, durationLabelParams);
            //relativeLayout.AddView(_countdownLabel, countdownLabelParams);
            //relativeLayout.AddView(_countdownButton, countdownButtonParams);
            //relativeLayout.AddView(_toggleCameraButton, toggleParams);

            Android.Widget.RelativeLayout.LayoutParams thumbnailParams = new Android.Widget.RelativeLayout.LayoutParams(
                Android.Widget.RelativeLayout.LayoutParams.MatchParent,
                Android.Widget.RelativeLayout.LayoutParams.MatchParent
            );

            Android.Widget.RelativeLayout.LayoutParams retakeParams = new Android.Widget.RelativeLayout.LayoutParams(
                Android.Widget.RelativeLayout.LayoutParams.WrapContent,
                Android.Widget.RelativeLayout.LayoutParams.WrapContent
            );

            retakeParams.AddRule(LayoutRules.AlignParentBottom);
            retakeParams.AddRule(LayoutRules.AlignParentLeft);

            Android.Widget.RelativeLayout.LayoutParams finishParams = new Android.Widget.RelativeLayout.LayoutParams(
                Android.Widget.RelativeLayout.LayoutParams.WrapContent,
                Android.Widget.RelativeLayout.LayoutParams.WrapContent
            );
            finishParams.AddRule(LayoutRules.AlignParentBottom);
            finishParams.AddRule(LayoutRules.AlignParentRight);

            _videoPreviewLayout.AddView(_videoThumbnail, thumbnailParams);
            //_videoPreviewLayout.AddView(_retakeButton, retakeParams);
            //_videoPreviewLayout.AddView(_finishButton, finishParams);
            _videoPreviewLayout.Visibility = ViewStates.Gone;

            relativeLayout.AddView(_videoPreviewLayout);
            relativeLayout.BringChildToFront(_videoPreviewLayout);

        }

        async void DisplayPlayback()
        {
            Element.ShowingPlayback = true;

            if (!string.IsNullOrEmpty(_filePath))
            {
                Android.Graphics.Bitmap thumb;

                thumb = await ThumbnailUtils.CreateVideoThumbnailAsync(_filePath, ThumbnailKind.FullScreenKind);

                if (thumb != null)
                {
                    _videoThumbnail.SetImageBitmap(thumb);

                    relativeLayout.BringChildToFront(_videoPreviewLayout);
                    _videoPreviewLayout.Visibility = ViewStates.Visible;
                    //_recordButton.Visibility = ViewStates.Gone;
                    _countdownLabel.Visibility = ViewStates.Gone;
                    _countdownButton.Visibility = ViewStates.Gone;
                }
                else
                    Acr.UserDialogs.UserDialogs.Instance.Toast("Something went wrong. Please try again.", TimeSpan.FromSeconds(2));
            }
            else
                Acr.UserDialogs.UserDialogs.Instance.Toast("Something went wrong. Please try again.", TimeSpan.FromSeconds(2));

        }

        void HidePlayback()
        {
            // hide preview
            _videoPreviewLayout.Visibility = ViewStates.Gone;

            //_recordButton.Visibility = ViewStates.Visible;
            UpdateCountdownLabel(_countdownSeconds);

            Element.ShowingPlayback = false;
        }

        void UpdateCountdownLabel(int secs)
        {
            if (secs > 0)
            {
                Element.CountingDown = true;

                _countdownButton.SetBackground(null);
                _countdownLabel.Text = countdownItems[secs];
                _countdownLabel.Visibility = ViewStates.Visible;
                _countdownButton.Visibility = ViewStates.Gone;
            }
            else
            {
                Element.CountingDown = false;

                _countdownButton.SetBackgroundResource(boastloadapp.Droid.Resource.Drawable.xamarin_logo);
                _countdownLabel.Text = "";
                _countdownLabel.Visibility = ViewStates.Gone;
                _countdownButton.Visibility = ViewStates.Visible;
            }
        }

        //async void UpdateRecordButton(string state)
        //{
        //  int delta = 50 / 10;
        //  int init_radius;

        //  GradientDrawable drawable;
        //  drawable = new GradientDrawable();
        //  drawable.SetShape(ShapeType.Rectangle);
        //  drawable.SetSize(50, 50);

        //  switch (state)
        //  {
        //      case "counting":
        //          drawable.SetColor(Android.Graphics.Color.LightGray);
        //          drawable.SetCornerRadius(50);
        //          _recordButton.SetBackground(drawable);
        //          _recordButton.Enabled = false;
        //          break;
        //      case "start":
        //          init_radius = 0;
        //          _recordButton.Enabled = true;
        //          drawable.SetColor(Android.Graphics.Color.Red);
        //          for (int i = 0; i < 10; i++)
        //          {
        //              drawable.SetCornerRadius(init_radius + delta * i);
        //              _recordButton.SetBackground(drawable);
        //              await Task.Delay(20);
        //          }
        //          drawable.SetCornerRadius(50);
        //          break;
        //      case "stop":
        //          init_radius = 50;
        //          _recordButton.Enabled = true;
        //          drawable.SetColor(Android.Graphics.Color.Red);
        //          for (int i = 0; i < 10; i++)
        //          {
        //              drawable.SetCornerRadius(init_radius - delta * i);
        //              _recordButton.SetBackground(drawable);
        //              await Task.Delay(20);
        //          }
        //          break;
        //      default:
        //          init_radius = 0;
        //          _recordButton.Enabled = true;
        //          drawable.SetColor(Android.Graphics.Color.Red);
        //          for (int i = 0; i < 10; i++)
        //          {
        //              drawable.SetCornerRadius(init_radius + delta * i);
        //              _recordButton.SetBackground(drawable);
        //              await Task.Delay(20);
        //          }
        //          break;
        //  }

        //}
        #endregion

        #region Event handlers
        void HandleRecordingButtonClicked(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Recording button clicked");
            if (_recording)
            {
                //_recordButton.Text = "Start Recording";
                StopRecording();
                //UpdateRecordButton("start");
            }
            else
            {
                if (_countdownSeconds > 0)
                {
                    //System.Diagnostics.Debug.WriteLine("Starting countdown timer");
                    //UpdateRecordButton("counting");
                    _countdownTimer.Start();
                    //_recordButton.Enabled = false;
                }
                else
                {
                    //_recordButton.Text = "Stop Recording";
                    StartRecording();
                    //UpdateRecordButton("stop");
                }
            }
        }

        void _retakeButton_Click(object sender, EventArgs e)
        {
            // delete file
            Java.IO.File file = new Java.IO.File(_filePath);
            bool deleted = file.Delete();

            HidePlayback();

            Reset();
        }

        async void _finishButton_Click(object sender, EventArgs e)
        {
            /// REMOVED
            //         if (!testapp.Settings.InformedAboutAndroidConverterDownload)
            //{
            //  var okay = await Acr.UserDialogs.UserDialogs.Instance.ConfirmAsync("To compress the video files sent to the server, this app needs to download a video converter which is around 15MB in size.", null, "Download", "Nevermind");
            //             testapp.Settings.InformedAboutAndroidConverterDownload = okay;
            //  if (!okay)
            //  {
            //      return;
            //  }
            //  //Android.Support.V7.App.AlertDialog.Builder builder = new Android.Support.V7.App.AlertDialog.Builder(Forms.Context);
            //  //builder.SetTitle("Video Compression")
            //  //       .SetMessage("To compress the video files sent to the server, this app needs to download a video converter which is around 15MB in size.")


            //  //AlertDialog  = builder.Create();
            //}
            /// ENDS HERE

            // compress video
            _recorder.SetVideoCompressing(true);
            //Java.IO.File videoFile = await videoConverter.ConvertFileAsync(this.Context, new Java.IO.File(_filePath));
            Java.IO.File videoFile = new Java.IO.File(_filePath);

            System.Diagnostics.Debug.WriteLine("original file size: {0}MB", videoFile.Length() / (1024 * 1024.0));

            //var converter = new VideoConverter();
            //var compressedFile = await converter.ConvertFileAsync(Forms.Context, videoFile);

            //System.Diagnostics.Debug.WriteLine("compressed file size: {0}MB", compressedFile.Length() / (1024 * 1024.0));

            _recorder.SetVideoCompressing(false);

            HidePlayback();

            Reset();

            var mediaFile = new MediaFile(_filePath, () => System.IO.File.OpenRead(_filePath), true, (dispose) =>
            {
                if (dispose)
                {
                    //if (compressedFile != null)
                    //{
                    //// remove file
                    //compressedFile.Delete();
                    // compressedFile.Dispose();
                    //}
                }
            });
            _recorder.SetVideoRecorded(mediaFile);
        }

        //void _videoThumbnail_Click(object sender, EventArgs e)
        //{
        //    Intent intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse(_filePath));
        //    intent.SetDataAndType(Android.Net.Uri.Parse(_filePath), "video/mp4");
        //    var activity = Forms.Context as Activity;
        //    activity.StartActivity(intent);
        //}

        void _countdownLabel_Click(object sender, EventArgs e)
        {
            _countdownDialog.Show();
        }

        void HandleDurationTimerFired(Message obj)
        {
            _durationSpan = _durationSpan.Add(TimeSpan.FromSeconds(1));
            _durationLabel.Text = _durationSpan.ToString("c");
        }

        void HandleCountdownTimerFired(Message obj)
        {
            int msg = obj.What;

            _countdownSeconds -= 1;
            UpdateCountdownLabel(_countdownSeconds);
            PlayNotificationSound();

            if (msg == 1)
            {
                System.Diagnostics.Debug.WriteLine("Starting recording");
                StartRecording();
                //UpdateRecordButton("stop");
                //_recordButton.Text = "Stop Recording";
                //_recordButton.Enabled = true;
            }
        }

        void HandleCountdownValueSelected(object sender, DialogClickEventArgs e)
        {
            _countdownValue = e.Which;

            Reset();
        }
        #endregion

        #region IDisposable implementation
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                System.Diagnostics.Debug.WriteLine("Disposing video recorder");
                ReleaseCamera();

                ReleaseMediaRecorder();

                UnsubscribeFromEvents();
            }
        }
        #endregion


    }

#pragma warning restore 0618

    class DurationTimerTask : TimerTask
    {
        Handler _h;
        public DurationTimerTask(Handler h)
        {
            _h = h;
        }

        public override void Run()
        {
            _h.SendMessage(new Message());
        }
    }

    class MyCountdownTimer : CountDownTimer
    {
        Handler _h;
        public MyCountdownTimer(long millisInFuture, long countDownInterval, Handler h) : base(millisInFuture, countDownInterval)
        {
            _h = h;
        }

        public override void OnFinish()
        {
            _h.SendEmptyMessage(1);
        }

        public override void OnTick(long millisUntilFinished)
        {
            _h.SendEmptyMessage(0);
        }
    }
}
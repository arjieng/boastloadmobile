﻿using System;
using boastloadapp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ViewCell), typeof(DefaultViewCellRenderer))]
namespace boastloadapp.Droid
{
    public class DefaultViewCellRenderer : ViewCellRenderer
    {
        protected override Android.Views.View GetCellCore(Cell item, Android.Views.View convertView, Android.Views.ViewGroup parent, Android.Content.Context context)
        {
            return base.GetCellCore(item, convertView, parent, context);
        }
    }
}


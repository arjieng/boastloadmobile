﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Android.App;
using Android.Graphics;
using Android.Media;
using Android.Provider;
using Android.Widget;
using boastloadapp;
using boastloadapp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomVideoPlayer), typeof(CustomVideoPlayerRenderer))]
namespace boastloadapp.Droid
{
    public class CustomVideoPlayerRenderer : ViewRenderer<CustomVideoPlayer, VideoView>, IDisposable
    {
        VideoView _videoView;
        MediaController _mediaController;
        CustomVideoPlayer _element;

        double screenWidth, screenHeight;

        public CustomVideoPlayerRenderer()
        {
            System.Diagnostics.Debug.WriteLine("VideoPlayerRenderer initialized");
        }

        protected override void OnElementChanged(ElementChangedEventArgs<CustomVideoPlayer> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
            {
                System.Diagnostics.Debug.WriteLine("Setting native control");
                _videoView = new VideoView(Context);
                _mediaController = new MediaController(Context);
                _mediaController.SetAnchorView(_videoView);
                _videoView.SetMediaController(_mediaController);

                var activity = Forms.Context as Activity;
                Android.Graphics.Point size = new Android.Graphics.Point();
                activity.WindowManager.DefaultDisplay.GetSize(size);
                System.Diagnostics.Debug.WriteLine("Width: {0}\tHeight: {1}", size.X, size.Y);
                screenHeight = size.Y;
                screenWidth = size.X;

                SetNativeControl(_videoView);
            }

            if (e.OldElement != null)
            {
                // unsubscribe
            }

            if (e.NewElement != null)
            {
                _element = e.NewElement;
            }
        }

        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == "VideoSource")
            {
                UpdateVideoUrl(_element.VideoSource);
            }
        }

        protected override void Dispose(bool disposing)
        {
            System.Diagnostics.Debug.WriteLine("VideoPlayerRenderer::Dispose");
            _mediaController.Dispose();
            _videoView.Dispose();

            //_mediaController = null;
            //_videoView = null;

            //base.Dispose(disposing);
        }

        protected async void UpdateVideoUrl(string url)
        {
            if (url == null)
                return;

            _element.IsLoading = true;

            var size = await RetrieveVideoThumbnail(url);
            if (size.X > 0 && size.Y > 0)
            {
                var ratio = (double)size.X / size.Y;
                if (ratio >= 1)
                {
                    // landscape
                    System.Diagnostics.Debug.WriteLine("Landscape");
                    _element.WidthRequest = _element.Width;
                    _element.HeightRequest = (_element.Width / size.X) * size.Y;
                }
                else
                {
                    // portrait
                    System.Diagnostics.Debug.WriteLine("Portrait");
                    // FIXME: Check with actual portrait video.
                    _element.WidthRequest = _element.Width;
                    _element.HeightRequest = (_element.Width / size.X) * size.Y;
                }
            }

            if (_videoView != null)
            {
                _videoView.SetVideoURI(Android.Net.Uri.Parse(url));

                _videoView.Prepared += HandleVideoPrepared;
            }
        }


        void HandleVideoPrepared(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Video is prepared");
            _element.IsLoading = false;
            _mediaController.Show(2000);

            _videoView.Prepared -= HandleVideoPrepared;
        }

        protected async Task<Android.Graphics.Point> RetrieveVideoThumbnail(string url)
        {
            var size = new Android.Graphics.Point();
            System.Diagnostics.Debug.WriteLine("Retrieving thumbnail");
            Bitmap thumb = null;
            MediaMetadataRetriever mediaMetadataRetriever = null;
            try
            {
                mediaMetadataRetriever = new MediaMetadataRetriever();
                // for SDK >= 14
                await mediaMetadataRetriever.SetDataSourceAsync(url, new Dictionary<string, string>());
                thumb = mediaMetadataRetriever.GetFrameAtTime(100);

            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Could not get video thumbnail");
                System.Diagnostics.Debug.WriteLine("{0}", ex.Message);
            }
            finally
            {
                if (mediaMetadataRetriever != null)
                {
                    mediaMetadataRetriever.Release();
                }
            }

            if (thumb != null)
            {

                size.X = thumb.Width;
                size.Y = thumb.Height;

                thumb.Dispose();
            }

            return size;
        }
    }
}
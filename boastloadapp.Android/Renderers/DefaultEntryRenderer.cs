﻿using System;
using Android.App;
using Android.Text;
using Android.Views;
using Android.Views.InputMethods;
using boastloadapp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Entry), typeof(DefaultEntryRenderer))]
namespace boastloadapp.Droid
{
    public class DefaultEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.SetPadding(0,0,0,0);
                this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
                Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);
                //Control.InputType(InputTypes.)
                Element.Focused += Element_Focused;
                Element.Unfocused += Element_Unfocused;
                //Control.TextAlignment = Android.Views.TextAlignment.Center;
                //Control.Gravity = Android.Views.GravityFlags.CenterVertical;
                if (e.NewElement.StyleId == null)
                {
                    Control.Gravity = Android.Views.GravityFlags.Center;
                }
                else if (e.NewElement.StyleId.Equals("L"))
                {
                    Control.Gravity = Android.Views.GravityFlags.Left;
                }
            }

            if (e.OldElement != null)
            {
                Element.Focused -= Element_Focused;
            }
        }

        private void Element_Unfocused(object sender, FocusEventArgs e)
        {
            HideNavigationBar();
        }

        void HideNavigationBar()
        {
            var window = ((Activity)Forms.Context).Window;
            int uiOptions = (int)window.DecorView.SystemUiVisibility;

            uiOptions |= (int)SystemUiFlags.LowProfile;
            uiOptions |= (int)SystemUiFlags.Fullscreen;
            uiOptions |= (int)SystemUiFlags.HideNavigation;
            uiOptions |= (int)SystemUiFlags.ImmersiveSticky;

            window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
        }

        private void Element_Focused(object sender, FocusEventArgs e)
        {
            HideNavigationBar();
            var imm = (InputMethodManager)Context.GetSystemService(Android.Content.Context.InputMethodService);
            imm.ShowSoftInput(Control, Android.Views.InputMethods.ShowFlags.Implicit);


        }
    }
}

﻿using System;
using boastloadapp;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using boastloadapp.Droid;
using Android.Views.InputMethods;
using Android.App;
using Android.Widget;
using Android.Text;
using Android.Views;
using System.ComponentModel;
using Android.Content;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace boastloadapp.Droid
{
    public class CustomEntryRenderer : EntryRenderer
    {
        public CustomEntryRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            CustomEntry entry = (CustomEntry)this.Element;

            if (this.Control != null)
            {
                Control.SetPadding(0, 0, 0, 0);
                this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
                Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);
                //Control.InputType(InputTypes.)
                //Element.Focused += Element_Focused;
                //Element.Unfocused += Element_Unfocused;
                //Control.TextAlignment = Android.Views.TextAlignment.Center;
                //Control.Gravity = Android.Views.GravityFlags.CenterVertical;
                if (e.NewElement.StyleId == null)
                {
                    Control.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.CenterVertical;
                }
                else if (e.NewElement.StyleId.Equals("C"))
                {
                    Control.Gravity = Android.Views.GravityFlags.Center;
                }
                else if (e.NewElement.StyleId.Equals("LC"))
                {
                    Control.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.CenterVertical;
                }

                if (entry.AutoCapitalization == AutoCapitalizationType.Words)
                {
                    Control.InputType = InputTypes.TextFlagCapWords;
                }
                else if (entry.AutoCapitalization == AutoCapitalizationType.Sentences)
                {
                    Control.InputType = InputTypes.TextFlagCapSentences;
                }
                else if (entry.AutoCapitalization == AutoCapitalizationType.All)
                {
                    Control.InputType = InputTypes.TextFlagCapCharacters;
                }

                if (entry != null)
                {
                    SetReturnType(entry);

                    Control.EditorAction += (object sender, TextView.EditorActionEventArgs args) =>
                    {
                        if (entry.ReturnType != ReturnType.Next)
                            entry.Unfocus();

                        entry.InvokeCompleted();
                    };
                }
            }

            if (e.OldElement != null)
            {
                //Element.Focused -= Element_Focused;
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //CustomEntry entry = (CustomEntry)this.Element;
            base.OnElementPropertyChanged(sender, e);
            //if (Element.StyleId == null)
            //{
            //    Control.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.CenterVertical;
            //}
            //else if (Element.StyleId.Equals("C"))
            //{
            //    Control.Gravity = Android.Views.GravityFlags.Center;
            //}
            //else if (Element.StyleId.Equals("LC"))
            //{
            //    Control.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.CenterVertical;
            //}

            //if (entry.AutoCapitalization == AutoCapitalizationType.Words)
            //{
            //    Control.InputType = InputTypes.TextFlagCapWords;
            //}
            //else if (entry.AutoCapitalization == AutoCapitalizationType.Sentences)
            //{
            //    Control.InputType = InputTypes.TextFlagCapSentences;
            //}
            //else if (entry.AutoCapitalization == AutoCapitalizationType.All)
            //{
            //    Control.InputType = InputTypes.TextFlagCapCharacters;
            //}

            //if (entry != null)
            //{
            //    SetReturnType(entry);

            //    Control.EditorAction += (object senderrr, TextView.EditorActionEventArgs args) =>
            //    {
            //        if (entry.ReturnType != ReturnType.Next)
            //            entry.Unfocus();

            //        entry.InvokeCompleted();
            //    };
            //}
        }

        private void SetReturnType(CustomEntry entry)
        {
            ReturnType type = entry.ReturnType;

            switch (type)
            {
                case ReturnType.Go:
                    Control.ImeOptions = ImeAction.Go;
                    Control.SetImeActionLabel("Go", ImeAction.Go);
                    break;
                case ReturnType.Next:
                    Control.ImeOptions = ImeAction.Next;
                    Control.SetImeActionLabel("Next", ImeAction.Next);
                    break;
                case ReturnType.Send:
                    Control.ImeOptions = ImeAction.Send;
                    Control.SetImeActionLabel("Send", ImeAction.Send);
                    break;
                case ReturnType.Search:
                    Control.ImeOptions = ImeAction.Search;
                    Control.SetImeActionLabel("Search", ImeAction.Search);
                    break;
                default:
                    Control.ImeOptions = ImeAction.Done;
                    Control.SetImeActionLabel("Done", ImeAction.Done);
                    break;
            }
        }

        //private void Element_Unfocused(object sender, FocusEventArgs e)
        //{
        //    HideNavigationBar();
        //}

        //void HideNavigationBar()
        //{
        //    var window = ((Activity)Forms.Context).Window;
        //    int uiOptions = (int)window.DecorView.SystemUiVisibility;

        //    uiOptions |= (int)SystemUiFlags.LowProfile;
        //    uiOptions |= (int)SystemUiFlags.Fullscreen;
        //    uiOptions |= (int)SystemUiFlags.HideNavigation;
        //    uiOptions |= (int)SystemUiFlags.ImmersiveSticky;

        //    window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
        //}

        //private void Element_Focused(object sender, FocusEventArgs e)
        //{
        //    HideNavigationBar();
        //    var imm = (InputMethodManager)Context.GetSystemService(Android.Content.Context.InputMethodService);
        //    imm.ShowSoftInput(Control, Android.Views.InputMethods.ShowFlags.Implicit);
        //}
    }
}

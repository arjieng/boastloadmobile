﻿using System;
using System.ComponentModel;
using Android.Content;
using boastloadapp;
using boastloadapp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Label), typeof(DefaultLabelRenderer))]
namespace boastloadapp.Droid
{
    public class DefaultLabelRenderer: LabelRenderer
    {
        public DefaultLabelRenderer(Context context) : base(context)
        {

        }
        
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if(Control != null)
            {
                // Remove Padding
                this.Control.SetPadding(0, (int)-2.ScaleHeight(), 0, (int)-2.ScaleHeight());
                this.UpdateLayout();
            }

            if(e.NewElement != null)
            {

            }

            if(e.OldElement != null)
            {
                
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

        }
    }
}

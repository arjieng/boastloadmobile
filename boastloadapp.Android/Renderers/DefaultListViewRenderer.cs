﻿using System;
using Android.Content;
using Android.Views;
using boastloadapp;
using boastloadapp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomListView), typeof(ListViewNoScrollRenderer))]
namespace boastloadapp.Droid
{
    public class ListViewNoScrollRenderer : ListViewRenderer
    {
        public ListViewNoScrollRenderer(Context context) : base(context)
        {

        }

        protected CustomListView customListView { get; private set; }
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            // Remove Scroll Bars
            this.VerticalScrollBarEnabled = false;
            this.HorizontalScrollBarEnabled = false;

            if(Control != null || e.NewElement != null){
                this.customListView = (CustomListView)this.Element;
                Control.VerticalScrollBarEnabled = false;
                Control.HorizontalScrollBarEnabled = false;
            }
        }

        public override bool DispatchTouchEvent(MotionEvent e)
        {
            if(!customListView.Scroll){
                if(e.Action == MotionEventActions.Move){
                    return true;
                }
            }
            return base.DispatchTouchEvent(e);
        }
    }
}
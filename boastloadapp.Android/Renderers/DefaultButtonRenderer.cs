﻿using System;
using Android.Content;
using Android.Graphics;
using boastloadapp.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Button), typeof(CustomButtonRenderer))]
namespace boastloadapp.Droid
{
    public class CustomButtonRenderer : ButtonRenderer
    {
        public CustomButtonRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            try
            {
                base.OnElementChanged(e);
                // Disabled Auto Caps
                Control.SetAllCaps(false);
                // Set Default Padding
                Control.SetPadding(10.scale(), 0, 10.scale(), 0);
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine("[CustomButtonRenderer] OnElementChanged");
            }
        }
        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
        }
    }
}


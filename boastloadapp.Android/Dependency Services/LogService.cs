﻿using System;
using boastloadapp.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(LogService))]
namespace boastloadapp.Droid
{
    public class LogService : ILogServices
    {
        public void Log(string log)
        {
            Console.WriteLine(log);
        }
    }
}
